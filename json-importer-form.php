<h2><?php _e( 'File Upload', 'file_upload' ); ?></h2>
<br>

<!-- #content -->

<div id="general-semi-nav">
    <!-- #main-nav-left -->
    <div id="content-nav-right" class="general-vibe">
        <div class="widefat general-semi upload_files" border="0">
            <form name="customefile" id="fileform" method="post" class="reset_form" action="" style="width:100%;" enctype="multipart/form-data">
                <!-- .section -->
                <div class="section">
                    <h2 class="heading"><?php _e( 'Upload file', 'file_upload' ); ?></h2>
                    <div class="option allow">
                        <input type="file" id ="file" name="upload_file" class="full-width" />
                    </div>
                    <!-- .option -->
                </div>
                <label style="display: inline-block;">
                    <?php echo  __( 'Select Post Type:', 'WPLI' ) ; ?></label>
                <select id="selectcategory" name="selectcategory">
                    <option value="teacher"><?php echo __( 'Teacher', 'WPLI' ); ?></option>
                    <!-- <option value="studio"><?php //echo __( 'Studios', 'WPLI' ); ?></option> -->
                    <option value="school"><?php echo __( 'Schools & Studios', 'WPLI' ); ?></option>
                    <option value="retreat"><?php echo __( 'Retreat', 'WPLI' ); ?></option>
                </select>
                <!-- .section -->
                <div class="wraps">
                    <input style="margin-left: 10px; margin-top:10px;" type="submit" id="upload_file_button" name="submit" class="button button-primary" value="<?php echo __( 'Upload', 'WPLI' ); ?>"/>
                    <input style="margin-left: 10px; margin-top:10px;" type="submit" id="export_all_button" name="export_all_button" class="button button-primary" value="<?php echo __( 'Export ALL', 'WPLI' ); ?>"/>
                </div>
            </form>
        </div>
    </div>
    <!-- #content-nav-right -->
</div>
<!-- #general-semi-nav -->