<h2><?php _e( 'Restricted Usernames', 'restricted_username' ); ?></h2>
<br>

<!-- #content -->

<div id="general-semi-nav-checking">
    <!-- #main-nav-left -->
    <div id="content-nav-right-checking" class="general-vibe">
        <div class="widefat general-semi upload_files checking_restricted_username_files" border="0">
            <form name="customrestricted_username" id="customrestricted_username" method="post" class="reset_form" action="" style="width:100%;" enctype="multipart/form-data">
             <!-- .section -->
                <div class="restricted-username">
                    <label style="display: inline-block;">
                        <?php _e( 'Restricted Usernames:', 'WPLI' ) ; ?>
                    </label> 
                    <?php 
                        $getdata = get_option('restrictedusername');  
                        $decodedValue=json_decode($getdata);
                        $finalArray = array();
                        if(!empty($decodedValue)){
                            foreach($decodedValue as $arrays){
                                $finalArray[] = $arrays;
                            }    
                        }
                        $restricted_username =implode('&#013;', $finalArray);
                    ?>
                    <textarea contenteditable="true" id='restricted-username-check' cols='60' rows='8'><?php echo $restricted_username;?></textarea>                 
                </div> 
                <!-- .section -->
                <div class="wraps">
                    <input style="margin-top:10px;" type="submit" id="restricted_file_button" name="submit" class="button button-primary" value="<?php echo __( 'Submit', 'WPLI' ); ?>"/>                    
                </div>
            </form>
        </div>
    </div>
    <!-- #content-nav-right -->
</div>
<!-- #general-semi-nav -->