<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    WPLI
 * @subpackage WPLI/admin
 * @author     Rohit Sharma
 */

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
class WPLI_Admin {
    private $plugin_name,$version,$helper;

    public function __construct( $WPLI, $version ) {
        $this->plugin_name = $WPLI;
        $this->version = $version;
        $this->helper = new listing_importer_helper();
        add_action( 'wp_ajax_nopriv_get_smartimportteacher', array( $this,'smart_import_ajax_handler' ));
        add_action( 'wp_ajax_get_smartimportteacher',  array( $this,'smart_import_ajax_handler' ));
        add_action( 'wp_ajax_nopriv_get_smartremainingrecords',  array( $this,'smart_remaining_ajax_handler' ));
        add_action( 'wp_ajax_get_smartremainingrecords',  array( $this,'smart_remaining_ajax_handler' ));
        add_action( 'wp_ajax_nopriv_delete_all',  array( $this,'delete_all_listing_records' ));
        add_action( 'wp_ajax_nopriv_get_exportall',  array( $this,'exportall_handler' ));
        add_action( 'wp_ajax_get_exportall',  array( $this,'exportall_handler' ));
        add_action( 'wp_ajax_delete_all',  array( $this,'delete_all_listing_records' ));
        add_action( 'wp_ajax_nopriv_delete_blank_listing',  array( $this,'delete_blank_listing_records' ));
        add_action( 'wp_ajax_delete_blank_listing',  array( $this,'delete_blank_listing_records' ));
        add_action( 'admin_menu', array($this, 'listing_admin_menu' ) );
        add_action('admin_enqueue_scripts', array($this,'cstm_css_and_js'));
    }

    public function listing_admin_menu() {
        add_menu_page(__('Import Json Feed','WPLI'),__('Import Json Feed','WPLI'),'manage_options','file_upload',array($this,'app_builder_create_page'),'',110);
    }

    public function cstm_css_and_js() {
        wp_enqueue_style( 'wooccm-backend-css', plugins_url( 'public/assets/backend_css.css', APP_RELPATH ) );
        wp_enqueue_script( 'script_wccs', plugins_url( 'public/assets/js/script_wccs.js', APP_RELPATH ), array( 'jquery' ), '1.2' );
        wp_enqueue_script( 'billing_script_wccs', plugins_url( 'public/assets/js/billing_script_wccs.js', APP_RELPATH ), array( 'jquery' ), '1.2' );
        wp_enqueue_script( 'shipping_script_wccs', plugins_url( 'public/assets/js/shipping_script_wccs.js', APP_RELPATH ), array( 'jquery' ), '1.2' );
        wp_enqueue_script( 'importer_custom_js', plugins_url( 'public/assets/js/custom-js.js', APP_RELPATH ), array( 'jquery' ), '1.3' );
        //wp_enqueue_script( 'importer_custom_js', plugins_url( 'public/assets/js/my-js.js', APP_RELPATH ), array( 'jquery' ), '1.3' );
        wp_enqueue_script( 'importer_sweet_alert_js','https://cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.js');
        wp_enqueue_style ( 'importer_sweet_alert_style', 'https://cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.css');
        wp_localize_script( 'importer_custom_js', 'importer_ajax_object', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
        wp_localize_script( 'importer_custom_js', 'remaining_ajax_object', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
        wp_localize_script( 'importer_custom_js', 'json_ajax_object', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
        wp_localize_script( 'importer_custom_js', 'export_all_ajax_object', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
    }

    public function exportall_handler(){
        global $wpdb;
        $exportargs = array(
            'post_type' =>array('teacher','studio','school','retreat'),
            'post_status' => 'all',
            'posts_per_page' => -1,
            'nopaging' => true,

        );
        //echo "<pre>"; print_r($exportargs);exit;
        //$allpostsexport = new WP_Query($exportargs);
        $allpostsexport = get_posts($exportargs);
        // echo "<pre>";print_r($allpostsexport);exit;
        if(!empty($allpostsexport)){
            $json_array = array();
            foreach( $allpostsexport as $allpostsexports ){
                //echo "<pre>";print_r($allpostsexports);exit;
                $post_id = $allpostsexports->ID;
                $listing_exportalljson  = get_post_meta( $post_id, 'listing_json', true );
                $json_exportobject[] = json_decode($listing_exportalljson);
                //echo "<pre>";print_r($json_exprotobject);exit;
            }
            $json_exportstring  =  json_encode( $json_exportobject,true );
            //echo "<pre>";print_r($json_exportstring);exit;
            $export_file_name="exportAll.json";
            $myexportfile = fopen(APP_PATH."logs/$export_file_name", "a") or die("Unable to open file!");
            fwrite($myexportfile, $json_exportstring);
            fclose($myexportfile);
            $json_file_export_url = APP_URL."logs/$export_file_name";
            $response[ 'success' ] =  1;
            $response[ 'file_data' ] =  $json_exportstring;
            $response[ 'file_url' ] =  $json_file_export_url;
            echo json_encode( $response ); exit;
        }
    }

    public function app_builder_create_page(){
        $coordinates = $this->helper->get_geocode('US, 3700 Vesta Dr - Raleigh, NC 27603-3842');
        //echo '<pre>'; print_r( $coordinates );
        //echo $city_name = $this->helper->get_city_name( $coordinates['lat'],$coordinates['lng'] );exit;

        wp_enqueue_style( 'wooccm-backend-css', plugins_url( 'public/assets/backend_css.css', APP_RELPATH ) );
        wp_enqueue_script( 'script_wccs', plugins_url( 'public/assets/js/script_wccs.js', APP_RELPATH ), array( 'jquery' ), '1.2' );
        wp_enqueue_script( 'billing_script_wccs', plugins_url( 'public/assets/js/billing_script_wccs.js', APP_RELPATH ), array( 'jquery' ), '1.2' );
        wp_enqueue_script( 'shipping_script_wccs', plugins_url( 'public/assets/js/shipping_script_wccs.js', APP_RELPATH ), array( 'jquery' ), '1.2' );
        wp_enqueue_script( 'importer_custom_js', plugins_url( 'public/assets/js/custom-js.js', APP_RELPATH ), array( 'jquery' ), '1.3' );
        wp_enqueue_script( 'importer_custom_js', plugins_url( 'public/assets/js/my-js.js', APP_RELPATH ), array( 'jquery' ), '1.3' );
        wp_enqueue_script( 'importer_sweet_alert_js','https://cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.js');
        wp_enqueue_style ( 'importer_sweet_alert_style', 'https://cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.css');
        wp_localize_script( 'importer_custom_js', 'importer_ajax_object', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
        wp_localize_script( 'importer_custom_js', 'remaining_ajax_object', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
        wp_enqueue_style( 'wp-color-picker' );
        wp_enqueue_script( 'my-script-handle', plugins_url('public/assets/js/iris_init.js', __FILE__ ), array( 'wp-color-picker' ), false, true );
        require_once(APP_PATH.'/admin/json-importer-form.php');
    }

    public function smart_import_ajax_handler(){
        global $wpdb;
        ini_set("max_execution_time",3000000000);
        ini_set("max_allowed_memory",'4024M');
        ini_set("max_posts_vars",'4024M');
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);

        if($_FILES['file']['name'] !=''){
            if( $_FILES['file']['type'] /* == 'application/json'  */) {
                if (file_exists (ABSPATH.'/wp-admin/includes/taxonomy.php')) {
                    require_once (ABSPATH.'/wp-load.php');
                    require_once (ABSPATH.'/wp-admin/includes/taxonomy.php');
                }
                $str      = file_get_contents($_FILES['file']['tmp_name']);
                $strdata  = json_decode($str, true);
                $total_listings = count( $strdata );//12687
                //  Upload fiel here.
                $folder_path = APP_PATH.'uploads/';
                $filename = basename($_FILES['file']['name']);
                $newname = $folder_path . $filename;
                if (!move_uploaded_file($_FILES['file']['tmp_name'], $newname)) {
                    $response[ 'status' ]  = "fail";
                    echo json_encode( $response );exit;
                }

                $response[ 'total_records' ]  = $total_listings;
                $response[ 'data' ]  = $str;
                update_option( 'temp_json_feed',$filename );
                $response[ 'status' ]  = 'completed';
                echo json_encode( $response );exit;
            }else{
                $error = "Please Insert Json format";
            }
        }
    }

    public function smart_remaining_ajax_handler(){
        global $wpdb;
        // Get File here
        $file_name = get_option( 'temp_json_feed' );
        $folder_path = APP_PATH.'uploads/';
        $file_parth = $folder_path.$file_name;
        $json_feed_string     = file_get_contents($file_parth );
        $json_array = json_decode( $json_feed_string, true , JSON_UNESCAPED_SLASHES);
        // echo"<pre>";print_r( $json_array ); exit;
        $current_index =  $_REQUEST[ 'index' ];
        $total_deleted_items  = 0;
        for( $i=$current_index; $i<$current_index+30; $i++ ){
            //if( !isset($json_array[$i]) ) continue;
            //echo '<pre>'; print_r( $json_array[$i]);exit;
            if( isset( $json_array[$i]  )){
                $import_status=$this->save_json_listing($json_array[$i]);
                $total_deleted_items++;
            }
           // echo "<pre>";print_r($import_status);exit;
        }

        $current_index+= 30;

        if($import_status){
            $response[ 'status' ]  = "Feed Imported";
            $response[ 'current_index' ]  = $current_index;
            $response[ 'total_deleted_items' ]  = $total_deleted_items;
        }else{
            $response[ 'status' ]  = "Feed not Imported";
            $response[ 'total_deleted_items' ]  = $total_deleted_items;
        }

        echo json_encode( $response );exit;
    }
    public function save_json_listing($strshow){
        global $wpdb;
        $table_name = $wpdb->prefix."rejection_listing";

        if(!empty($strshow) && isset($_REQUEST['selectcategory'])) {
                if(!empty($strshow['title'])) {
                    if( !$this->helper->the_slug_exists($strshow['title'],isset($_REQUEST['selectcategory'] ))) {
                        $strshow['type'] = str_replace('yoga_instructors','yoga_instructor',$strshow['type']);
                        $coordinates = $this->helper->get_geocode($strshow['address']);
                        $city_name = $this->helper->get_city_name( $coordinates['lat'],$coordinates['lng'] );

                        if($strshow['address']){
                            $paddress = $strshow['address'];
                            if($strshow['city']){
                                $paddress .= ' '.@$strshow['city'];
                            }
                            if($strshow['country']){
                                $paddress .= ' '.str_replace('US','USA',@$strshow['country']);
                            }
                        }else{
                            $paddress = $strshow['city'];
                            if(@$strshow['country']){
                                $paddress .= ', '.str_replace('US','USA',@$strshow['country']);
                            }
                        }
                        if(!$strshow['description']){
                            $strshow['description'] = $strshow['title'].' is a '.ucwords(str_replace('_',' ',$strshow['type'])).' in '.$paddress;
                        }

                        $my_post = array();
                        $my_post['post_type'] = $_REQUEST['selectcategory'];
                        $title_name = explode(',',$strshow['address']);

                        if( !empty($city_name)){
                            $my_post['post_title']   = $strshow['title'].'-'.$city_name;
                        }else{
                            $my_post['post_title']   = $strshow['title'];
                        }

                        $title  =   htmlentities(preg_replace('/[^A-Za-z0-9\-]/', '', $my_post['post_title'] ));

                        $myposts = $wpdb->get_results( $wpdb->prepare("select * from $wpdb->posts where post_title ='".$title."' AND post_type='".$_REQUEST['selectcategory']."' and post_status='publish'"));
                        $listing_json = json_encode( $strshow,true );

                        //print_r($listing_json);exit;
                        if( !empty( $myposts )){
                            $my_post['post_status']  = 'pending';
                        }else if( !isset($strshow['website']) ){
                            $my_post['post_status']  = 'pending';
                        }else if( !$strshow['website'] ){
                            $my_post['post_status']  = 'pending';
                        }else if (!filter_var($strshow['website'], FILTER_VALIDATE_URL)){
                            $my_post['post_status']  = 'pending';
                        }else{
                            $my_post['post_status']  = 'publish';
                        }

                        $listing_slug =  strtolower($this->helper->clean( sanitize_text_field( $my_post['post_title'] ) ) );
                        $is_listing_exists = get_page_by_path( $listing_slug ,OBJECT, $_REQUEST['selectcategory']);

                        if( !empty($is_listing_exists) ){   $error = "Record Already Exists!"; return false; }
                        $my_post['post_content'] =  $strshow['title'].' is a '.ucwords(str_replace('_',' ',$strshow['type'])).' in '.$paddress;
                        //echo '<pre>'; print_r( $my_post ); exit;

                        $post_id   = wp_insert_post( $my_post );
                        $cat_list  = $strshow['category'];
                        $cat       = explode(',',$cat_list);
                        $resultcat = array();
                        $parent = 0;
                        $cat_post_array = array();
                        $cat_post_array['teacher'] = 'teacher-category';
                        $cat_post_array['studio'] = 'studio-category';
                        $cat_post_array['school'] = 'school-category';
                        $cat_post_array['retreat'] = 'retreat-category';
                        if($cat_post_array[$_REQUEST['selectcategory']]){
                            $types = array($cat_post_array[$_REQUEST['selectcategory']]);
                            foreach( $types as $type ) {
                                $catInfo = get_term_by('name',$type,$cat_post_array[$_REQUEST['selectcategory']]);
                                if($catInfo && (end($resultcat)==$catInfo->parent) ) {
                                    $showcat = $catInfo->term_id;
                                }else{
                                    $showcat = wp_insert_category(array("cat_name"=>str_replace('US','USA',$type),"taxonomy"=>$cat_post_array[$_REQUEST['selectcategorys']],"category_parent"=>end($resultcat)),$wp_error);

                                    if(!$showcat){
                                        $catInfo = get_term_by('name',$categories,$cat_post_array[$_REQUEST['selectcategory']]);

                                        if(!empty($catInfo)){
                                            $showcat = $catInfo->term_id;
                                        }
                                    }
                                }
                                $parent      = $showcat;
                                $resultcat[] = $showcat;
                            }
                        }

                        foreach($cat as $showcatdata){
                            //$parent = 0;
                            if( strpos($showcatdata,">") > 0 ){
                                $categoryTrail = explode(">",$showcatdata);
                                foreach($categoryTrail as $categories){
                                    $catInfo = get_term_by('name',$categories,$cat_post_array[$_REQUEST['selectcategory']]);
                                    if(!empty($catInfo)){
                                        $showcat = $catInfo->term_id;
                                    }else{
                                        $showcat = wp_insert_category(array("cat_name"=>$categories,"taxonomy"=>$cat_post_array[$_REQUEST['selectcategory']],"category_parent"=>$parent),$wp_error);
                                        if(!$showcat){
                                            $catInfo = get_term_by('name',$categories,$cat_post_array[$_REQUEST['selectcategory']]);
                                            if(!empty($catInfo)){
                                                $showcat = $catInfo->term_id;
                                            }
                                        }
                                    }
                                    $resultcat[] = $showcat;
                                    $parent      = $showcat;
                                }
                            }else{
                                $catInfo = get_term_by('name',$categories,$cat_post_array[$_REQUEST['selectcategory']]);
                                if(!empty($catInfo)){
                                    $showcat = $catInfo->term_id;
                                }else{
                                    $showcat     = wp_insert_category(array("cat_name"=>$showcatdata,"taxonomy"=>$cat_post_array[$_REQUEST['selectcategory']],"category_parent"=>$parent),$wp_error);
                                    if(!$showcat){
                                        $catInfo = get_term_by('name',$categories,$cat_post_array[$_REQUEST['selectcategory']]);
                                        if(!empty($catInfo)){
                                            $showcat = $catInfo->term_id;
                                        }
                                    }
                                }
                                $resultcat[] = $showcat;
                            }
                        }

                        $resultcat = array_map( 'intval', $resultcat );
                        $resultcat = array_unique( $resultcat );
                        $tags_list = $strshow['tags'];
                        $tagsitem  = array($strshow['type']);
                        $tagsList  = array();
                        $tagsList_array = array();
                        $tagsList_array['teacher'] = 'teacher-tags';
                        $tagsList_array['studio'] = 'studio-tags';
                        $tagsList_array['school'] = 'school-tags';
                        $tagsList_array['retreat'] = 'retreat-tags';

                        if(!empty($tags_list) && isset($tagsList_array[$_REQUEST['selectcategory']])){
                            //$parent   = 0;
                            $tagsitem = explode(',',$tags_list);
                            foreach($tagsitem as $tags){
                                $catInfo = get_term_by('name',$tags,$tagsList_array[$_REQUEST['selectcategory']]);
                                if(!$catInfo){
                                    $showcat  = wp_insert_category(array("cat_name"=>trim($tags),"taxonomy"=>$tagsList_array[$_REQUEST['selectcategory']],"category_parent"=>$parent),$wp_error);
                                    $term_id  = $showcat->term_id;
                                }else{
                                    $term_id  = $catInfo->term_id;
                                }
                                $tagsitem[] = $term_id;
                                $tagsList[] = trim($tags);
                            }
                            wp_set_object_terms($post_id,$tagsitem,$tagsList_array[$_REQUEST['selectcategory']]);
                        }

                        /*** Adding the location field here *****/
                        $locationList_array = array();
                        $locationList_array['teacher'] = 'teacher-location';
                        $locationList_array['studio'] = 'studio-location';
                        $locationList_array['school'] = 'school-location';
                        $locationList_array['retreat'] = 'retreat-location';

                        $locationInfo = get_term_by('name',trim($city_name),$locationList_array[$_REQUEST['selectcategory']]);

                        if(!$locationInfo){
                            $showcat      = wp_insert_category(array("cat_name"=>trim($city_name),"taxonomy"=>$locationList_array[$_REQUEST['selectcategory']]),$wp_error);
                            $locationInfo = get_term_by('name',trim($city_name),$locationList_array[$_REQUEST['selectcategory']]);
                            wp_set_object_terms($post_id,array($locationInfo->term_id),$locationList_array[$_REQUEST['selectcategory']]);
                        }else{
                            wp_set_object_terms($post_id,array($locationInfo->term_id),$locationList_array[$_REQUEST['selectcategory']]);
                        

                        
                        $listing_rate = array($strshow['price']);

                        if(!empty($listing_rate)){
                            //add_post_meta($post_id,'listing_rate',$listing_rate);
                        }
                        $listing_reviewed = array();
                        add_post_meta($post_id,'listing_reviewed',$listing_reviewed);
                        $plan_id = array(14);
                        if(!empty($plan_id)){
                            add_post_meta($post_id,'plan_id',$plan_id);
                        }
                        $claimed = array(0);
                        if(!empty($claimed)){
                            add_post_meta($post_id,'claimed',$claimed);
                        }
                        // 'price'=>$strshow['price']
                        $listing_plan_data = array('menu'=>'true','announcment'=>'true','deals'=>'true','competitor_campaigns'=>'true','events'=>'true','bookings'=>'true');
                        if(!empty($claimed)){
                                add_post_meta($post_id,'claimed',$claimed);
                        }

                        /** FAQ TABS **/
                        $faq = array('title'=>array(),'description'=>array());
                        $i=1;
                        if( $strshow['specifications']['tab_1']['tab_name'] ) {
                            $faq['title'][1]      = $strshow['specifications']['tab_1']['tab_name'];
                            $content = array();
                            foreach($strshow['specifications']['tab_1']['tab_content'] as $key=>$value){
                                $contentText = (is_array($value))? implode("\r\n",$value):$value;
                                $content[]   = $key.': '.$contentText;
                            }
                            $faq['description'][1] = implode("\r\n",$content);
                        }
                        if( $strshow['specifications']['tab_2']['tab_name'] ) {
                            $faq['title'][2]      = $strshow['specifications']['tab_2']['tab_name'];
                            $content = array();
                            foreach($strshow['specifications']['tab_2']['tab_content'] as $key=>$value){
                                $contentText = (is_array($value))? implode("\r\n",$value):$value;
                                $content[]   = $key.': '.$contentText;
                            }
                            $faq['description'][2] = implode("\r\n",$content);
                        }
                        if( $strshow['specifications']['tab_3']['tab_name'] ) {
                            $faq['title'][3]      = $strshow['specifications']['tab_3']['tab_name'];
                            $content = array();
                            foreach($strshow['specifications']['tab_3']['tab_content'] as $key=>$value){
                                $contentText = (is_array($value))? implode("\r\n",$value):$value;
                                $content[]   = $key.': '.$contentText;
                            }
                            $faq['description'][3] = implode("\r\n",$content);
                        }
                        if( $strshow['specifications']['tab_4']['tab_name'] ) {
                            $faq['title'][4]      = $strshow['specifications']['tab_4']['tab_name'];
                            $content = array();
                            foreach($strshow['specifications']['tab_4']['tab_content'] as $key=>$value){
                                $contentText = (is_array($value))? implode("\r\n",$value):$value;
                                $content[]   = $key.': '.$contentText;
                            }
                            $faq['description'][4] = implode("\r\n",$content);
                        }

                        if( $strshow['payment_methods'] ) {
                            add_post_meta($post_id,'payment_methods',$strshow['payment_methods']);
                        }
                        ///$priceRange = explode('-',$strshow['price']);
                        $lp_listingpro_options = array(
                                                'tagline_text'=> $strshow['title'].' is a '.ucwords(str_replace('_',' ',$strshow['type'])).' in '.$paddress,
                                                'gAddress'=>$paddress,
                                                'latitude'=>$coordinates['lat'],
                                                'longitude'=>$coordinates['lng'],
                                                'mappin'=>'true',
                                                'phone'=>$strshow['phone'],
                                                'whatsapp'=>'',
                                                'email'=>$strshow['mail'],
                                                'website'=>urldecode($strshow['website']),
                                                //'twitter'=>'https://twitter.com',
                                                //'facebook'=>'https://facebook.com',
                                                //'linkedin'=>'https://linkedin.com',
                                                'youtube'=>'',
                                                'instagram'=>'',
                                                'video'=>'',
                                                'gallery'=>'',
                                                //'price_status'=>'notsay',
                                                //'list_price'=>@$priceRange[0],
                                                //'list_price_to'=>@$priceRange[1],
                                                'Plan_id'=>0,
                                                'lp_purchase_days'=>'',
                                                'reviews_ids'=>'',
                                                'claimed_section'=>'',
                                                'listings_ads_purchase_date'=>'',
                                                'listings_ads_purchase_packages'=>'',
                                                'faqs'=>array('faq'=>$faq['title'],'faqans'=>$faq['description']),
                                                //'business_hours'=>$strshow['work_hours'],
                                                'campaign_id'=>'',
                                                'changed_planid'=>'',
                                                'listing_reported_by'=>'',
                                                'listing_reported'=>'',
                                                'business_logo'=>''
                                );
                            $lp_listingpro_options[ 'listing_title' ]  = $strshow['title'];

                            if(!empty($lp_listingpro_options)){
                                add_post_meta($post_id,'lp_listingpro_options',$lp_listingpro_options);
                            }

                            if( $strshow['other_images'] ) {
                            }
                            $gallery_image_ids = '1799,1798,1797,1796,1795';
                            if(!empty($gallery_image_ids)){
                                add_post_meta($post_id,'gallery_image_ids',$gallery_image_ids);
                            }
                            $lp_listingpro_options_fields = array(array('lp_feature'=>array(28,32)));
                            if(!empty($lp_listingpro_options_fields)){
                                add_post_meta($post_id,'lp_listingpro_options_fields',$lp_listingpro_options_fields);
                            }
                            $campaign_status = array('active');
                            if(!empty($campaign_status)){
                                add_post_meta($post_id,'campaign_status',$campaign_status);
                            }
                            $lp_random_ads = array('active');
                            if(!empty($lp_random_ads)){
                                add_post_meta($post_id,'lp_random_ads',$lp_random_ads);
                            }
                            $lp_detail_page_ads = array('active');
                            if(!empty($lp_detail_page_ads)){
                                add_post_meta($post_id,'lp_detail_page_ads',$lp_detail_page_ads);
                            }
                            $lp_top_in_search_page_ads = array('active');
                            if(!empty($lp_top_in_search_page_ads)){
                                add_post_meta($post_id,'lp_top_in_search_page_ads',$lp_top_in_search_page_ads);
                            }
                            $_thumbnail_id = array(123);
                            if(!empty($_thumbnail_id)){
                                add_post_meta($post_id,'_thumbnail_id',$_thumbnail_id);
                            }
                            $post_views_count = array(0);
                            if(!empty($post_views_count)){
                                    add_post_meta($post_id,'post_views_count',$post_views_count);
                            }
                            $response = wp_set_object_terms($post_id, $resultcat,$cat_post_array[$_REQUEST['selectcategory']] );
                            //echo "<pre>";print_r($response);die('hello');

                            //  Set Template
                            //add_post_meta( $post_id, '_wp_page_template', 'custom-listing.php' );
                            //add_post_meta( $post_id, '_wp_page_template', 'custom-studio.php' );
                            /** importing the image here ***/
                            add_post_meta ( $post_id , 'listing_json', json_encode( $strshow ) );
                            $this->helper->set_featured_image($post_id,$strshow['main_image']);
                            $msg = "Data imported successfully";
                    }
                }
            //}
            return true;
        }else{
            return false;
            $error = "File Contains invalid Json format";
        }
        return false;
    }

    public function delete_all_listing_records(){
        $args=array('post_type'=>array('teacher','studio','school','retreat'),'numberposts'=>-1);
        $allposts= get_posts($args);
        if( count( $allposts ) > 0 ){
            foreach($allposts as $eachpost){
                wp_delete_post( $eachpost->ID, true );
            }
        }
    }

    public function delete_blank_listing_records(){
        global $wpdb;
        $args=array('post_type'=>array('teacher','studio','school','retreat'),'numberposts'=>-1,'fields'=> 'ID');
        $allListing = get_posts($args);
        $table_name = $wpdb->prefix."rejection_listing";

        if(!empty($allListing)){
            foreach($allListing as $post){
                $post_id = $post->ID;
                $posttitle = $post->post_title;
                $myposts = $wpdb->get_results( $wpdb->prepare("select * from $wpdb->posts where post_title ='".$posttitle."'AND ID!='".$post_id."'"));
                if( !empty( $myposts )){
                    foreach( $myposts as $delete_post  ){
                        wp_delete_post( $delete_post->ID, true );
                    }
                }
                $website = $listing_json['website'];

                if(!$website){
                    wp_delete_post( $post_id, true );
                }
            }

            $responses['posts'] = get_post_meta( $post->ID, 'lp_listingpro_options', true );
            if(!empty($responses)){
                foreach($responses as $response){
                    $json_string = json_encode($response);
                    $folder_path = APP_PATH.'uploads/'.'file.json';
                    $file_path = $folder_path;
                }
            }
        }
    }
}

new WPLI_Admin( 'JSON Feed Importer','1.0.0' );