<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    WPLI
 * @subpackage WPLI/admin
 * @author     Rohit Sharma
 */

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

class WPLI_Admin {
    private $plugin_name,$version,$helper,$comment_list;

    public function __construct( $WPLI, $version ) {
        $this->plugin_name = $WPLI;
        $this->version = $version;
        $this->helper = new listing_importer_helper();
        //$this->comment_list = new Custom_Table_Comments_Example_List_Table( $this->screen->post_type);

        add_action( 'wp_ajax_nopriv_get_smartimportteacher', array( $this,'smart_import_ajax_handler' ));
        add_action( 'wp_ajax_get_smartimportteacher',  array( $this,'smart_import_ajax_handler' ));
        add_action( 'wp_ajax_nopriv_get_smartremainingrecords',  array( $this,'smart_remaining_ajax_handler' ));
        add_action( 'wp_ajax_get_smartremainingrecords',  array( $this,'smart_remaining_ajax_handler' ));
        add_action( 'wp_ajax_nopriv_delete_all',  array( $this,'delete_all_listing_records' ));
        add_action( 'wp_ajax_delete_all',  array( $this,'delete_all_listing_records' ));
        add_action( 'wp_ajax_nopriv_delete_media_files',  array( $this,'delete_media_attachments_records' ));
        add_action( 'wp_ajax_delete_media_files',  array( $this,'delete_media_attachments_records' ));
        add_action( 'wp_ajax_nopriv_get_exportall',  array( $this,'exportall_handler' ));
        add_action( 'wp_ajax_get_exportall',  array( $this,'exportall_handler' ));
        add_action( 'wp_ajax_nopriv_getTheContent',  array( $this,'getTheContent_handler' ));
        add_action( 'wp_ajax_getTheContent',  array( $this,'getTheContent_handler' ));
        add_action( 'wp_ajax_nopriv_delete_blank_listing',  array( $this,'delete_blank_listing_records' ));
        add_action( 'wp_ajax_delete_blank_listing',  array( $this,'delete_blank_listing_records' ));
        add_action( 'wp_ajax_nopriv_save_comments_listing',  array( $this,'save_comments_listing_records' ));
        add_action( 'wp_ajax_save_comments_listing',  array( $this,'save_comments_listing_records' ));
        add_action( 'wp_ajax_nopriv_update_comments_listing',  array( $this,'update_comments_listing_records' ));
        add_action( 'wp_ajax_update_comments_listing',  array( $this,'update_comments_listing_records' ));
        add_action( 'wp_ajax_nopriv_delete_comments_listing',  array( $this,'delete_comments_listing_records' ));
        add_action( 'wp_ajax_delete_comments_listing',  array( $this,'delete_comments_listing_records' ));
        //add_action( 'admin_init', array($this,'custom_teacher_comments_listing_page_handler' ));
        add_action( 'admin_menu', array($this, 'listing_admin_menu' ));
        add_filter('post_row_actions',array($this,'comments_action_row'),10, 2);
        add_action('admin_enqueue_scripts', array($this,'cstm_css_and_js'));
        add_action('admin_init', array($this,'wpli_listingpro_settings_box'));
        add_action('admin_init', array($this,'wpli_listingpro_settings_formfields'));
        add_action('admin_print_scripts', array($this,'wpli_postsettings_admin_scripts'));
        add_action('admin_print_styles', array($this,'wpli_postsettings_admin_styles'));
        add_action('save_post', array($this,'wpli_savePostMeta'));
        add_filter('menu_order', array($this,'wpli_admin_menu_order'));
        add_action( 'wp_ajax_nopriv_website_checking_listing',  array( $this,'website_checking_listing_records' ));
        add_action( 'wp_ajax_website_checking_listing',  array( $this,'website_checking_listing_records' ));
    }


    public function delete_media_attachments_records(){
        $attachments = get_posts( array('post_type'=>'attachment','numberposts'=>-1) );
        if(!empty($attachments)){
            foreach($attachments as $attachment){
                //echo "<pre>";print_r($attachment);exit;
                $eachattachment = get_post_meta($attachment->ID,'_thumbnail_id', true);
                if($eachattachment){
                    wp_delete_attachment($eachattachment,true );
                }
                wp_delete_post( $attachment->ID, true );
                echo 'Deleted = '.$attachment->ID;
                echo '<br/>';
            }
        }
    }


    public function website_checking_listing_records(){
        global $wpdb;
        $resposne =  array();

        //echo "<pre>";print_r($_REQUEST['selectcategorychecker']);exit;
        if(isset($_REQUEST['selectcategorychecker'])){
            $allListing = explode(",",$_REQUEST['selectcategorychecker']);
            $exportargs = array(
                'post_type' =>$allListing,
                'post_status' => 'all',
                'posts_per_page' => -1,
                'nopaging' => true,
            );

            $listingData = get_posts( $exportargs );
            $response =  array();
            if(!empty($listingData)){
                foreach($listingData as $listing ){
                    $temp_array  = array();
                    //echo"<pre>";print_r($listing); echo "</pre>";  exit;
                    //echo "select * from $wpdb->posts where post_type='".$exportchecking."' and post_status='publish'";exit;
                    //echo $listing->ID;
                    //$post_id,'lp_listingpro_options',$lp_listingpro_options
                    $listing_website =  get_post_meta( $listing->ID, 'lp_listingpro_options' , true  );
                    //echo "<pre>";print_r($listing_website);exit;
                    //exit;
                    if( !isset( $listing_website['website'] ) || empty( $listing_website['website'] )){
                        $listing->post_status  = 'pending';
                        $temp_array[ 'type' ] = $listing->post_type;
                        $temp_array[ 'title' ] = $listing->post_title;
                        $temp_array[ 'url' ] = get_permalink( $listing->ID );
                    }else if (!filter_var($listing_website['post_type'], FILTER_VALIDATE_URL)){
                        $temp_array[ 'type' ] = $listing->post_type;
                        $temp_array[ 'title' ] = $temp_array[ 'title' ] = $listing->post_title;
                        $temp_array[ 'url' ] = get_permalink( $listing->ID );
                    }

                    $response[] = $temp_array;
                }
            }
        }

        echo json_encode( $response ); exit;

    }


    public function wpli_admin_menu_order( $menu_order ) {
        //echo "<pre>";print_r($menu_order);exit;
        $new_positions = array(
            'file_upload' => 14,
            'listingpro'=>  5
        );

        function move_element(&$array, $a, $b) {
            $out = array_splice($array, $a, 1);
            array_splice($array, $b, 0, $out);
        }

        move_element($menu_order, 0, 13);
        return $menu_order;
    }
	public function wpli_savePostMeta($post_id) {
		global $listingpro_settings, $reviews_options, $listingpro_formFields, $claim_options, $ads_options, $page_options, $post_options, $price_plans_options;

		$meta = 'lp_'.strtolower(THEMENAME).'_options';
        //print_r($meta);exit;

		// verify nonce
		if (!isset($_POST['lp_meta_box_nonce']) || !wp_verify_nonce($_POST['lp_meta_box_nonce'], basename(__FILE__))) {
				return $post_id;
		}

		// check autosave
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return $post_id;
		}
		// check permissions
		if ('page' == $_POST['post_type']) {
				if (!current_user_can('edit_page', $post_id)) {
						return $post_id;
				}
		} elseif (!current_user_can('edit_post', $post_id)) {
				return $post_id;
		}

		if($_POST['post_type']=='lp-reviews'){
			$metaboxes_reviews = $reviews_options;

		}

		if($_POST['post_type']=='lp-ads'){
			$metaboxes = $ads_options;
		}

		if($_POST['post_type']=='listing'){
			$metaboxes = $listingpro_settings;
		}

        $post_type_array = array();
        $post_type_array[] = 'teacher';
        $post_type_array[] = 'studio';
        $post_type_array[] = 'school';
        $post_type_array[] = 'retreat';
        $post_type_array[] = 'restaurant';
        if( in_array($_POST['post_type'],$post_type_array ) ){
			$metaboxes = $listingpro_settings;
		}

		if($_POST['post_type']=='form-fields'){
			$metaboxes = $listingpro_formFields;
		}
		if($_POST['post_type']=='lp-claims'){
			$metaboxes = $claim_options;
		}
		if($_POST['post_type']=='page'){
			$metaboxes = $page_options;
		}
		if($_POST['post_type']=='post'){
			$metaboxes = $post_options;
		}
		if($_POST['post_type']=='price_plan'){
			$metaboxes = $price_plans_options;
		}
		if(!empty($metaboxes_reviews)) {
			$myMeta = array();

			foreach ($metaboxes_reviews as $metabox) {
				$myMeta[$metabox['id']] = isset($_POST[$metabox['id']]) ? $_POST[$metabox['id']] : "";
			}

			update_post_meta($post_id, $meta, $myMeta);

		}

		if(!empty($metaboxes)) {
			$myMeta = array();

			foreach ($metaboxes as $metabox) {
				$myMeta[$metabox['id']] = isset($_POST[$metabox['id']]) ? $_POST[$metabox['id']] : "";
			}

			update_post_meta($post_id, $meta, $myMeta);
			if(isset($_POST['lp_form_fields_inn'])){
				$metaFields = 'lp_'.strtolower(THEMENAME).'_options_fields';
				$fields = $_POST['lp_form_fields_inn'];
				$filterArray = lp_save_extra_fields_in_listing($fields, $post_id);
				$fields = array_merge($fields,$filterArray);

				update_post_meta($post_id, $metaFields, $fields);
			}else{
				$metaFields = 'lp_'.strtolower(THEMENAME).'_options_fields';
				update_post_meta($post_id, $metaFields, '');
			}
		}
	}

    public function wpli_postsettings_admin_scripts(){
        //echo "<pre>";print_r('hello world!@!!!!!!!');exit;
        global $post,$pagenow;

        if (current_user_can('edit_posts') && ($pagenow == 'post-new.php' || $pagenow == 'post.php' )) {
			    if( isset($post) ) {
					wp_localize_script( 'jquery', 'script_data', array(
						'post_id' => $post->ID,
						'nonce' => wp_create_nonce( 'lp-ajax' ),
						'image_ids' => get_post_meta( $post->ID, 'gallery_image_ids', true ),
						'label_create' => __("Create Featured Gallery", "listingpro-plugin"),
						'label_edit' => __("Edit Featured Gallery", "listingpro-plugin"),
						'label_save' => __("Save Featured Gallery", "listingpro-plugin"),
						'label_saving' => __("Saving...", "listingpro-plugin")
					));
				}


            //wp_register_script('post-metaboxes', plugins_url( 'assets/js/metaboxes.js', dirname(dirname(__FILE__) )));
            wp_register_script( 'post-metaboxes', plugins_url( 'public/assets/js/metaboxes.js', APP_RELPATH ) );

            wp_enqueue_script('post-metaboxes');
			global $listingpro_options;
			$mapAPI = '';
			$mapAPI = $listingpro_options['google_map_api'];
			if(empty($mapAPI)){
				$mapAPI = 'AIzaSyDQIbsz2wFeL42Dp9KaL4o4cJKJu4r8Tvg';
			}
			wp_enqueue_script('maps', 'https://maps.googleapis.com/maps/api/js?key='.$mapAPI.'&libraries=places', 'jquery', '', false);

			if (current_user_can('edit_posts') && ($pagenow == 'edit.php' && isset($_GET['page']) && $_GET['page'] == 'listing-claims')) {
				wp_enqueue_script('bootstrapadmin', get_template_directory_uri() . '/assets/lib/bootstrap/js/bootstrap.min.js', 'jquery', '', true);
			}
            $post_type_array = array();
            $post_type_array[] = 'listing';
            $post_type_array[] = 'teacher';
            $post_type_array[] = 'studio';
            $post_type_array[] = 'school';
            $post_type_array[] = 'retreat';
            $post_type_array[] = 'restaurant';
			if (current_user_can('edit_posts') && ($pagenow == 'post-new.php' || $pagenow == 'post.php')) {
				if ( in_array($post->post_type,$post_type_array) || 'lp-reviews' === $post->post_type   || 'lp-ads' === $post->post_type || 'lp-claims' === $post->post_type || 'events' == $post->post_type ) {
					//wp_enqueue_script('accorianui', plugins_url( 'assets/js/jqueryuiaccordian.js', dirname(dirname(__FILE__) )), 'jquery', '', true);
					wp_enqueue_script('jquery-ui', get_template_directory_uri() . '/assets/js/jquery-ui.js', 'jquery', '', true);
                   // wp_enqueue_script('jquery-ui-trigger', get_template_directory_uri() . '/assets/js/jquery-ui-trigger.js', 'jquery', '', true);
                    wp_localize_script(	'jquery-ui-trigger','global',array('ajax' => admin_url( 'admin-ajax.php' ),));
					wp_enqueue_script('jquery-droppin', get_template_directory_uri() . '/assets/js/drop-pin.js', 'jquery', '', true);
				}

			}

        }
    }

    function wpli_postsettings_admin_styles(){
        global $pagenow;
        if (current_user_can('edit_posts') && ($pagenow == 'post-new.php' || $pagenow == 'post.php' || $pagenow == 'edit-tags.php'|| $pagenow == 'term.php' )) {
            //wp_register_style('post-metaboxes', plugins_url( 'assets/css/metaboxes.css', dirname(dirname(__FILE__) )), false, '1.00', 'screen');
            wp_register_style( 'post-metaboxes', plugins_url( 'public/assets/css/metaboxes.css', APP_RELPATH ) );


			// wp_register_style('select2-metaboxes', plugins_url( 'assets/css/select2.css', dirname(dirname(__FILE__) )), false, '1.00', 'screen');
           // wp_register_style('prettify-metaboxes', plugins_url( 'assets/css/prettify.css', dirname(dirname(__FILE__) )), false, '1.00', 'screen');
            wp_enqueue_style('post-metaboxes');
			 //wp_enqueue_style('select2-metaboxes');
            //wp_enqueue_style('prettify-metaboxes');

        }

		if (current_user_can('edit_posts') && ($pagenow == 'edit.php' && isset($_GET['page']) && $_GET['page'] == 'listing-claims')) {
			wp_enqueue_style('bootstrapcss', get_template_directory_uri() . '/assets/lib/bootstrap/css/bootstrap.min.css');
		}
		if (current_user_can('edit_posts') && ($pagenow == 'post-new.php' || $pagenow == 'post.php')) {
					wp_enqueue_style('jquery-ui');

		}
    }
    public function wpli_listingpro_settings_box() {
        global $listingpro_settings;
        $listingpro_settings = Array(
            Array(
                'name' => esc_html__('Business Tagline Text', 'listingpro-plugin'),
                'id' => 'tagline_text',
                'type' => 'text',
                'desc' => 'Your Business One liner'),

            Array(
                'name' => esc_html__('Google Address', 'listingpro-plugin'),
                'id' => 'gAddress',
                'type' => 'text',
                'desc' => 'Google address for map'),
            Array(
                'name' => esc_html__('Latitude', 'listingpro-plugin'),
                'id' => 'latitude',
                'type' => 'text',
                'desc' => ''),
            Array(
                'name' => esc_html__('Longitude', 'listingpro-plugin'),
                'id' => 'longitude',
                'type' => 'text',
                'desc' => ''),

            Array(
                'name' => esc_html__('Map Pin', 'listingpro-plugin'),
                'id' => 'wpli-mappin',
                'type' => 'mappinbuton',
                'desc' => ''),

            Array(
                'name' => esc_html__('Phone', 'listingpro-plugin'),
                'id' => 'phone',
                'type' => 'text',
                'desc' => ''),
            /*
            Array(
                'name' => esc_html__('Whatsapp', 'listingpro-plugin'),
                'id' => 'whatsapp',
                'type' => 'text',
                'desc' => ''),
            */
            Array(
                'name' => esc_html__('Email', 'listingpro-plugin'),
                'id' => 'email',
                'type' => 'text',
                'desc' => 'This Email is not for public'),
            Array(
                'name' => esc_html__('Website', 'listingpro-plugin'),
                'id' => 'website',
                'type' => 'text',
                'desc' => 'Your website URL'),
            Array(
                'name' => esc_html__('Image Gallery', 'listingpro-plugin'),
                'id' => 'gallery',
                'type' => 'gallery',
                'desc' => esc_html__('Select images to present your buisness online.', 'listingpro-plugin'),
            ),
            /*
            Array(
                'name' => esc_html__('Twitter', 'listingpro-plugin'),
                'id' => 'twitter',
                'type' => 'text',
                'desc' => 'Your twitter URL'),
            Array(
                'name' => esc_html__('Facebook', 'listingpro-plugin'),
                'id' => 'facebook',
                'type' => 'text',
                'desc' => 'Your facebook URL'),
            Array(
                'name' => esc_html__('LinkedIn', 'listingpro-plugin'),
                'id' => 'linkedin',
                'type' => 'text',
                'desc' => 'Your Linkedin URL'),

            Array(
                'name' => esc_html__('Youtube Channel Link', 'listingpro-plugin'),
                'id' => 'youtube',
                'type' => 'text',
                'desc' => esc_html__('Your Youtube Channel URL as social link', 'listingpro-plugin'),
            ),
            Array(
                'name' => esc_html__('Instagram Profile Link', 'listingpro-plugin'),
                'id' => 'instagram',
                'type' => 'text',
                'desc' => esc_html__('Your Instagram Profile URL as social link', 'listingpro-plugin'),
            ),
            Array(
                'name' => esc_html__('Youtube Video URL', 'listingpro-plugin'),
                'id' => 'video',
                'type' => 'text',
                'desc' => esc_html__('Any specific Youtube Video? You want to share on business page', 'listingpro-plugin'),
            ),


            Array(
                'name' => esc_html__('Show Price Status', 'listingpro-plugin'),
                'id' => 'price_status',
                'type' => 'select',
                'std' => 'Price Range',
                'options' => array(
                    'notsay' => 'Prefer not to say',
                    'inexpensive' => ''.$lp_priceSymbol.' Inexpensive',
                    'moderate' => ''.$lp_priceSymbol2.' Moderate',
                    'pricey' => ''.$lp_priceSymbol3.' Pricey',
                    'ultra_high_end' => ''.$lp_priceSymbol4.' Ultra High-End',
                ),
                'desc' => esc_html__('It will show your business price range', 'listingpro-plugin')
            ),

            Array(
                'name' => esc_html__('Price From', 'listingpro-plugin'),
                'id' => 'list_price',
                'type' => 'Text',
                'desc' => esc_html__('Ignore this if your buisness does not have any specific price to show', 'listingpro-plugin')
            ),
            Array(
                'name' => esc_html__('Price To', 'listingpro-plugin'),
                'id' => 'list_price_to',
                'type' => 'Text',
                'desc' => esc_html__('Ignore this if your buisness does not have any specific price to show', 'listingpro-plugin')
            ),
            Array(
                'name' => esc_html__('Plans', 'listingpro-plugin'),
                'id' => 'Plan_id',
                'type' => 'select',
                'options' => $priceplans,
                'desc' => esc_html__('Ignore this if this post will not be a paid one', 'listingpro-plugin')
            ),
            */
            Array(
                'name' => esc_html__('Listing\'s Duration', 'listingpro-plugin'),
                'id' => 'lp_purchase_days',
                'type' => 'hidden',
                'default' => '',
                'desc' => esc_html__('This is Listing Duration which were associated with plan', 'listingpro-plugin')
            ),
            /*
            Array(
                'name' => esc_html__('Reviews', 'listingpro-plugin'),
                'id' => 'reviews_ids',
                'type' => 'array',
                'desc' => ''),
            */
            Array(
                'name' => esc_html__('Verify Listing', 'listingpro-plugin'),
                'id' => 'claimed_section',
                'type' => 'select',
                'std' => 'Verify Listing',
                'options' => array(
                    'not_claimed' => 'Not Claimed',
                    'claimed' => 'Claimed'
                ),
                'default'=> 'not_claimed',
                'desc' => esc_html__('Approve claim at claim section this will override complete claim process', 'listingpro-plugin')
            ),
            /*

            Array(
                'name' => esc_html__('Ad Purchased on', 'listingpro-plugin'),
                'id' => 'listings_ads_purchase_date',
                'type' => 'hidden',
                'default'	=>'',
                'desc' => ''),
            Array(
                'name' => esc_html__('Ad Purchased Packages', 'listingpro-plugin'),
                'id' => 'listings_ads_purchase_packages',
                'type' => 'hidden',
                'default'	=>'',
                'desc' => ''),
            Array(
                'name' => esc_html__('Faq', 'listingpro-plugin'),
                'id' => 'wali-faqs',
                'type' => 'faqs',
                'desc' => ''),
            Array(
                'name' => esc_html__('Timings', 'listingpro-plugin'),
                'id' => 'business_hours',
                'type' => 'timings',
                'desc' => esc_html__('Set Your business time details', 'listingpro-plugin')
            ),
            Array(
                'name' => esc_html__('Copmain ID', 'listingpro-plugin'),
                'id' => 'campaign_id',
                'type' => 'hidden',
                'desc' => '',
            ),
            Array(
                'name' => esc_html__('Changed Plan ID', 'listingpro-plugin'),
                'id' => 'changed_planid',
                'type' => 'hidden',
                'desc' => '',
            ),

            Array(
                'name' => esc_html__('Reported By', 'listingpro-plugin'),
                'id' => 'listing_reported_by',
                'type' => 'hidden',
                'default' => '0',
                'desc' => ''),

            Array(
                'name' => esc_html__('Reported Count', 'listingpro-plugin'),
                'id' => 'listing_reported',
                'type' => 'hidden',
                'default' => '0',
                'desc' => ''),
            */


        );

        $b_logo =   lp_theme_option('business_logo_switch');
        if( $b_logo == 1 )
        {
            $listingpro_settings[]  =   Array(
                'name' => esc_html__('Business Logo', 'listingpro-plugin'),
                'id' => 'business_logo',
                'type' => 'file',
                'desc' => esc_html__('Select business logo for your listing.', 'listingpro-plugin'),
            );
        }


        // $post_type_array = array();
        // $post_type_array[] = 'listing';
        // $post_type_array[] = 'teacher';
        // $post_type_array[] = 'studio';
        // $post_type_array[] = 'school';
        // $post_type_array[] = 'retreat';
        //echo "<pre>";print_r($post);exit;
        add_meta_box('teacher_meta_settings',esc_html__( 'Listing settings', 'listingpro-plugin' ),array($this,'listingteacherpro_metabox_render'),'teacher','normal','high',$listingpro_settings);
        add_meta_box('studio_meta_settings',esc_html__( 'listing settings', 'listingpro-plugin' ),array($this,'listingstudiopro_metabox_render'),'studio','normal','high',$listingpro_settings);
        add_meta_box('school_meta_settings',esc_html__( 'listing settings', 'listingpro-plugin' ),array($this,'listingschoolpro_metabox_render'),'school','normal','high',$listingpro_settings);
        add_meta_box('retreat_meta_settings',esc_html__( 'listing settings', 'listingpro-plugin' ),array($this,'listingretreatpro_metabox_render'),'retreat','normal','high',$listingpro_settings);
        add_meta_box('restaurant_meta_settings',esc_html__( 'Restaurant settings', 'listingpro-plugin' ),array($this,'listingrestaurantpro_metabox_render'),'restaurant','normal','high',$listingpro_settings);

    }



    public function listingteacherpro_metabox_render($post, $metabox) {
        //echo '<pre>'; print_r( $metabox ); echo '</pre>';
        global $post;
        $options = get_post_meta($post->ID, 'lp_'.strtolower(THEMENAME).'_options', true);
        ?>
        <input type="hidden" name="lp_meta_box_nonce" value="<?php echo wp_create_nonce(basename(__FILE__));?>" />
        <table class="form-table lp-metaboxes">
            <tbody>
            <?php
            if( !empty($metabox['args'])){
                foreach ($metabox['args'] as $settings) {
                    $settings['value'] = isset($options[$settings['id']]) ? $options[$settings['id']] : (isset($settings['std']) ? $settings['std'] : '');
                    call_user_func('settings_'.$settings['type'], $settings);
                }
            }
            ?>
            </tbody>
        </table>
        <?php
    }

    public function listingstudiopro_metabox_render($post, $metabox) {
        //echo '<pre>'; print_r( $metabox ); echo '</pre>';
        global $post;
        $options = get_post_meta($post->ID, 'lp_'.strtolower(THEMENAME).'_options', true);
        ?>
        <input type="hidden" name="lp_meta_box_nonce" value="<?php echo wp_create_nonce(basename(__FILE__));?>" />
        <table class="form-table lp-metaboxes">
            <tbody>
            <?php
            if( !empty($metabox['args'])){
                foreach ($metabox['args'] as $settings) {
                    $settings['value'] = isset($options[$settings['id']]) ? $options[$settings['id']] : (isset($settings['std']) ? $settings['std'] : '');
                    call_user_func('settings_'.$settings['type'], $settings);
                }
            }
            ?>
            </tbody>
        </table>
        <?php
    }
    public function listingretreatpro_metabox_render($post, $metabox) {
        //echo '<pre>'; print_r( $metabox ); echo '</pre>';
        global $post;
        $options = get_post_meta($post->ID, 'lp_'.strtolower(THEMENAME).'_options', true);
        ?>
        <input type="hidden" name="lp_meta_box_nonce" value="<?php echo wp_create_nonce(basename(__FILE__));?>" />
        <table class="form-table lp-metaboxes">
            <tbody>
            <?php
            if( !empty($metabox['args'])){
                foreach ($metabox['args'] as $settings) {
                    $settings['value'] = isset($options[$settings['id']]) ? $options[$settings['id']] : (isset($settings['std']) ? $settings['std'] : '');
                    call_user_func('settings_'.$settings['type'], $settings);
                }
            }
            ?>
            </tbody>
        </table>
        <?php
    }
    public function listingschoolpro_metabox_render($post, $metabox) {
        //echo '<pre>'; print_r( $metabox ); echo '</pre>';
        global $post;
        $options = get_post_meta($post->ID, 'lp_'.strtolower(THEMENAME).'_options', true);
        ?>
        <input type="hidden" name="lp_meta_box_nonce" value="<?php echo wp_create_nonce(basename(__FILE__));?>" />
        <table class="form-table lp-metaboxes">
            <tbody>
            <?php
            if( !empty($metabox['args'])){
                foreach ($metabox['args'] as $settings) {
                    $settings['value'] = isset($options[$settings['id']]) ? $options[$settings['id']] : (isset($settings['std']) ? $settings['std'] : '');
                    call_user_func('settings_'.$settings['type'], $settings);
                }
            }
            ?>
            </tbody>
        </table>
        <?php
    }
    public function listingrestaurantpro_metabox_render($post, $metabox) {
        //echo '<pre>'; print_r( $metabox ); echo '</pre>';
        global $post;
        $options = get_post_meta($post->ID, 'lp_'.strtolower(THEMENAME).'_options', true);
        ?>
        <input type="hidden" name="lp_meta_box_nonce" value="<?php echo wp_create_nonce(basename(__FILE__));?>" />
        <table class="form-table lp-metaboxes">
            <tbody>
            <?php
            if( !empty($metabox['args'])){
                foreach ($metabox['args'] as $settings) {
                    $settings['value'] = isset($options[$settings['id']]) ? $options[$settings['id']] : (isset($settings['std']) ? $settings['std'] : '');
                    call_user_func('settings_'.$settings['type'], $settings);
                }
            }
            ?>
            </tbody>
        </table>
        <?php
    }


    public function wpli_listingpro_settings_formfields() {
        global $listingpro_formFields;
        $showInFilter = Array(
            'name'          => '',
            'id'            => 'lp-showin-filter',
            'type'          => 'hidden',
            'desc' => ''
        );
        $showFieldsInFilter = lp_theme_option('enable_extrafields_filter');
        if(!empty($showFieldsInFilter)){
            $showInFilter = Array(
                'name'          => __( 'Show in Filter', 'listingpro-plugin' ),
                'id'            => 'lp-showin-filter',
                'type'          => 'select',
                'child_of'=> '',
                'options'=>array(
                    'notshowinfilter'=> __('No', 'listingpro-plugin'),
                    'displaytofilt'=> __('Yes', 'listingpro-plugin'),
                ),
                'desc' => ''
            );
        }
        $listingpro_formFields = Array(


            Array(
                'name'          => __( 'Field Type', 'listingpro-plugin' ),
                'id'            => 'field-type',
                'type'          => 'select',
                'child_of'=> '',
                'options'       => array(
                    'text'                              => __( 'Text', 'listingpro-plugin' ),
                    //'textarea'                          => __( 'Textarea', 'listingpro-plugin' ),
                    //'wysiwyg'                           => __( 'TinyMCE wysiwyg editor', 'listingpro-plugin' ),
                    /* 			'text_time'                         => __( 'Time picker', 'listingpro-plugin' ),
                                'select_timezone'                   => __( 'Timezone', 'listingpro-plugin' ),
                                'text_date_timestamp'               => __( 'Date', 'listingpro-plugin' ),
                                'text_datetime_timestamp'           => __( 'Date and time', 'listingpro-plugin' ),
                                'text_datetime_timestamp_timezone'  => __( 'Date, time and timezone', 'listingpro-plugin' ), */
                    //'color'                       => __( 'Colorpicker', 'listingpro-plugin' ),
                    'check'                          => __( 'Checkbox', 'listingpro-plugin' ),
                    'checkbox'                          => __( 'Checkbox (Switch On/Off)', 'listingpro-plugin' ),
                    'checkboxes'                        => __( 'Multicheck', 'listingpro-plugin' ),
                    'radio'                             => __( 'Radio', 'listingpro-plugin' ),
                    'select'                            => __( 'Drop-Down', 'listingpro-plugin' ),
                ),
                'desc' => ''
            ),
            Array(
                'name'          => __( 'Radio Options', 'listingpro-plugin' ),
                'id'            => 'radio-options',
                'type'          => 'textarea',
                'child_of'=> 'field-type',
                'match'=>'radio',
                'desc' => 'Comma separated options if type support choices'
            ),
            Array(
                'name'          => __( 'Select Options', 'listingpro-plugin' ),
                'id'            => 'select-options',
                'type'          => 'textarea',
                'child_of'=> 'field-type',
                'match'=>'select',
                'desc' => 'Comma separated options if type support choices'
            ),
            Array(
                'name'          => __( 'Multicheck Options', 'listingpro-plugin' ),
                'id'            => 'multicheck-options',
                'type'          => 'textarea',
                'child_of'=> 'field-type',
                'match'=>'checkboxes',
                'desc' => 'Comma separated options if type support choices'
            ),
            Array(
                'name'          => __( 'Exclusive from Categories', 'listingpro-plugin' ),
                'id'            => 'exclusive_field',
                'type'          => 'check',
                'child_of'=> '',
                'desc' => ''
            ),
            Array(
                'name'          => __( 'Select category for this field', 'listingpro-plugin' ),
                'id'            => 'field-cat',
                'type'          => 'checkboxes',
                'child_of'=> '',
                'options'=>listing_get_cat_array(),
                'desc' => ''
            ),
            $showInFilter




        );

        add_meta_box('teacher_meta_settings',esc_html__(  'Custom Form Fields:', 'listingpro-plugin' ),array($this,'listingteacherpro_custom_metabox_render'),'form-fields','normal','high',$listingpro_formFields);
    }

    public function listingteacherpro_custom_metabox_render($post, $metabox) {
        // echo '<pre>'; print_r( $metabox ); echo '</pre>';
        global $post;
        $options = get_post_meta($post->ID, 'lp_'.strtolower(THEMENAME).'_options', true);
        ?>
        <input type="hidden" name="lp_meta_box_nonce" value="<?php echo wp_create_nonce(basename(__FILE__));?>" />
        <table class="form-table lp-metaboxes">
            <tbody>
            <?php
            //echo "<pre>";print_r($options);exit;
            //$metabox['args'] = options();
            if( !empty($metabox['args'])){
                foreach ($metabox['args'] as $settings) {
                    $settings['value'] = isset($options[$settings['id']]) ? $options[$settings['id']] : (isset($settings['std']) ? $settings['std'] : '');
                    call_user_func('settings_'.$settings['type'], $settings);
                }
            }
            ?>
            </tbody>
        </table>
        <?php
    }

    public function listing_admin_menu() {
        //$icon = plugin_dir_path( __DIR__ ).'public/assets/images/importer.svg';
        add_menu_page(__('Post Importer','WPLI'),__('Post Importer','WPLI'),'manage_options','file_upload',array($this,'app_builder_create_page'),'',"",2);
        add_submenu_page( 'file_upload', __('Website Checker','WPLI'),__('Website Checker','WPLI'),'manage_options', 'wpli_website_checker',array($this,'app_builder_create_website_checker'));
    }


    public function delete_comments_listing_records(){
        global $wpdb;
        if(isset($_REQUEST['saveDelete'])){
            $deleteId = $_REQUEST['saveDelete'];
            $table = $wpdb->prefix.'listing_comments';
            //echo "select * from $table where id='".$deleteId."'";exit;
            $myposts = $wpdb->query( $wpdb->prepare("DELETE FROM $table where id='".$deleteId."'"));
            if(!empty($myposts)){
                $response[ 'success' ]  = "1";
                $response[ 'msg' ]  = 'Row Deleted Successfully!';
            }else{
                $response[ 'fail' ]  = "0";
                $response[ 'msg' ]  = 'Row Deleted failed !';
            }
            echo json_encode( $response );exit;
        }
    }

    public function update_comments_listing_records(){
        global $wpdb;
       if(isset($_REQUEST['addnewcomments']) && isset($_REQUEST['addTable'])){
           $addId = $_REQUEST['addTable'];
           $updateComments = $_REQUEST['addnewcomments'];
           $table = $wpdb->prefix.'listing_comments';
           $sql = $wpdb->query($wpdb->prepare("UPDATE `$table` SET comment ='$updateComments' WHERE id ='$addId'"));
           //print_r($sql);exit;
           if(!empty($sql)){
            $response[ 'success' ]  = "1";
            $response[ 'msg' ]  = 'Data Updated Successfully!';
            $response[ 'comment' ]  = $updateComments;
            $response[ 'timnestamp' ]  = date( 'Y-m-d H:i:s',time());
            $response[ 'fail' ]  = "0";
            $response[ 'msg' ]  = 'Data Updated failed !';
        }
        echo json_encode( $response );exit;
       }
    }

    public function getTheContent_handler(){
        global $wpdb;
        $listingarray =array();
        if(isset($_REQUEST['query_var'])){
            $listing_id   = $_REQUEST['query_var'];
            $sql = $wpdb->get_results('SELECT id,comment,timestamp FROM '.$wpdb->prefix.'listing_comments WHERE listing_id = "'.$listing_id.'"');
            //print_r($sql);exit;
            if(!empty($sql)){
                foreach($sql as $row){
                    $listingarray[]=$row;
                }
            }
        }
        echo json_encode($listingarray);exit;
    }


    public function comments_action_row($actions, $post){

        $comment[ 'comment'] = sprintf('<a post_id="'.$post->ID.'" class="comment-popup thickbox" id="comment-popup" title="Comments" href="#TB_inline?width=1300&height=350&inlineId=modal-window-id" aria-label="%s">%s</a>',
            esc_attr__( 'Comment' ),
            __( 'Comment' )
            );

        $offset = 3;
        $actions = array_slice($actions, 0, $offset, true) + $comment+ array_slice($actions, $offset, NULL, true);
        $posts = get_post( $post );
        //echo "<pre>";print_r($posts->ID); echo '</pre>';
        if ($posts->post_type){
            add_thickbox();?>
            <div id="modal-window-id" style="display:none;">
                <input type="submit" name="filter_action" id="myBtn-submit" class="button" value="Add New Comment">
                <div id='loader'>
                    <img id="loading-image" src='<?php echo plugin_dir_url( __DIR__ ).'public\assets\images\reload.gif';?>' width='32px' height='32px'>
                </div>
                <table class="comments-listings" id ="comments-listings">
                    <thead>
                        <tr>
                            <th>Comments</th>
                            <th>Time-Stamp</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="listingtbody">

                    </tbody>
                </table>
            </div>
            <div id="myModal" class="modal">
                <div class="modal-content">
                    <div class="modal-header">
                        <span class="close_add_new">&times;</span>
                        <h2>Add Comments</h2>
                    </div>
                    <div class="modal-body">
                        <textarea id="wreview" name="wreview" rows="10" cols="90"></textarea>
                        <input type="hidden" value="<?php echo $posts->post_type;?>" name ="posttypecomment" id="posttypecomment">
                    </div>
                    <div class="modal-footer">
                        <button id="save" class="button" type="button" name="save-details">Save</button>
                    </div>
                </div>
            </div>
            <?php
             /*
                $actions['comment'] = sprintf('<a post_id="'.$post->ID.'" class="comment-popup thickbox" id="comment-popup" title="Comments" href="#TB_inline?width=1300&height=350&inlineId=modal-window-id" aria-label="%s">%s</a>',
                esc_attr__( 'Comment' ),
                __( 'Comment' )
                );
            */
        }
        return $actions;
    }

   public function save_comments_listing_records(){
        global $wpdb;
       if(isset($_REQUEST['wreview']) && isset($_REQUEST['posttypecomment']) && isset($_REQUEST['listingID'])){
           $listing_id   = $_REQUEST['listingID'];
           $commenttype  = $_REQUEST['wreview'];
           $posttype     = $_REQUEST['posttypecomment'];
           $table = $wpdb->prefix.'listing_comments';
           $data = array('post_type' => $posttype, 'comment' => $commenttype,'listing_id'=>$listing_id);
           $format = array('%s','%s','%d');
           $wpdb->insert($table,$data,$format);
           $response[ 'success' ] =  1;
           $response[ 'msg' ] =  'Data Insert Successfully!';
           echo json_encode( $response );exit;

       }
   }

    public function cstm_css_and_js() {
        wp_enqueue_style( 'wooccm-backend-css', plugins_url( 'public/assets/backend_css.css', APP_RELPATH ) );
        wp_enqueue_style( 'custom-backend-css', plugins_url( 'public/assets/css/custom_css.css', APP_RELPATH ) );
        wp_enqueue_script( 'script_wccs', plugins_url( 'public/assets/js/script_wccs.js', APP_RELPATH ), array( 'jquery' ), '1.2' );
        wp_enqueue_script( 'billing_script_wccs', plugins_url( 'public/assets/js/billing_script_wccs.js', APP_RELPATH ), array( 'jquery' ), '1.2' );
        wp_enqueue_script( 'shipping_script_wccs', plugins_url( 'public/assets/js/shipping_script_wccs.js', APP_RELPATH ), array( 'jquery' ), '1.2' );
        wp_enqueue_script( 'importer_custom_js', plugins_url( 'public/assets/js/custom-js.js', APP_RELPATH ), array( 'jquery' ), '1.3' );
        wp_enqueue_script( 'importer_sweet_alert_js','https://cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.js');
        wp_enqueue_style ( 'importer_sweet_alert_style', 'https://cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.css');
        wp_enqueue_style( 'icon-font-Material',"https://fonts.googleapis.com/icon?family=Material+Icons");
        wp_enqueue_style ( 'checking_website_select2_style', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css');
        wp_enqueue_script( 'checking_website_select2_js','https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js');
        wp_localize_script( 'importer_custom_js', 'importer_ajax_object', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
        wp_localize_script( 'importer_custom_js', 'remaining_ajax_object', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
        wp_localize_script( 'importer_custom_js', 'json_ajax_object', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
        wp_localize_script( 'importer_custom_js', 'export_all_ajax_object', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
    }

    public function exportall_handler(){
        global $wpdb;
        $exportargs = array(
            'post_type' =>array('teacher','studio','school','retreat','restaurant'),
            'post_status' => 'all',
            'posts_per_page' => -1,
            'nopaging' => true,

        );
        //echo "<pre>"; print_r($exportargs);exit;
        //$allpostsexport = new WP_Query($exportargs);
        $allpostsexport = get_posts($exportargs);
        //echo "<pre>";print_r($allpostsexport);exit;
        if(!empty($allpostsexport)){
            $json_array = array();
            foreach( $allpostsexport as $allpostsexports ){
                //echo "<pre>";print_r($allpostsexports);exit;
                $post_id = $allpostsexports->ID;
                $listing_exportalljson  = get_post_meta( $post_id, 'listing_json', true );
                $json_exportobject[] = json_decode($listing_exportalljson);
                //echo "<pre>";print_r($json_exprotobject);exit;
            }
            $json_exportstring  =  json_encode( $json_exportobject,true );
            //echo "<pre>";print_r($json_exportstring);exit;
            $export_file_name="exportAll.json";
            $myexportfile = fopen(APP_PATH."logs/$export_file_name", "a") or die("Unable to open file!");
            fwrite($myexportfile, $json_exportstring);
            fclose($myexportfile);
            $json_file_export_url = APP_URL."logs/$export_file_name";
            $response[ 'success' ] =  1;
            $response[ 'file_data' ] =  $json_exportstring;
            $response[ 'file_url' ] =  $json_file_export_url;
            echo json_encode( $response ); exit;
        }
    }
    public function app_builder_create_website_checker(){
        require_once(APP_PATH.'/admin/json-checker-form.php');
    }
    public function app_builder_create_page(){
        $coordinates = $this->helper->get_geocode('US, 3700 Vesta Dr - Raleigh, NC 27603-3842');
        //echo '<pre>'; print_r( $coordinates );
        //echo $city_name = $this->helper->get_city_name( $coordinates['lat'],$coordinates['lng'] );exit;

        wp_enqueue_style( 'wooccm-backend-css', plugins_url( 'public/assets/backend_css.css', APP_RELPATH ) );
        wp_enqueue_script( 'script_wccs', plugins_url( 'public/assets/js/script_wccs.js', APP_RELPATH ), array( 'jquery' ), '1.2' );
        wp_enqueue_script( 'billing_script_wccs', plugins_url( 'public/assets/js/billing_script_wccs.js', APP_RELPATH ), array( 'jquery' ), '1.2' );
        wp_enqueue_script( 'shipping_script_wccs', plugins_url( 'public/assets/js/shipping_script_wccs.js', APP_RELPATH ), array( 'jquery' ), '1.2' );
        wp_enqueue_script( 'importer_custom_js', plugins_url( 'public/assets/js/custom-js.js', APP_RELPATH ), array( 'jquery' ), '1.3' );
        wp_enqueue_script( 'importer_custom_js', plugins_url( 'public/assets/js/my-js.js', APP_RELPATH ), array( 'jquery' ), '1.3' );
        wp_enqueue_script( 'importer_sweet_alert_js','https://cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.js');
        wp_enqueue_style ( 'importer_sweet_alert_style', 'https://cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.css');
        wp_localize_script( 'importer_custom_js', 'importer_ajax_object', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
        wp_localize_script( 'importer_custom_js', 'remaining_ajax_object', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
        wp_enqueue_style( 'wp-color-picker' );
        wp_enqueue_script( 'my-script-handle', plugins_url('public/assets/js/iris_init.js', __FILE__ ), array( 'wp-color-picker' ), false, true );
        require_once(APP_PATH.'/admin/json-importer-form.php');
    }

    public function smart_import_ajax_handler(){
        global $wpdb;
        ini_set("max_execution_time",3000000000);
        ini_set("max_allowed_memory",'4024M');
        ini_set("max_posts_vars",'4024M');
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);

        if($_FILES['file']['name'] !=''){
            // echo '<pre>'; print_r( $_FILES ); exit;

            if( $_FILES['file']['type'] /* == 'application/json'  */) {
                if (file_exists (ABSPATH.'/wp-admin/includes/taxonomy.php')) {
                    require_once (ABSPATH.'/wp-load.php');
                    require_once (ABSPATH.'/wp-admin/includes/taxonomy.php');
                }
                $str      = file_get_contents($_FILES['file']['tmp_name']);
                $strdata  = json_decode($str, true);
                $total_listings = count( $strdata );//12687
                //  Upload fiel here.
                $folder_path = APP_PATH.'uploads/';
                $filename = basename($_FILES['file']['name']);
                $newname = $folder_path . $filename;
                if (!move_uploaded_file($_FILES['file']['tmp_name'], $newname)) {
                    $response[ 'status' ]  = "fail";
                    echo json_encode( $response );exit;
                }

                $response[ 'total_records' ]  = $total_listings;
                $response[ 'data' ]  = $str;
                update_option( 'temp_json_feed',$filename );
                $response[ 'status' ]  = 'completed';
                echo json_encode( $response );exit;
            }else{
                $error = "Please Insert Json format";
            }
        }
    }

    public function smart_remaining_ajax_handler(){
        global $wpdb;
        // Get File here
        $file_name = get_option( 'temp_json_feed' );
        $folder_path = APP_PATH.'uploads/';
        $file_parth = $folder_path.$file_name;
        $json_feed_string     = file_get_contents($file_parth );
        $json_array = json_decode( $json_feed_string, true , JSON_UNESCAPED_SLASHES);
        // echo"<pre>";print_r( $json_array ); exit;
        $current_index =  $_REQUEST[ 'index' ];
        $total_deleted_items  = 0;
        for( $i=$current_index; $i<$current_index+30; $i++ ){
            //if( !isset($json_array[$i]) ) continue;
            //echo '<pre>'; print_r( $json_array[$i]);exit;
            if( isset( $json_array[$i]  )){
                $import_status=$this->save_json_listing($json_array[$i]);
                $total_deleted_items++;
            }
           // echo "<pre>";print_r($import_status);exit;
        }

        $current_index+= 30;

        if($import_status){
            $response[ 'status' ]  = "Feed Imported";
            $response[ 'current_index' ]  = $current_index;
            $response[ 'total_deleted_items' ]  = $total_deleted_items;
        }else{
            $response[ 'status' ]  = "Feed not Imported";
            $response[ 'total_deleted_items' ]  = $total_deleted_items;
        }

        echo json_encode( $response );exit;
    }
    public function save_json_listing($strshow){
        global $wpdb;
        $table_name = $wpdb->prefix."rejection_listing";
        //echo '<pre>'; print_r( $strshow );exit;
        if(!empty($strshow) && isset($_REQUEST['selectcategory'])) {
                if(!empty($strshow['title'])) {
                    if( !$this->helper->the_slug_exists($strshow['title'],isset($_REQUEST['selectcategory'] ))) {
                        $strshow['type'] = str_replace('yoga_instructors','yoga_instructor',$strshow['type']);
                        $coordinates = $this->helper->get_geocode($strshow['address']);
                        //echo '<pre>'; print_r( $coordinates );exit;
                        $city_name = $this->helper->get_city_name( $coordinates['lat'],$coordinates['lng'] );

                        if($strshow['address']){
                            $paddress = $strshow['address'];
                            if($strshow['city']){
                                $paddress .= ' '.@$strshow['city'];
                            }
                            if($strshow['country']){
                                $paddress .= ' '.str_replace('US','USA',@$strshow['country']);
                            }
                        }else{
                            $paddress = $strshow['city'];
                            if(@$strshow['country']){
                                $paddress .= ', '.str_replace('US','USA',@$strshow['country']);
                            }
                        }
                        if(!$strshow['description']){
                                //$strshow['description'] = $strshow['title'].' is a '.ucwords(str_replace('_',' ',$strshow['type'])).' in '.$paddress;
                                $strshow['description'] = $strshow['title'].' is a '.ucwords(str_replace('_',' ',$strshow['type'])).' in '.$coordinates[ 'formatted_address' ];

                        }

                        $my_post = array();
                        $my_post['post_type'] = $_REQUEST['selectcategory'];
                        $title_name = explode(',',$strshow['address']);

                        if( !empty($city_name)){
                            $my_post['post_title']   = $strshow['title'].'-'.$city_name;
                        }else{
                            $my_post['post_title']   = $strshow['title'];
                        }
                        //echo "select * from $wpdb->posts where post_title ='".$my_post['post_title']."' AND post_type='".$_REQUEST['selectcategory']."' and post_status='publish'";exit;
                        $title = $this->helper->clean( sanitize_text_field( $my_post['post_title'] ) );
                        $myposts = $wpdb->get_results( $wpdb->prepare("select * from $wpdb->posts where post_title ='".$title."' AND post_type='".$_REQUEST['selectcategory']."' and post_status='publish'"));
                        $listing_json = json_encode( $strshow,true );

                        //print_r($listing_json);exit;
                        if( !empty( $myposts )){
                            /*
                            foreach( $myposts as $delete_post  ){

                                $data = array('listing_options_json' => $listing_json,'post_type' => $_REQUEST['selectcategory'], 'reason' => 'Duplicate Listing');
                                $format = array('%s','%s', '%s');
                                $wpdb->insert($table_name,$data,$format);
                            }
                            return false;
                            */
                            $my_post['post_status']  = 'pending';
                        }else if( !isset($strshow['website']) ){
                            /*
                            $data = array('listing_options_json' => $listing_json, 'post_type' => $_REQUEST['selectcategory'], 'reason' => 'Website Address not exist');
                            $format = array('%s','%s','%s');
                            $wpdb->insert($table_name,$data,$format);
                            return false;
                            */
                            $my_post['post_status']  = 'pending';
                        }else if( !$strshow['website'] ){
                            /*
                            $data = array('listing_options_json' => $listing_json,'post_type' => $_REQUEST['selectcategory'], 'reason' => 'Website Address cannot be empty or null');
                            $format = array('%s','%s','%s');
                            $wpdb->insert($table_name,$data,$format);
                            return false;
                            */
                            $my_post['post_status']  = 'pending';
                        }else if (!filter_var($strshow['website'], FILTER_VALIDATE_URL)){
                            /*
                            $data = array('listing_options_json' => $listing_json, 'post_type' => $_REQUEST['selectcategory'], 'reason' => 'Improper Website Address');
                            //echo "<pre>";print_r($data);
                            $format = array('%s','%s','%s');
                            $wpdb->insert($table_name,$data,$format);
                            return false;
                            */
                            $my_post['post_status']  = 'pending';
                        }else{
                            $my_post['post_status']  = 'publish';
                        }

                        $listing_slug =  strtolower($this->helper->clean( sanitize_text_field( $my_post['post_title'] ) ) );
                        //$listing_slug = html_entity_decode(sanitize_text_field( $my_post['post_title']));
                        //echo "<pre>";print_r($listing_slug);
                        $is_listing_exists = get_page_by_path( $listing_slug ,OBJECT, $_REQUEST['selectcategory']);

                        if( !empty($is_listing_exists) ){   $error = "Record Already Exists!"; return false; }
                        //$my_post['post_content'] =  $strshow['title'].' is a '.ucwords(str_replace('_',' ',$strshow['type'])).' in '.$paddress;
                        $my_post['post_content'] =  $strshow['title'].' is a '.ucwords(str_replace('_',' ',$strshow['type'])).' in '.$coordinates[ 'formatted_address' ];


                        //echo '<pre>'; print_r( $my_post ); exit;

                        $post_id   = wp_insert_post( $my_post );

                        $cat_list  = $strshow['category'];
                        $cat       = explode(',',$cat_list);
                        $resultcat = array();
                        $parent = 0;
                        $cat_post_array = array();
                        $cat_post_array['teacher']      = 'teachers';
                        $cat_post_array['studio']       = 'studio-category';
                        $cat_post_array['school']       = 'schools';
                        $cat_post_array['retreat']      = 'retreats';
                        $cat_post_array['restaurant']   = 'restaurant-category';

                        $cat_list_array['teacher']      = 'Yoga Teacher';
                        $cat_list_array['studio']       = 'Yoga Studio';
                        $cat_list_array['school']       = 'Yoga School';
                        $cat_list_array['retreat']      = 'Yoga Retreat';
                        $cat_list_array['restaurant']   = 'Restaurant';
                        //  Check parent ID
                        $parentCategory = get_term_by('name',$cat_list_array[$_REQUEST['selectcategory']],$cat_post_array[$_REQUEST['selectcategory']]);

                        if( $parentCategory ){
                            $parent_cat_id  = $parentCategory->term_id;
                        }else{
                            //  Insert parent ID
                            $parent_cat_id  = wp_insert_category(array("cat_name"=>$cat_list_array[$_REQUEST['selectcategory']],"taxonomy"=>$cat_post_array[$_REQUEST['selectcategory']]),$wp_error);
                        }

                        if($cat_post_array[$_REQUEST['selectcategory']] && $cat_list_array[$_REQUEST['selectcategory']]){
                            $types = array($cat_post_array[$_REQUEST['selectcategory']],$cat_list_array[$_REQUEST['selectcategory']]);
                                                        foreach( $types as $type ) {
                                $catInfo = get_term_by('name',$type,$cat_post_array[$_REQUEST['selectcategory']],$cat_list_array[$_REQUEST['selectcategory']]);
                                if($catInfo && (end($resultcat)==$catInfo->parent) ) {
                                    $showcat = $catInfo->term_id;
                                }else{
                                    $showcat = wp_insert_category(array("cat_name"=>str_replace('US','USA',$type),"taxonomy"=>$cat_post_array[$_REQUEST['selectcategory']],"category_parent"=>$parent_cat_id),$wp_error);

                                    if(!$showcat){
                                        $catInfo = get_term_by('name',$categories,$cat_post_array[$_REQUEST['selectcategory']],$cat_list_array[$_REQUEST['selectcategory']]);

                                        if(!empty($catInfo)){
                                            $showcat = $catInfo->term_id;
                                        }
                                    }
                                }

                                $parent      = $showcat;
                                $resultcat[] = $showcat;
                            }
                        }

                        foreach($cat as $showcatdata){
                            if( strpos($showcatdata,">") > 0 ){
                                $categoryTrail = explode(">",$showcatdata);
                                foreach($categoryTrail as $categories){
                                    $catInfo = get_term_by('name',$categories,$cat_post_array[$_REQUEST['selectcategory']]);

                                    if(!empty($catInfo)){
                                        $showcat = $catInfo->term_id;
                                    }else{
                                        $showcat = wp_insert_category(array("cat_name"=>$categories,"taxonomy"=>$cat_post_array[$_REQUEST['selectcategory']],"category_parent"=>$parent_cat_id),$wp_error);
                                        if(!$showcat){
                                            $catInfo = get_term_by('name',$categories,$cat_post_array[$_REQUEST['selectcategory']]);
                                            if(!empty($catInfo)){
                                                $showcat = $catInfo->term_id;
                                            }
                                        }
                                    }
                                    $resultcat[] = $showcat;
                                    $parent      = $showcat;
                                }
                            }else{
                                $catInfo = get_term_by('name',$categories,$cat_post_array[$_REQUEST['selectcategory']]);
                                if(!empty($catInfo)){
                                    $showcat = $catInfo->term_id;
                                }else{
                                    $showcat     = wp_insert_category(array("cat_name"=>$showcatdata,"taxonomy"=>$cat_post_array[$_REQUEST['selectcategory']],"category_parent"=>$parent_cat_id),$wp_error);
                                    if(!$showcat){
                                        $catInfo = get_term_by('name',$categories,$cat_post_array[$_REQUEST['selectcategory']]);
                                        if(!empty($catInfo)){
                                            $showcat = $catInfo->term_id;
                                        }
                                    }
                                }
                                $resultcat[] = $showcat;
                            }
                        }

                        $resultcat = array_map( 'intval', $resultcat );
                        $resultcat = array_unique( $resultcat );
                        $tags_list = $strshow['tags'];
                        $tagsitem  = array($strshow['type']);
                        $tagsList  = array();
                        $tagsList_array = array();
                        $tagsList_array['teacher'] = 'teacher-tags';
                        $tagsList_array['studio'] = 'studio-tags';
                        $tagsList_array['school'] = 'school-tags';
                        $tagsList_array['retreat'] = 'retreat-tags';
                        $tagsList_array['restaurant'] ='restaurant-tags';

                        if(!empty($tags_list) && isset($tagsList_array[$_REQUEST['selectcategory']])){
                            //$parent   = 0;
                            $tagsitem = explode(',',$tags_list);
                            foreach($tagsitem as $tags){
                                $catInfo = get_term_by('name',$tags,$tagsList_array[$_REQUEST['selectcategory']]);
                                if(!$catInfo){
                                    $showcat  = wp_insert_category(array("cat_name"=>trim($tags),"taxonomy"=>$tagsList_array[$_REQUEST['selectcategory']],"category_parent"=>$parent),$wp_error);
                                    $term_id  = $showcat->term_id;
                                }else{
                                    $term_id  = $catInfo->term_id;
                                }
                                $tagsitem[] = $term_id;
                                $tagsList[] = trim($tags);
                            }
                            wp_set_object_terms($post_id,$tagsitem,$tagsList_array[$_REQUEST['selectcategory']]);
                        }

                        /*** Adding the location field here *****/
                        $locationList_array = array();
                        $locationList_array['teacher']      = 'teacher-location';
                        $locationList_array['studio']       = 'studio-location';
                        $locationList_array['school']       = 'school-location';
                        $locationList_array['retreat']      = 'retreat-location';
                        $locationList_array['restaurant']   = 'restaurant-location';
                        if(@$strshow['city']){
                            $locationInfo = get_term_by('name',trim(@$strshow['city']),$locationList_array[$_REQUEST['selectcategory']]);
                            if(!$locationInfo){
                                $showcat      = wp_insert_category(array("cat_name"=>trim(@$strshow['city']),"taxonomy"=>$locationList_array[$_REQUEST['selectcategory']]),$wp_error);
                                $locationInfo = get_term_by('name',trim(@$strshow['city']),$locationList_array[$_REQUEST['selectcategory']]);
                                wp_set_object_terms($post_id,array($locationInfo->term_id),$locationList_array[$_REQUEST['selectcategory']]);

                            }else{
                                wp_set_object_terms($post_id,array($locationInfo->term_id),$locationList_array[$_REQUEST['selectcategory']]);
                            }

                        }else{
                            if( @$strshow['address'] ) {
                                $address = explode(',',@$strshow['address']);
                                $locationInfo = get_term_by('name',trim(@$address[1]),$locationList_array[$_REQUEST['selectcategory']]);
                                //
                                if(!$locationInfo){
                                    $showcat      = wp_insert_category(array("cat_name"=>trim(@$address[1]),"taxonomy"=>$locationList_array[$_REQUEST['selectcategory']]),$wp_error);
                                    $locationInfo = get_term_by('name',trim(@$address[1]),$locationList_array[$_REQUEST['selectcategory']]);
                                    wp_set_object_terms($post_id,array($locationInfo->term_id),$locationList_array[$_REQUEST['selectcategory']]);
                                }else{
                                    wp_set_object_terms($post_id,array($locationInfo->term_id),$locationList_array[$_REQUEST['selectcategory']]);
                                }
                            }
                        }

                        $listing_rate = array($strshow['price']);

                        if(!empty($listing_rate)){
                            //add_post_meta($post_id,'listing_rate',$listing_rate);
                        }
                        $listing_reviewed = array();
                        add_post_meta($post_id,'listing_reviewed',$listing_reviewed);
                        $plan_id = array(14);
                        if(!empty($plan_id)){
                            add_post_meta($post_id,'plan_id',$plan_id);
                        }
                        $claimed = array(0);
                        if(!empty($claimed)){
                            add_post_meta($post_id,'claimed',$claimed);
                        }

                        // 'price'=>$strshow['price']
                        $listing_plan_data = array('menu'=>'true','announcment'=>'true','deals'=>'true','competitor_campaigns'=>'true','events'=>'true','bookings'=>'true');
                        if(!empty($claimed)){
                            add_post_meta($post_id,'claimed',$claimed);
                        }

                        /** FAQ TABS **/
                        $faq = array('title'=>array(),'description'=>array());
                        $i=1;
                        if( $strshow['specifications']['tab_1']['tab_name'] ) {
                            $faq['title'][1]      = $strshow['specifications']['tab_1']['tab_name'];
                            $content = array();
                            foreach($strshow['specifications']['tab_1']['tab_content'] as $key=>$value){
                                $contentText = (is_array($value))? implode("\r\n",$value):$value;
                                $content[]   = $key.': '.$contentText;
                            }
                            $faq['description'][1] = implode("\r\n",$content);
                        }
                        if( $strshow['specifications']['tab_2']['tab_name'] ) {
                            $faq['title'][2]      = $strshow['specifications']['tab_2']['tab_name'];
                            $content = array();
                            foreach($strshow['specifications']['tab_2']['tab_content'] as $key=>$value){
                                $contentText = (is_array($value))? implode("\r\n",$value):$value;
                                $content[]   = $key.': '.$contentText;
                            }
                            $faq['description'][2] = implode("\r\n",$content);
                        }
                        if( $strshow['specifications']['tab_3']['tab_name'] ) {
                            $faq['title'][3]      = $strshow['specifications']['tab_3']['tab_name'];
                            $content = array();
                            foreach($strshow['specifications']['tab_3']['tab_content'] as $key=>$value){
                                $contentText = (is_array($value))? implode("\r\n",$value):$value;
                                $content[]   = $key.': '.$contentText;
                            }
                            $faq['description'][3] = implode("\r\n",$content);
                        }
                        if( $strshow['specifications']['tab_4']['tab_name'] ) {
                            $faq['title'][4]      = $strshow['specifications']['tab_4']['tab_name'];
                            $content = array();
                            foreach($strshow['specifications']['tab_4']['tab_content'] as $key=>$value){
                                $contentText = (is_array($value))? implode("\r\n",$value):$value;
                                $content[]   = $key.': '.$contentText;
                            }
                            $faq['description'][4] = implode("\r\n",$content);
                        }

                        if( $strshow['payment_methods'] ) {
                            add_post_meta($post_id,'payment_methods',$strshow['payment_methods']);
                        }
                        ///$priceRange = explode('-',$strshow['price']);
                        $lp_listingpro_options = array(
                            //'tagline_text'=> $strshow['title'].' is a '.ucwords(str_replace('_',' ',$strshow['type'])).' in '.$paddress,
                            'tagline_text'=> $strshow['title'].' is a '.ucwords(str_replace('_',' ',$strshow['type'])).' in '.$coordinates[ 'formatted_address' ],
                            //'gAddress'=>$paddress,
                            'gAddress'=>$coordinates[ 'formatted_address' ],
                            'latitude'=>$coordinates['lat'],
                            'longitude'=>$coordinates['lng'],
                            'mappin'=>'true',
                            'phone'=>$strshow['phone'],
                            'whatsapp'=>'',
                            //'email'=>$strshow['mail'],
                            'website'=>urldecode($strshow['website']),
                            //'twitter'=>'https://twitter.com',
                            //'facebook'=>'https://facebook.com',
                            //'linkedin'=>'https://linkedin.com',
                            'youtube'=>'',
                            'instagram'=>'',
                            'video'=>'',
                            'gallery'=>'',
                            //'price_status'=>'notsay',
                            //'list_price'=>@$priceRange[0],
                            //'list_price_to'=>@$priceRange[1],
                            'Plan_id'=>0,
                            'lp_purchase_days'=>'',
                            'reviews_ids'=>'',
                            'claimed_section'=>'',
                            'listings_ads_purchase_date'=>'',
                            'listings_ads_purchase_packages'=>'',
                            'faqs'=>array('faq'=>$faq['title'],'faqans'=>$faq['description']),
                            //'business_hours'=>$strshow['work_hours'],
                            'campaign_id'=>'',
                            'changed_planid'=>'',
                            'listing_reported_by'=>'',
                            'listing_reported'=>'',
                            'business_logo'=>''
            );
                            $lp_listingpro_options[ 'listing_title' ]  = $strshow['title'];

                            if(!empty($lp_listingpro_options)){
                                add_post_meta($post_id,'lp_listingpro_options',$lp_listingpro_options);
                            }

                            if( $strshow['other_images'] ) {
                            }
                            $gallery_image_ids = '1799,1798,1797,1796,1795';
                            if(!empty($gallery_image_ids)){
                                add_post_meta($post_id,'gallery_image_ids',$gallery_image_ids);
                            }
                            $lp_listingpro_options_fields = array(array('lp_feature'=>array(28,32)));
                            if(!empty($lp_listingpro_options_fields)){
                                add_post_meta($post_id,'lp_listingpro_options_fields',$lp_listingpro_options_fields);
                            }
                            $campaign_status = array('active');
                            if(!empty($campaign_status)){
                                //add_post_meta($post_id,'campaign_status',$campaign_status);
                            }
                            $lp_random_ads = array('active');
                            if(!empty($lp_random_ads)){
                                //add_post_meta($post_id,'lp_random_ads',$lp_random_ads);
                            }
                            $lp_detail_page_ads = array('active');
                            if(!empty($lp_detail_page_ads)){
                                //add_post_meta($post_id,'lp_detail_page_ads',$lp_detail_page_ads);
                            }
                            $lp_top_in_search_page_ads = array('active');
                            if(!empty($lp_top_in_search_page_ads)){
                                //add_post_meta($post_id,'lp_top_in_search_page_ads',$lp_top_in_search_page_ads);
                            }
                            /*
                            $_thumbnail_id = array(123);
                            if(!empty($_thumbnail_id)){
                                add_post_meta($post_id,'_thumbnail_id',$_thumbnail_id);
                            }
                            $post_views_count = array(0);
                            if(!empty($post_views_count)){
                                    add_post_meta($post_id,'post_views_count',$post_views_count);
                            }
                            */
                            $response = wp_set_object_terms($post_id, $resultcat,$cat_post_array[$_REQUEST['selectcategory']],$cat_list_array[$_REQUEST['selectcategory']] );
                            //echo "<pre>";print_r($response);die('hello');

                            //  Set Template
                            //add_post_meta( $post_id, '_wp_page_template', 'custom-listing.php' );
                            //add_post_meta( $post_id, '_wp_page_template', 'custom-studio.php' );
                            /** importing the image here ***/
                            add_post_meta ( $post_id , 'listing_json', json_encode( $strshow ) );
                            // $this->helper->set_featured_image($post_id,$strshow['main_image']);
                            $msg = "Data imported successfully";
                    }
                }
            //}
            return true;
        }else{
            return false;
            $error = "File Contains invalid Json format";
        }
        return false;
    }

    public function delete_all_listing_records(){
        $args=array('post_type'=>array('teacher','studio','school','retreat'),'post_status' => 'all','numberposts'=>-1);
        $allposts= get_posts($args);
        if( count( $allposts ) > 0 ){
            foreach($allposts as $eachpost){
                wp_delete_post( $eachpost->ID, true );
            }
        }
    }

    public function delete_blank_listing_records(){
        global $wpdb;
        $args=array('post_type'=>array('teacher','studio','school','retreat','restaurant'),'numberposts'=>-1,'fields'=> 'ID');
        $allListing = get_posts($args);
        $table_name = $wpdb->prefix."rejection_listing";

        if(!empty($allListing)){
            foreach($allListing as $post){
                $post_id = $post->ID;
                $posttitle = $post->post_title;
                $myposts = $wpdb->get_results( $wpdb->prepare("select * from $wpdb->posts where post_title ='".$posttitle."'AND ID!='".$post_id."'"));
                if( !empty( $myposts )){
                    foreach( $myposts as $delete_post  ){
                        wp_delete_post( $delete_post->ID, true );
                    }
                }
                $website = $listing_json['website'];

                if(!$website){
                    wp_delete_post( $post_id, true );
                }
            }

            $responses['posts'] = get_post_meta( $post->ID, 'lp_listingpro_options', true );
            if(!empty($responses)){
                foreach($responses as $response){
                    $json_string = json_encode($response);
                    $folder_path = APP_PATH.'uploads/'.'file.json';
                    $file_path = $folder_path;
                }
            }
        }
    }
}

new WPLI_Admin( 'JSON Feed Importer','1.0.0' );