<h2><?php _e( 'Website Checker', 'website_checker' ); ?></h2>
<br>

<!-- #content -->

<div id="general-semi-nav-checking">
    <!-- #main-nav-left -->
    <div id="content-nav-right-checking" class="general-vibe">
        <div class="widefat general-semi upload_files checking_website_upload_files" border="0">
            <form name="customchecker" id="checkerform" method="post" class="reset_form" action="" style="width:100%;" enctype="multipart/form-data">
             <!-- .section -->
                       
                <div class="website-checker">
                    <label style="display: inline-block;">
                        <?php echo  __( 'Website Checker:', 'WPLI' ) ; ?></label>
                    <select id="selectcategorychecker" class="js-example-basic-multiple" name="selectcategorychecker[]" multiple="multiple">
                        <option value="teacher"><?php echo __( 'Teacher', 'WPLI' ); ?></option>
                        <!-- <option value="studio"><?php //echo __( 'Studios', 'WPLI' ); ?></option> -->
                        <option value="school"><?php echo __( 'Schools & Studios', 'WPLI' ); ?></option>
                        <option value="retreat"><?php echo __( 'Retreat', 'WPLI' ); ?></option>
                        <option value="restaurant"><?php echo __( 'Restaurant', 'WPLI' ); ?></option>
                    </select>
                </div> 
                <!-- .section -->
                <div class="wraps">
                    <input style="margin-left: 10px; margin-top:10px;" type="submit" id="checker_file_button" name="submit" class="button button-primary" value="<?php echo __( 'Submit', 'WPLI' ); ?>"/>                    
                </div>
            </form>
        </div>
    </div>
    <!-- #content-nav-right -->
</div>
<!-- #general-semi-nav -->