<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    WPLI
 * @subpackage WPLI/admin
 * @author     Rohit Sharma
 */

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

class WPLI_Admin {
    private $plugin_name,$version,$helper,$comment_list;

    public function __construct( $WPLI, $version ) {
        $this->plugin_name = $WPLI;
        $this->version = $version;
        $this->helper = new listing_importer_helper();
        add_action( 'wp_ajax_nopriv_test_osm', array( $this,'test_osm' ));
        add_action( 'wp_ajax_test_osm',  array( $this,'test_osm' ));
        add_action( 'wp_ajax_nopriv_get_smartimportteacher', array( $this,'smart_import_ajax_handler' ));
        add_action( 'wp_ajax_get_smartimportteacher',  array( $this,'smart_import_ajax_handler' ));
        add_action( 'wp_ajax_nopriv_get_smartremainingrecords',  array( $this,'smart_remaining_ajax_handler' ));
        add_action( 'wp_ajax_get_smartremainingrecords',  array( $this,'smart_remaining_ajax_handler' ));
        add_action( 'wp_ajax_nopriv_delete_all',  array( $this,'delete_all_listing_records' ));
        add_action( 'wp_ajax_delete_all',  array( $this,'delete_all_listing_records' ));
        //add_action( 'wp_ajax_nopriv_delete_taxonomy',  array( $this,'delete_all_taxonomy_records' ));
        //add_action( 'wp_ajax_delete_taxonomy',  array( $this,'delete_all_taxonomy_records' ));
        add_action( 'wp_ajax_nopriv_delete_media_files',  array( $this,'delete_media_attachments_records' ));
        add_action( 'wp_ajax_delete_media_files',  array( $this,'delete_media_attachments_records' ));
        add_action( 'wp_ajax_nopriv_get_exportall',  array( $this,'exportall_handler' ));
        add_action( 'wp_ajax_get_exportall',  array( $this,'exportall_handler' ));
        add_action( 'wp_ajax_nopriv_getTheContent',  array( $this,'getTheContent_handler' ));
        add_action( 'wp_ajax_getTheContent',  array( $this,'getTheContent_handler' ));
        add_action( 'wp_ajax_nopriv_delete_blank_listing',  array( $this,'delete_blank_listing_records' ));
        add_action( 'wp_ajax_delete_blank_listing',  array( $this,'delete_blank_listing_records' ));
        add_action( 'wp_ajax_nopriv_save_comments_listing',  array( $this,'save_comments_listing_records' ));
        add_action( 'wp_ajax_save_comments_listing',  array( $this,'save_comments_listing_records' ));
        add_action( 'wp_ajax_nopriv_update_comments_listing',  array( $this,'update_comments_listing_records' ));
        add_action( 'wp_ajax_update_comments_listing',  array( $this,'update_comments_listing_records' ));
        add_action( 'wp_ajax_nopriv_delete_comments_listing',  array( $this,'delete_comments_listing_records' ));
        add_action( 'wp_ajax_delete_comments_listing',  array( $this,'delete_comments_listing_records' ));
        //add_action( 'admin_init', array($this,'custom_teacher_comments_listing_page_handler' ));
        add_action( 'admin_menu', array($this, 'listing_admin_menu' ));
        add_filter('post_row_actions',array($this,'comments_action_row'),10,2);
        add_filter('post_row_actions',array($this,'remove_row_actions_post'),10,3);
        add_action('admin_enqueue_scripts', array($this,'cstm_css_and_js'));
        add_action('admin_init', array($this,'wpli_listingpro_settings_box'));
        add_action('admin_init', array($this,'wpli_listingpro_settings_formfields'));
        add_action('admin_print_scripts', array($this,'wpli_postsettings_admin_scripts'));
        add_action('admin_print_styles', array($this,'wpli_postsettings_admin_styles'));
        add_action('save_post', array($this,'wpli_savePostMeta'));
        add_filter('custom_menu_order', array($this,'wpli_admin_menu_order'));
        add_filter('menu_order', array($this,'wpli_admin_menu_order'));
        add_action( 'wp_ajax_nopriv_website_checking_listing',  array( $this,'website_checking_listing_records' ));
        add_action( 'wp_ajax_website_checking_listing',  array( $this,'website_checking_listing_records' ));
        add_action( 'wp_ajax_nopriv_delete_taxonomy_terms',  array( $this,'wpli_delete_taxonomy_terms' ));
        add_action( 'wp_ajax_delete_taxonomy_terms',  array( $this,'wpli_delete_taxonomy_terms' ));
        add_action( 'wp_ajax_nopriv_proyoga_contactownerediteds', array( $this,'proyoga_contactownereditedhandlers' ));
        add_action( 'wp_ajax_proyoga_contactownerediteds',  array( $this,'proyoga_contactownereditedhandlers' ));
        add_action( 'wp_ajax_nopriv_proyoga_contactowerreport', array( $this,'proyoga_contactowerreporthandler' ));
        add_action( 'wp_ajax_proyoga_contactowerreport',  array( $this,'proyoga_contactowerreporthandler' ));
        add_action( 'restrict_manage_posts', array($this,'posts_filter_export_csv' ));
        add_action( 'wp_ajax_nopriv_get_exportcsvpending',  array( $this,'exportcsvpendinghandler' ));
        add_action( 'wp_ajax_get_exportcsvpending',  array( $this,'exportcsvpendinghandler' ));
        add_action( 'wp_ajax_nopriv_get_updatingListingfields',  array( $this,'updatingListingfieldshandler' ));
        add_action( 'wp_ajax_get_updatingListingfields',  array( $this,'updatingListingfieldshandler' ));
        add_action( 'wp_ajax_nopriv_get_stagListingfields',  array( $this,'stagListingfieldshandler' ));
        add_action( 'wp_ajax_get_stagListingfields',  array( $this,'stagListingfieldshandler' ));
        add_action( 'login_init', array($this,'action_login_init') );
        add_filter('validate_username', array($this,'proypga_validate_username'), 10, 2);
        add_filter('registration_errors', array($this,'proyoga_registration_errors'));
        add_action( 'wp_ajax_nopriv_restricted_names_listing',  array( $this,'restricted_names_listing_records' ));
        add_action( 'wp_ajax_restricted_names_listing',  array( $this,'restricted_names_listing_records' ));
        add_action( "edit_form_top", array( $this,"comments_post_edit_page" ));
        add_action( 'wp_ajax_nopriv_listingYogaContent', array( $this,'listingYogaContenthandler' ));
        add_action( 'wp_ajax_listingYogaContent',  array( $this,'listingYogaContenthandler' ));
    }

    public function listingYogaContenthandler(){
        $allListingGet = array(
            'posts_per_page' => -1,
            'post_type' =>array('teacher','school','retreat','restaurant'),
            'post_status' => 'publish','pending'
        );
        $allListingGetpostsexports = get_posts($allListingGet);
        if(!empty($allListingGetpostsexports)){
            foreach($allListingGetpostsexports as $allListingGetpostsexport){
                $postID = $allListingGetpostsexport->ID;
                $getData= get_post_meta($postID,'lp_listingpro_options',true);
                $getText = 'Curious about private yoga lessons or group yoga classes with '.$allListingGetpostsexport->post_title.'? Use the contact form on this page';
                $getData['yoga_content_tab'] =  $getText;                
                update_post_meta($postID,'lp_listingpro_options',$getData);
            }
        }
    }

    public function remove_row_actions_post($actions, $post){
        if( $post->post_type === 'edited' ) {
            unset( $actions['edit'] );
            unset( $actions['view'] );
            unset( $actions['trash'] );
            unset( $actions['inline hide-if-no-js'] );
        }
        return $actions;
    }
    public function restricted_names_listing_records(){
        $restrictedusernamecheck = $_REQUEST['restrictedusernamecheck'];
        $url = "https://raw.githubusercontent.com/shouldbee/reserved-usernames/master/reserved-usernames.json";
        $urlTxtFile = "https://www.cs.cmu.edu/~biglou/resources/bad-words.txt";
		$response = wp_remote_get( $url,array('timeout'=> 120,'httpversion' => '1.1',));
        $responsetxt = wp_remote_get( $urlTxtFile,array('timeout'=> 120,'httpversion' => '1.1',));
        $dataUserName = json_decode($response['body']);
        $dataUserNamearrayTxt = $responsetxt['body'];
        $dataUserNameTxt = explode("\n", $dataUserNamearrayTxt);
        $dataUserNameTxtFilter =array_filter($dataUserNameTxt);
        $restricted =  json_decode(get_option('restrictedusername'));
        $list = explode(",",$restrictedusernamecheck);
        // $restrictedusernamecheck = $_REQUEST['restrictedusernamecheck'];        
        // $list = explode(",",$restrictedusernamecheck);          
        // $data = json_encode( $list); 
        // $existing_val = get_option( 'restrictedusername' );

        if(!empty($dataUserName)){
            foreach($dataUserName as $reserved_username){
                if( !in_array( $reserved_username,$restricted)){
                    $restricted[] = $reserved_username;
                }     
            }
        }
        
        if(!empty($dataUserNameTxtFilter)){
            foreach($dataUserNameTxtFilter as $reservedUserName){
                if( !in_array( $reservedUserName,$restricted)){
                    $restricted[] = $reservedUserName;
                }      
            }
        }
        $arrayMerge = array_merge($list,$restricted);
        $data = json_encode( $arrayMerge );
        $existing_val = get_option( 'restrictedusername' );
        
        if ( false !== $existing_val ) {
            
            // option exist
            if ( $existing_val === $data ) {
                
                //echo "new value is same as old.";
            } else {
                //echo $existing_val; exit;
                update_option( 'restrictedusername', $data, 'yes' );
            }
        } else {
            add_option('restrictedusername', $data );
        }
        $response   =   array(
            'success' => 1,
            'message' => 'Listing Saved Successfully!'
        );
        echo json_encode( $response ); exit;
    }

    public function proypga_validate_username($valid, $username) {
        $restricted =  json_decode(get_option('restrictedusername'));
        //$restricted = array('profile', 'directory', 'domain', 'download', 'downloads', 'edit', 'editor', 'email', 'ecommerce', 'forum', 'forums', 'favorite', 'feedback', 'follow', 'files', 'gadget', 'gadgets', 'games', 'guest', 'group', 'groups', 'homepage', 'hosting', 'hostname', 'httpd', 'https', 'information', 'image', 'images', 'index', 'invite', 'intranet', 'indice', 'iphone', 'javascript', 'knowledgebase', 'lists','websites', 'webmaster', 'workshop', 'yourname', 'yourusername', 'yoursite', 'yourdomain');
        $pages = get_pages();
        foreach ($pages as $page) {
           $restricted[] = $page->post_name;
        }

        // echo '<pre>'; print_r( $restricted  ); exit;
        if ($valid && strpos( $username, ' ' ) !== false) $valid=false;
        if ($valid && in_array( $username, $restricted )) $valid=false;
        if ($valid && array_uintersect([$username], $restricted,'strcasecmp' )) $valid=false;
        if ($valid && strlen($username) < 5) $valid=false;
        // if(!$valid || is_user_logged_in() && current_user_can('create_users') ) return $valid;

        return $valid;
    }

    public function proyoga_registration_errors($errors) {
        if ( isset( $errors->errors['invalid_username'] ) )
           $errors->errors['invalid_username'][0] = __( 'ERROR: Invalid username.', 'WPLI' );
        return $errors;
    }

    public function stagListingfieldshandler(){
        global $wpdb;
        $meta 				= 'lp_'.strtolower(THEMENAME).'_options';
        $post_id            = $_POST['post_id'];
        $updating_listing_options_json  = get_post_meta( $post_id, $meta, true );
        // echo "<pre>";print_r($updating_listing_options_json);exit;
        $listing_options[ 'tagline_text' ] = $updating_listing_options_json['tagline_text'];
        $listing_options[ 'gAddress' ] = $updating_listing_options_json['gAddress'];
        $listing_options[ 'gAddressDetails' ] = $updating_listing_options_json['gAddressDetails'];
        $listing_options[ 'latitude' ] = $updating_listing_options_json['latitude'];
        $listing_options[ 'longitude' ] = $updating_listing_options_json['longitude'];
        $listing_options[ 'wpli-mappin' ] = $updating_listing_options_json['wpli-mappin'];
        $listing_options[ 'phone' ] = $updating_listing_options_json['phone'];
        $listing_options[ 'email' ] = $updating_listing_options_json['email'];
        $listing_options[ 'gallery' ] = $updating_listing_options_json['gallery'];
        $listing_options[ 'lp_purchase_days' ] = $updating_listing_options_json['lp_purchase_days'];
        $listing_options[ 'claimed_section' ] = $updating_listing_options_json['claimed_section'];
        $listing_options[ 'business_hours' ] = $updating_listing_options_json['business_hours'];
        $listing_options[ 'business_logo' ] = $updating_listing_options_json['business_logo'];
        $listing_options[ 'address_type' ] = $updating_listing_options_json['address_type'];

        $listing_options[ 'linkedin' ] = $updating_listing_options_json['linkedin'];
        $listing_options[ 'twitter' ] = $updating_listing_options_json['twitter'];
        $listing_options[ 'facebook' ] = $updating_listing_options_json['facebook'];
        $listing_options[ 'youtube' ] = $updating_listing_options_json['youtube'];
        $listing_options[ 'instagram' ] = $updating_listing_options_json['instagram'];
        $listing_options[ 'pinterest' ] = $updating_listing_options_json['pinterest'];
        $listing_options[ 'website' ] = $updating_listing_options_json['website'];

        $response   =   array(
            'data' => $listing_options,
            'post_id' => $post_id,
            'success' => 1,
            'message' => 'Listing Display Successfully!'
        );
        echo json_encode( $response ); exit;


    }

    public function updatingListingfieldshandler(){

        global $wpdb;
        $meta 				= 'lp_'.strtolower(THEMENAME).'_options';
        $post_id            = $_POST['post_id'];
        $updating_listing_options_json  = get_post_meta( $post_id, $meta, true );
        $listing_options[ 'tagline_text' ] = $updating_listing_options_json['tagline_text'];
        $listing_options[ 'gAddress' ] = $updating_listing_options_json['gAddress'];
        $listing_options[ 'gAddressDetails' ] = $updating_listing_options_json['gAddressDetails'];
        $listing_options[ 'latitude' ] = $updating_listing_options_json['latitude'];
        $listing_options[ 'longitude' ] = $updating_listing_options_json['longitude'];
        $listing_options[ 'wpli-mappin' ] = $updating_listing_options_json['wpli-mappin'];
        $listing_options[ 'phone' ] = $updating_listing_options_json['phone'];
        $listing_options[ 'email' ] = $updating_listing_options_json['email'];
        $listing_options[ 'gallery' ] = $updating_listing_options_json['gallery'];
        $listing_options[ 'lp_purchase_days' ] = $updating_listing_options_json['lp_purchase_days'];
        $listing_options[ 'claimed_section' ] = $updating_listing_options_json['claimed_section'];
        $listing_options[ 'business_hours' ] = $updating_listing_options_json['business_hours'];
        $listing_options[ 'business_logo' ] = $updating_listing_options_json['business_logo'];
        $listing_options[ 'address_type' ] = $updating_listing_options_json['address_type'];

        $listing_options[ 'linkedin' ] = $_REQUEST['linkedin7'];
        $listing_options[ 'twitter' ] = $_REQUEST['twitter7'];
        $listing_options[ 'facebook' ] = $_REQUEST['facebook7'];
        $listing_options[ 'youtube' ] = $_REQUEST['youtube7'];
        $listing_options[ 'instagram' ] = $_REQUEST['instagram7'];
        $listing_options[ 'pinterest' ] = $_REQUEST['pinterest7'];
        $listing_options[ 'website' ] = $_REQUEST['website7'];
        $updated_data = update_post_meta($post_id, $meta, $listing_options);
        // echo "<pre>";print_r($updated_data);exit;
        // echo "<pre>";print_r($updated_data);exit;
        $response   =   array(
            'update-query' => $updated_data,
            'post_id' => $post_id,
            'success' => 1,
            'message' => 'Listing Approved Successfully!'
        );
        echo json_encode( $response ); exit;


    }

    public function test_osm(){
        //echo "<pre>";print_r($_POST);exit;
        global $listingpro_options;
        $mapAPI = $listingpro_options['google_map_api'];
        $address = (isset($_POST['search']))?$_POST['search']:'USA';
		$url = "https://nominatim.openstreetmap.org/search?q=".$address."&format=json&polygon=1&addressdetails=1";
		$response = wp_remote_get( $url,array('timeout'=> 120,'httpversion' => '1.1',));
        $data = json_decode($response['body']);
        if(empty($data)){
            $url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&key='.$mapAPI;
            $response = wp_remote_get( $url,array('timeout'=> 120,'httpversion' => '1.1',));
            $data = json_decode($response['body']);
            if($data->status=='OK'){
                $location = $data->results[0]->geometry->location;
                $latitude = $location->lat != '' ? $location->lat : '25.7612';
                $longitude = $location->lng != '' ? $location->lng : '-80.1911';
                $address = $data->results[0]->formatted_address != '' ? $data->results[0]->formatted_address : '2037 Chestnut Philadelphia';
                $lineDataMapCheckerGoogle[] = array(
                    'Latitude'=>$latitude,
                    'Longitude'=>$longitude,
                    'Address'=>$address
                );
                //echo "<pre>";print_r($lineDataMapCheckerGoogle);exit;
                $response[ 'success' ] =  1;
                $response[ 'data' ] =  $lineDataMapCheckerGoogle;
            }
        }else
        if(!empty($data)){
            foreach($data as $map_data){
                $latitude  = $map_data->lat;
                $longitude = $map_data->lon;
                $address   = $map_data->display_name;
                $lineDataMapChecker[] = array(
                    'Latitude'=>$latitude,
                    'Longitude'=>$longitude,
                    'Address'=>$address
                );
                //echo "<pre>";print_r($lineDataWebChecker);exit;
                $response[ 'success' ] =  1;
                $response[ 'data' ] =  $lineDataMapChecker;
            }
           
        }else{
            $response[ 'success' ] =  0;
            $response[ 'data' ] =  $lineDataMapChecker;
            $response[ 'message' ] =  'Wrong Address';
        }
        echo json_encode( $response ); exit;

    }
    public function action_login_init() {
        if( !( isset($_REQUEST[ 'action' ]) ) && $_REQUEST[ 'action' ]!= 'logout' && is_user_logged_in()  ){
            $redirect_to = esc_url( home_url() );
            wp_redirect( $redirect_to );
            die();
        }
    }
    public function exportcsvpendinghandler(){
        global $wpdb;
        $exportargs = array(
            'post_type' =>$_REQUEST['post_type'],
            'post_status' => 'pending',
            'posts_per_page' => -1,
        );
        $allpostsexport = get_posts($exportargs);
        if(!empty($allpostsexport)){
            $json_array = array();
            foreach( $allpostsexport as $allpostsexports ){
                $post_id = $allpostsexports->ID;
                $listing_exportalljson  = get_post_meta( $post_id, 'listing_json', true );
                $listing_exportall_options_json  = get_post_meta( $post_id, 'lp_listingpro_options', true );
                $listing_decode  = json_decode($listing_exportalljson);
                $type = $listing_decode->type;
                $title = $listing_decode->title;
                $category = $listing_decode->category;
                $descriprion = $listing_decode->description;
                $address = $listing_exportall_options_json['gAddress'];
                $phone = $listing_exportall_options_json['phone'];
                $email = $listing_exportall_options_json['email'];
                $website = $listing_exportall_options_json['website'] ;
                $work_hour = $listing_exportall_options_json['business_hours'];
                $lineData[] = array(
                'Type'=>$type,
                'Title'=>$title,
                'Category'=>$category,
                'Description'=>$descriprion,
                'Address'=>$address,
                'Phone'=>trim($phone),
                'Email'=>$email,
                'Website'=>$website,
                'WorkHours'=>$work_hour);
            }
            //echo"<pre>";print_r($lineData);exit;
            $response[ 'success' ] =  1;
            $response[ 'file_data' ] =  $lineData;
            $response[ 'file_name' ] =  $_REQUEST['post_type'].' '.'Pending Listing.csv';
            echo json_encode( $response ); exit;
        }
    }

    public function posts_filter_export_csv(){
        global $post;
        $posttype= $post->post_type;
		echo '<input type="submit" name="filter_export_csv" id="csv_export" class="button" value="Export Csv">';
        echo '<input type="hidden" name="post_type" id="post_type" value="'.$posttype.'">';
    }

    public function proyoga_contactowerreporthandler(){
        //echo "<pre>";print_r($_POST);exit;
        $meta 				= 'lp_'.strtolower(THEMENAME).'_options_contactownerform';
        $post_id            = $_POST['postID'];
        $post_status        = $_POST['post_status'];
        $post_ID            = $_POST['post_ID'];
        $lp_listingpro_options = array(
            'Comment'               =>$_POST['validationComment'],
            'Email'               =>$_POST['ReportEmail'],
            'post_title'            =>$_POST['post_title'],
            'post_status'           =>$_POST['post_status']
        );

        $lp_listingpro_options_status = array(
            'postTypeComment' 		=> sanitize_text_field($_POST['validationComment']),
            'reporterType' 			=> sanitize_text_field($_POST['type']),
			'post_title' 			=> sanitize_text_field($_POST['post_title']),
			'email' 				=> sanitize_text_field($_POST['ReportEmail']),
			'post_type'				=>'edited',
			'post_status'			=> $_POST['post_status'],
			'report_id'				=> 1,
			'post_id'				=>sanitize_text_field($post_id)
        );
        // echo "<pre>";print_r($lp_listingpro_options_status);exit;
        $updated_data = update_post_meta($post_ID,'lp_'.strtolower(THEMENAME).'_options_contactownerform',$lp_listingpro_options_status);

        $response   =   array(
            'update-query' => $updated_data,
            'success' => 1,
            'message' => 'Listing Approved Successfully!'
        );
        echo json_encode( $response ); exit;
    }
    public function proyoga_contactownereditedhandlers(){
        global $wpdb;
        //echo "<pre>";print_r(get_queried_object_id());exit;
        //echo "SELECT ID FROM $wpdb->posts WHERE post_title = '".$_POST."'";exit;
        // $query_sql  = $wpdb->get_var( $wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_title = '".$_POST['post_title']."'"));
        // echo "<pre>";print_r($query_sql);exit;
        
        $meta 				= 'lp_'.strtolower(THEMENAME).'_options';
        $custom_meta 		= 'lp_'.strtolower(THEMENAME).'_options_contactownerform_submit';//lp_listingpro_options_contactownerform
        $post_id            = $_POST['postID'];
        $post_status        = $_POST['post_status'];
        $post_ID            = $_POST['post_ID'];
        $POSTID             = $_POST['POSTID'];
        $listing_exportall_options_json  = get_post_meta( $POSTID, 'lp_listingpro_options', true );
        //echo "<pre>";print_r($listing_exportall_options_json);exit;
        $lp_listingpro_options = array(
            'tagline_text'          =>$listing_exportall_options_json['tagline_text'],
            'gAddress'              =>$listing_exportall_options_json['gAddress'],
            'latitude'              =>$listing_exportall_options_json['latitude'],
            'longitude'             =>$listing_exportall_options_json['longitude'],
            'website'               =>$_POST['website7'],
            'twitter'               =>$_POST['twitter7'],
            'facebook'              =>$_POST['facebook7'],
            'linkedin'              =>$_POST['linkedin7'],
            'instagram'             =>$_POST['instagram7'],
            'youtube'               =>$_POST['youtube7'],
            'pinterest'             =>$_POST['pinterest7'],
            'descriptions'          =>$_POST['descriptions7'],
            'gallery_image_ids'     =>$_POST['gallery_image_ids'],
            'listing_title'         =>$_POST['post_title'],
            'post_link'             =>$_POST['post_link'],
            'post_status'           =>$_POST['post_status'],
            'editProfileRequestType'=>$_POST['editProfileType'],
            'gallery'              =>$_POST['filename']
        );
        //echo 'SELECT * FROM '.$wpdb->posts .' Where post_title="'.$_POST['post_title'].'"ORDER BY ID DESC';exit;
        //echo "<pre>";print_r($lp_listingpro_options);exit;
        update_post_meta($post_id,$meta,$lp_listingpro_options);
        $lp_listingpro_options_status = array(
            'tagline_text'          =>$listing_exportall_options_json['tagline_text'],
            'gAddress'              =>$listing_exportall_options_json['gAddress'],
            'latitude'              =>$listing_exportall_options_json['latitude'],
            'longitude'             =>$listing_exportall_options_json['longitude'],
            'post_status'           =>$_POST['post_status'],
            'website'               =>$_POST['website7'],
            'twitter'               =>$_POST['twitter7'],
            'facebook'              =>$_POST['facebook7'],
            'linkedin'              =>$_POST['linkedin7'],
            'instagram'             =>$_POST['instagram7'],
            'youtube'               =>$_POST['youtube7'],
            'pinterest'             =>$_POST['pinterest7'],
            'descriptions'          =>$_POST['descriptions7'],
            'gallery_image_ids'     =>$_POST['gallery_image_ids'],
            'listing_title'         =>$_POST['post_title'],
            'post_link'             =>$_POST['post_link'],
            'editProfileRequestType'=>$_POST['editProfileType'],
            'gallery'              =>$_POST['filename']
        );
        //echo "<pre>";print_r($lp_listingpro_options_status);exit;
        $update_edit_request = update_post_meta($post_ID,'lp_'.strtolower(THEMENAME).'_options_contactownerform',$lp_listingpro_options_status);

        $update_edit_request_option = update_post_meta($POSTID,$meta,$lp_listingpro_options_status);
        $update_edit_request_option_submit_approval = update_post_meta($POSTID,$custom_meta,$lp_listingpro_options_status);
        //echo "<pre>";print_r($update_edit_request_option_submit_approval);exit;
        $response   =   array(
            'update-query' => $update_edit_request,
            'update-query-option'=>$update_edit_request_option,
            'update-query-submit-approval'=>$update_edit_request_option_submit_approval,
            'success' => 1,
            'message' => 'Listing Approved Successfully!'
        );
        echo json_encode( $response ); exit;
    }

    public function wpli_delete_taxonomy_terms(){
        ini_set('memory_limit', '-1');
        // Get Taxonomy
        $taxonomy =  ( isset($_REQUEST[ 'taxonomy' ]))?$_REQUEST[ 'taxonomy' ]:0;

        if( taxonomy_exists( $taxonomy ) ){
            $terms = get_terms( array(
                'taxonomy' => $taxonomy,
                'fields' => 'ids',
                'hide_empty' => false
            ));

            if( empty( $terms )){
                $response   =   array(
                    'response' => 0,
                    'message' => 'Terms do not exist'
                );
            }else{
                foreach( $terms as $term_id ) {
                    wp_delete_term( $term_id, $taxonomy );
                }

                $response   =   array(
                    'response' => 1,
                    'message' => 'Taxonomy terms have been deleted successfully'
                );
            }

            echo json_encode( $response ); exit;
        }else{
            $response   =   array(
                                'response' => 0,
                                'message' => 'Taxonomy does not exist'
                            );
        }

        echo json_encode( $response ); exit;
    }

    public function delete_media_attachments_records(){
        $attachments = get_posts( array('post_type'=>'attachment','numberposts'=>-1) );
        if(!empty($attachments)){
            foreach($attachments as $attachment){
                //echo "<pre>";print_r($attachment);exit;
                $eachattachment = get_post_meta($attachment->ID,'_thumbnail_id', true);
                if($eachattachment){
                    wp_delete_attachment($eachattachment,true );
                }
                wp_delete_post( $attachment->ID, true );
                echo 'Deleted = '.$attachment->ID;
                echo '<br/>';
            }
        }
    }


    public function website_checking_listing_records(){
        //echo "<pre>";print_r($_REQUEST['selectcategorychecker']);exit;
        ini_set('memory_limit', '-1');
        if(isset($_REQUEST['selectcategorychecker'])){
            $allListing = explode(",",$_REQUEST['selectcategorychecker']);
            $exportargs = array(
                'post_type' =>$allListing,
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'nopaging' => true,
            );

            // echo "<pre>";print_r($allListing);exit;
            $listingData = get_posts( $exportargs );
            $response =  array();
            $type = '';
            if(!empty($listingData)){
                foreach($listingData as $listing ){
                    // echo '<pre>';print_r($type);exit;
                    // echo "<pre>";print_r($listing);
                    $listing_slug = get_permalink(  $listing->ID );
                    $listing_websiteChecker  = get_post_meta( $listing->ID, 'listing_json', true );
                    $listing_websiteCheckerDecode = json_decode($listing_websiteChecker);   
                    // if(!isset($listing_websiteCheckerDecode->type)){
                        if (in_array($listing->post_type, $allListing)){
                            $type = 'yoga_'.$listing->post_type;
                        }
                    // }else{
                        // $type = $listing_websiteCheckerDecode->type;
                    // }
                    // echo'<pre>';print_r($type);

                    $title      = $listing->post_title;
                    $type       = $type;
                    $website    = $listing_websiteCheckerDecode->website;
                    $slug       = $listing_slug;
                    $facebook   = $listing_websiteCheckerDecode->facebook;
                    $instagram  = $listing_websiteCheckerDecode->instagram;
                    $twitter    = $listing_websiteCheckerDecode->twitter;
                    $linkedin   = $listing_websiteCheckerDecode->linkedin;
                    $pinterest  = $listing_websiteCheckerDecode->pinterest;
                    $lineDataWebChecker[] = array(
                        'Type'=>$type,
                        'Title'=>$title,
                        'Website'=>$website,
                        'Slug'=>$slug,
                        'Facebook'=>$facebook,
                        'Instagram'=>$instagram,
                        'Twitter'=>$twitter,
                        'Linkedin'=>$linkedin,
                        'Pinterest'=>$pinterest
                    );
                    
                    // echo "<pre>";print_r($lineDataWebChecker);exit;
                    $response[ 'success' ] =  1;
                    $response[ 'file_data' ] =  $lineDataWebChecker;
                }
                // exit;
                echo json_encode( $response ); exit;
            }
        }
    }


    public function wpli_admin_menu_order( $menu_order ) {

        if(!$menu_order) return true;
        //echo "<pre>";print_r($menu_order);exit;
        $new_positions = array(
            'file_upload' => 15,
            'proyoga'=>  5
        );

        function move_element(&$array, $a, $b) {
            $out = array_splice($array, $a, 1);
            array_splice($array, $b, 0, $out);
        }

        move_element($menu_order, 0, 14);
        return $menu_order;
    }

	public function wpli_savePostMeta($post_id) {
        //echo "<pre>";print_r($_REQUEST['content']);exit;
		global $listingpro_settings, $reviews_options, $listingpro_formFields, $claim_options, $ads_options, $page_options, $post_options, $price_plans_options,$post;

		$meta = 'lp_'.strtolower(THEMENAME).'_options';
		// verify nonce

		if (!isset($_POST['lp_meta_box_nonce']) || !wp_verify_nonce($_POST['lp_meta_box_nonce'], basename(__FILE__))) {
				return $post_id;
		}

		// check autosave
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return $post_id;
		}
		// check permissions
		if ('page' == $_POST['post_type']) {
				if (!current_user_can('edit_page', $post_id)) {
						return $post_id;
				}
		} elseif (!current_user_can('edit_post', $post_id)) {
				return $post_id;
		}

		if($_POST['post_type']=='lp-reviews'){
			$metaboxes_reviews = $reviews_options;

		}

		if($_POST['post_type']=='lp-ads'){
			$metaboxes = $ads_options;
		}

		if($_POST['post_type']=='listing'){
			$metaboxes = $listingpro_settings;
		}

        $post_type_array = array();
        $post_type_array[] = 'teacher';
        $post_type_array[] = 'studio';
        $post_type_array[] = 'school';
        $post_type_array[] = 'retreat';
        $post_type_array[] = 'restaurant';
        if( in_array($_POST['post_type'],$post_type_array ) ){
			$metaboxes = $listingpro_settings;
		}

		if($_POST['post_type']=='form-fields'){
			$metaboxes = $listingpro_formFields;
		}
		if($_POST['post_type']=='lp-claims'){
			$metaboxes = $claim_options;
		}
		if($_POST['post_type']=='page'){
			$metaboxes = $page_options;
		}
		if($_POST['post_type']=='post'){
			$metaboxes = $post_options;
		}
		if($_POST['post_type']=='price_plan'){
			$metaboxes = $price_plans_options;
		}
		if(!empty($metaboxes_reviews)) {
			$myMeta = array();

			foreach ($metaboxes_reviews as $metabox) {
				$myMeta[$metabox['id']] = isset($_POST[$metabox['id']]) ? $_POST[$metabox['id']] : "";
			}

			update_post_meta($post_id, $meta, $myMeta);

		}
		if(!empty($metaboxes)) {
			$myMeta = array();

			foreach ($metaboxes as $metabox) {
				$myMeta[$metabox['id']] = isset($_POST[$metabox['id']]) ? $_POST[$metabox['id']] : "";
			}

			update_post_meta($post_id, $meta, $myMeta);
			if(isset($_POST['lp_form_fields_inn'])){
				$metaFields = 'lp_'.strtolower(THEMENAME).'_options_fields';
				$fields = $_POST['lp_form_fields_inn'];
				$filterArray = lp_save_extra_fields_in_listing($fields, $post_id);
				$fields = array_merge($fields,$filterArray);

				update_post_meta($post_id, $metaFields, $fields);
			}else{
				$metaFields = 'lp_'.strtolower(THEMENAME).'_options_fields';
				update_post_meta($post_id, $metaFields, '');
			}
		}
        ini_set('memory_limit', '-1');
        $cat_list_array['teacher']      = 'Yoga Teacher';
        $cat_list_array['studio']       = 'Yoga Studio';
        $cat_list_array['school']       = 'Yoga School';
        $cat_list_array['retreat']      = 'Yoga Retreat';
        $cat_list_array['restaurant']   = 'Restaurant';
        $listing_options =  get_post_meta($post_id, $meta, true );
        $metaGallery = get_post_meta( $post_id, 'gallery_image_ids', true );
        $listing_tagline = $listing_options['tagline_text'];
        $listing__content = $_REQUEST['content'];
        //echo "<pre>";print_r($listing__content);exit;
        // if($listing_tagline > 0){
        //     echo "<pre>";print_r($listing_tagline);exit;
        //     $listingtagline = explode(',', $listing_tagline);
        //     $latlistingtaglines = end($listingtagline);
        // }
        $location_detail_json = $_POST['latitude'] .','. $_POST['longitude']. ',' . $_POST['gAddress'];
        $tag_line_address = $_POST['gAddress'];
        //echo "<pre>";print_r($listing_tagline);exit;
        //echo "<pre>";print_r($tag_line_address);exit;
        //echo "<pre>";print_r($_POST);exit;
        $title_array = explode( '-' , $_POST['post_title'] );
        //echo "<pre>";print_r($title_array);exit;
        // $update_tagline = $title_array[0].' is a '.ucwords(str_replace('_',' ',$cat_list_array[$_POST['post_type']])).' in '.$tag_line_address;
        //$update_tagline = $title_array[0].' is a '.ucwords(str_replace('_',' ',$cat_list_array[$_POST['post_type']])).' in'.$title_array[1].','.$listing_tagline;
        $update_tagline = $listing_tagline;
        //echo "<pre>";print_r($update_tagline);exit;
        //echo "<pre>";print_r($update_tagline);exit;
        // $listing_content =  $title_array[0].' is a '.ucwords(str_replace('_',' ',$cat_list_array[$_POST['post_type']])).' in '.$tag_line_address;
        //$listing_content =  $title_array[0].' is a '.ucwords(str_replace('_',' ',$cat_list_array[$_POST['post_type']])).' in'.$title_array[1].','.$latlistingtaglines;
        $listing_content =  $listing__content;
        //echo "<pre>";print_r($listing_content);exit;
        //$title =$title_array[0].'-'.$tag_line_address;
        $title =$_POST['post_title'];
        //$slug_new = $title.'-'.$tag_line_address;
        $slug_new = $title;
        $numbers = explode(',', $_POST['phone']);
        $update_phone = array();
        if(!empty($numbers)){
            foreach($numbers as $number){      
                /*$phoneNumber = preg_replace('/[^0-9]/','',$number);
                if(strlen($phoneNumber) > 10){
                    $countryCode = substr($phoneNumber, 0, strlen($phoneNumber)-10);
                    $areaCode = substr($phoneNumber, -10, 3);
                    $nextThree = substr($phoneNumber, -7, 3);
                    $lastFour = substr($phoneNumber, -4, 4);
                    $phoneNumber = '+'.$countryCode.' ('.$areaCode.') '.$nextThree.'-'.$lastFour;
                }else if(strlen($phoneNumber) == 10) {
                    $areaCode = substr($phoneNumber, 0, 3);
                    $nextThree = substr($phoneNumber, 3, 3);
                    $lastFour = substr($phoneNumber, 6, 4);
                    $phoneNumber = '('.$areaCode.') '.$nextThree.'-'.$lastFour;
                }else if(strlen($phoneNumber) == 7) {
                    $nextThree = substr($phoneNumber, 0, 3);
                    $lastFour = substr($phoneNumber, 3, 4);
                    $phoneNumber = $nextThree.'-'.$lastFour;
                }
                $update_phone[] =  $phoneNumber;
                */
                $number_format = preg_replace("/[^0-9 _.,\-!()+= ]/", "",$number);
                $update_phone[] =  $number_format;
                /*$number_format =  "\n";
                if(preg_match("/^\+\d(\d{3})(\d{3})(\d{4})$/", $number,$value)){
                    $number_format = $value[1].'-'.$value[2].'-'.$value[3];
                    // return $number_format;
                    $update_phone[] =  $number_format;
                    //echo "<pre>";print_r($update_phone);
                }*/
            }
            // print_r($update_phone);
            // exit;
        }
      
        //print_r($slug_new);exit;
        remove_action('save_post', array($this,'wpli_savePostMeta'));
        $listing_post = array(
            'ID'           => $post_id,
            'post_title'   => $title,
            'post_name'    => $slug_new,
            'post_content' => $listing_content,
        );

        wp_update_post( $listing_post, true );

        // echo "<pre>";print_r($_POST['content']);exit;
        $listing_options[ 'tagline_text' ] = $update_tagline;
        $listing_options[ 'phone' ] = implode( ',', $update_phone );
        if($metaGallery == false){
            $gallery_image_ids = '110688';
            if(!empty($gallery_image_ids)){
                add_post_meta($post_id,'gallery_image_ids',$gallery_image_ids);
            }
        }else{
            $listing_options[ 'gallery' ] = $_POST['gallery'];
        }
        update_post_meta($post_id, $meta, $listing_options);
        add_action('save_post',array($this,'wpli_savePostMeta'));
	}

    public function wpli_postsettings_admin_scripts(){
        //echo "<pre>";print_r('hello world!@!!!!!!!');exit;
        global $post,$pagenow;

        if (current_user_can('edit_posts') && ($pagenow == 'post-new.php' || $pagenow == 'post.php' )) {
			    if( isset($post) ) {
					wp_localize_script( 'jquery', 'script_data', array(
						'post_id' => $post->ID,
						'nonce' => wp_create_nonce( 'lp-ajax' ),
						'image_ids' => get_post_meta( $post->ID, 'gallery_image_ids', true ),
						'label_create' => __("Create Featured Gallery", "proyoga-plugin"),
						'label_edit' => __("Edit Featured Gallery", "proyoga-plugin"),
						'label_save' => __("Save Featured Gallery", "proyoga-plugin"),
						'label_saving' => __("Saving...", "proyoga-plugin")
					));
				}


            //wp_register_script('post-metaboxes', plugins_url( 'assets/js/metaboxes.js', dirname(dirname(__FILE__) )));
            wp_register_script( 'post-metaboxes', plugins_url( 'public/assets/js/metaboxes.js', APP_RELPATH ) );

            wp_enqueue_script('post-metaboxes');
			global $listingpro_options;
			$mapAPI = '';
			$mapAPI = $listingpro_options['google_map_api'];
            //print_r($mapAPI);exit;
			if(empty($mapAPI)){
				$mapAPI = 'AIzaSyBMQKkIzg5Xty9iK4Uq0Z75785o2PXAdX4';
			}
			wp_enqueue_script('maps', 'https://maps.googleapis.com/maps/api/js?key='.$mapAPI.'&libraries=places', 'jquery', '', false);

			if (current_user_can('edit_posts') && ($pagenow == 'edit.php' && isset($_GET['page']) && $_GET['page'] == 'listing-claims')) {
				wp_enqueue_script('bootstrapadmin', get_template_directory_uri() . '/assets/lib/bootstrap/js/bootstrap.min.js', 'jquery', '', true);
			}
            $post_type_array = array();
            $post_type_array[] = 'listing';
            $post_type_array[] = 'teacher';
            $post_type_array[] = 'studio';
            $post_type_array[] = 'school';
            $post_type_array[] = 'retreat';
            $post_type_array[] = 'restaurant';
			if (current_user_can('edit_posts') && ($pagenow == 'post-new.php' || $pagenow == 'post.php')) {
				if ( in_array($post->post_type,$post_type_array) || 'lp-reviews' === $post->post_type   || 'lp-ads' === $post->post_type || 'lp-claims' === $post->post_type || 'events' == $post->post_type ) {
					//wp_enqueue_script('accorianui', plugins_url( 'assets/js/jqueryuiaccordian.js', dirname(dirname(__FILE__) )), 'jquery', '', true);
					wp_enqueue_script('jquery-ui', get_template_directory_uri() . '/assets/js/jquery-ui.js', 'jquery', '', true);
                   // wp_enqueue_script('jquery-ui-trigger', get_template_directory_uri() . '/assets/js/jquery-ui-trigger.js', 'jquery', '', true);
                    wp_localize_script(	'jquery-ui-trigger','global',array('ajax' => admin_url( 'admin-ajax.php' ),));
					wp_enqueue_script('jquery-droppin', get_template_directory_uri() . '/assets/js/drop-pin.js', 'jquery', '', true);
				}

			}

        }
    }

    function wpli_postsettings_admin_styles(){
        global $pagenow;
        if (current_user_can('edit_posts') && ($pagenow == 'post-new.php' || $pagenow == 'post.php' || $pagenow == 'edit-tags.php'|| $pagenow == 'term.php' )) {
            //wp_register_style('post-metaboxes', plugins_url( 'assets/css/metaboxes.css', dirname(dirname(__FILE__) )), false, '1.00', 'screen');
            wp_register_style( 'post-metaboxes', plugins_url( 'public/assets/css/metaboxes.css', APP_RELPATH ) );


			// wp_register_style('select2-metaboxes', plugins_url( 'assets/css/select2.css', dirname(dirname(__FILE__) )), false, '1.00', 'screen');
           // wp_register_style('prettify-metaboxes', plugins_url( 'assets/css/prettify.css', dirname(dirname(__FILE__) )), false, '1.00', 'screen');
            wp_enqueue_style('post-metaboxes');
			 //wp_enqueue_style('select2-metaboxes');
            //wp_enqueue_style('prettify-metaboxes');

        }

		if (current_user_can('edit_posts') && ($pagenow == 'edit.php' && isset($_GET['page']) && $_GET['page'] == 'listing-claims')) {
			wp_enqueue_style('bootstrapcss', get_template_directory_uri() . '/assets/lib/bootstrap/css/bootstrap.min.css');
		}
		if (current_user_can('edit_posts') && ($pagenow == 'post-new.php' || $pagenow == 'post.php')) {
					wp_enqueue_style('jquery-ui');

		}
    }
    public function wpli_listingpro_settings_box() {
        global $listingpro_settings;
        $listingpro_settings = Array(
            Array(
                'name' => esc_html__('Business Tagline Text', 'proyoga-plugin'),
                'id' => 'tagline_text',
                'type' => 'text',
                'desc' => 'Your Business One liner'),

            Array(
                'name' => esc_html__('Listing Address', 'proyoga-plugin'),
                'id' => 'gAddress',
                'type' => 'text',
                'desc' => 'Listing address for map'),
            Array(
                'name' => esc_html__('Latitude', 'proyoga-plugin'),
                'id' => 'latitude',
                'type' => 'text',
                'desc' => ''),
            Array(
                'name' => esc_html__('Longitude', 'proyoga-plugin'),
                'id' => 'longitude',
                'type' => 'text',
                'desc' => ''),

            Array(
                'name' => esc_html__('Map Pin', 'proyoga-plugin'),
                'id' => 'wpli-mappin',
                'type' => 'mappinbuton',
                'desc' => ''),

            Array(
                'name' => esc_html__('Phone', 'proyoga-plugin'),
                'id' => 'phone',
                'type' => 'text',
                'desc' => 'Please fill only numbers and symbols like +( )-'),
            /*
            Array(
                'name' => esc_html__('Whatsapp', 'proyoga-plugin'),
                'id' => 'whatsapp',
                'type' => 'text',
                'desc' => ''),
            */
            Array(
                'name' => esc_html__('Email', 'proyoga-plugin'),
                'id' => 'email',
                'type' => 'text',
                'desc' => 'This Email is not for public'),
            Array(
                'name' => esc_html__('Website', 'proyoga-plugin'),
                'id' => 'website',
                'type' => 'text',
                'desc' => 'Your website URL'),
            Array(
                'name' => esc_html__('Image Gallery', 'proyoga-plugin'),
                'id' => 'gallery',
                'type' => 'gallery',
                'desc' => esc_html__('Select images to present your buisness online.', 'proyoga-plugin'),
            ),

            Array(
                'name' => esc_html__('Twitter', 'proyoga-plugin'),
                'id' => 'twitter',
                'type' => 'text',
                'desc' => 'Your twitter URL'),
            Array(
                'name' => esc_html__('Facebook', 'proyoga-plugin'),
                'id' => 'facebook',
                'type' => 'text',
                'desc' => 'Your facebook URL'),
            Array(
                'name' => esc_html__('LinkedIn', 'proyoga-plugin'),
                'id' => 'linkedin',
                'type' => 'text',
                'desc' => 'Your Linkedin URL'),
            Array(
                'name' => esc_html__('Youtube Channel Link', 'proyoga-plugin'),
                'id' => 'youtube',
                'type' => 'text',
                'desc' => esc_html__('Your Youtube Channel URL as social link', 'proyoga-plugin'),
            ),
            Array(
                'name' => esc_html__('Instagram Profile Link', 'proyoga-plugin'),
                'id' => 'instagram',
                'type' => 'text',
                'desc' => esc_html__('Your Instagram Profile URL as social link', 'proyoga-plugin'),
            ),
            Array(
                'name' => esc_html__('Pinterest Profile Link', 'proyoga-plugin'),
                'id' => 'pinterest',
                'type' => 'text',
                'desc' => esc_html__('Your Pinterest Profile URL as social link', 'proyoga-plugin'),
            ),
/*
            Array(
                'name' => esc_html__('Youtube Channel Link', 'proyoga-plugin'),
                'id' => 'youtube',
                'type' => 'text',
                'desc' => esc_html__('Your Youtube Channel URL as social link', 'proyoga-plugin'),
            ),
            Array(
                'name' => esc_html__('Instagram Profile Link', 'proyoga-plugin'),
                'id' => 'instagram',
                'type' => 'text',
                'desc' => esc_html__('Your Instagram Profile URL as social link', 'proyoga-plugin'),
            ),
            Array(
                'name' => esc_html__('Youtube Video URL', 'proyoga-plugin'),
                'id' => 'video',
                'type' => 'text',
                'desc' => esc_html__('Any specific Youtube Video? You want to share on business page', 'proyoga-plugin'),
            ),


            Array(
                'name' => esc_html__('Show Price Status', 'proyoga-plugin'),
                'id' => 'price_status',
                'type' => 'select',
                'std' => 'Price Range',
                'options' => array(
                    'notsay' => 'Prefer not to say',
                    'inexpensive' => ''.$lp_priceSymbol.' Inexpensive',
                    'moderate' => ''.$lp_priceSymbol2.' Moderate',
                    'pricey' => ''.$lp_priceSymbol3.' Pricey',
                    'ultra_high_end' => ''.$lp_priceSymbol4.' Ultra High-End',
                ),
                'desc' => esc_html__('It will show your business price range', 'proyoga-plugin')
            ),

            Array(
                'name' => esc_html__('Price From', 'proyoga-plugin'),
                'id' => 'list_price',
                'type' => 'Text',
                'desc' => esc_html__('Ignore this if your buisness does not have any specific price to show', 'proyoga-plugin')
            ),
            Array(
                'name' => esc_html__('Price To', 'proyoga-plugin'),
                'id' => 'list_price_to',
                'type' => 'Text',
                'desc' => esc_html__('Ignore this if your buisness does not have any specific price to show', 'proyoga-plugin')
            ),
            Array(
                'name' => esc_html__('Plans', 'proyoga-plugin'),
                'id' => 'Plan_id',
                'type' => 'select',
                'options' => $priceplans,
                'desc' => esc_html__('Ignore this if this post will not be a paid one', 'proyoga-plugin')
            ),
            */
            Array(
                'name' => esc_html__('Listing\'s Duration', 'proyoga-plugin'),
                'id' => 'lp_purchase_days',
                'type' => 'hidden',
                'default' => '',
                'desc' => esc_html__('This is Listing Duration which were associated with plan', 'proyoga-plugin')
            ),
            /*
            Array(
                'name' => esc_html__('Reviews', 'proyoga-plugin'),
                'id' => 'reviews_ids',
                'type' => 'array',
                'desc' => ''),
            */
            Array(
                'name' => esc_html__('Verify Listing', 'proyoga-plugin'),
                'id' => 'claimed_section',
                'type' => 'select',
                'std' => 'Verify Listing',
                'options' => array(
                    'not_claimed' => 'Not Claimed',
                    'claimed' => 'Claimed'
                ),
                'default'=> 'not_claimed',
                'desc' => esc_html__('Approve claim at claim section this will override complete claim process', 'proyoga-plugin')
            ),
            Array(
                'name' => esc_html__('Timings', 'proyoga-plugin'),
                'id' => 'business_hours',
                'type' => 'timings',
                'desc' => esc_html__('Set Your business time details', 'proyoga-plugin')
            ),
            
            /*

            Array(
                'name' => esc_html__('Ad Purchased on', 'proyoga-plugin'),
                'id' => 'listings_ads_purchase_date',
                'type' => 'hidden',
                'default'	=>'',
                'desc' => ''),
            Array(
                'name' => esc_html__('Ad Purchased Packages', 'proyoga-plugin'),
                'id' => 'listings_ads_purchase_packages',
                'type' => 'hidden',
                'default'	=>'',
                'desc' => ''),
            Array(
                'name' => esc_html__('Faq', 'proyoga-plugin'),
                'id' => 'wali-faqs',
                'type' => 'faqs',
                'desc' => ''),

            Array(
                'name' => esc_html__('Copmain ID', 'proyoga-plugin'),
                'id' => 'campaign_id',
                'type' => 'hidden',
                'desc' => '',
            ),
            Array(
                'name' => esc_html__('Changed Plan ID', 'proyoga-plugin'),
                'id' => 'changed_planid',
                'type' => 'hidden',
                'desc' => '',
            ),

            Array(
                'name' => esc_html__('Reported By', 'proyoga-plugin'),
                'id' => 'listing_reported_by',
                'type' => 'hidden',
                'default' => '0',
                'desc' => ''),

            Array(
                'name' => esc_html__('Reported Count', 'proyoga-plugin'),
                'id' => 'listing_reported',
                'type' => 'hidden',
                'default' => '0',
                'desc' => ''),
            */


        );

        $b_logo =   lp_theme_option('business_logo_switch');
        if( $b_logo == 1 )
        {
            $listingpro_settings[]  =   Array(
                'name' => esc_html__('Business Logo', 'proyoga-plugin'),
                'id' => 'business_logo',
                'type' => 'file',
                'desc' => esc_html__('Select business logo for your listing.', 'proyoga-plugin'),
            );
        }

        $listingpro_settings[]  =   Array(
            'name' => esc_html__('Comments', 'proyoga-plugin'),
            'id' => 'comments',
            'type' => 'text',
            // 'desc' => esc_html__('Comments.', 'proyoga-plugin'),
        );

        $listingpro_settings[]  =   Array(
            'name' => esc_html__('Yoga tab', 'proyoga-plugin'),
            'id' => 'yoga_content_tab',
            'type' =>'textarea_2',
            'desc' => esc_html__('Description for Yoga Tab', 'proyoga-plugin')
        );
        //wp_editor( 'This is the default text!', 'editor', $listingpro_settings);
        // $post_type_array = array();
        // $post_type_array[] = 'listing';
        // $post_type_array[] = 'teacher';
        // $post_type_array[] = 'studio';
        // $post_type_array[] = 'school';
        // $post_type_array[] = 'retreat';
        //echo "<pre>";print_r($post);exit;
        add_meta_box('teacher_meta_settings',esc_html__( 'Listing settings', 'proyoga-plugin' ),array($this,'listingteacherpro_metabox_render'),'teacher','normal','high',$listingpro_settings);
        add_meta_box('studio_meta_settings',esc_html__( 'listing settings', 'proyoga-plugin' ),array($this,'listingstudiopro_metabox_render'),'studio','normal','high',$listingpro_settings);
        add_meta_box('school_meta_settings',esc_html__( 'listing settings', 'proyoga-plugin' ),array($this,'listingschoolpro_metabox_render'),'school','normal','high',$listingpro_settings);
        add_meta_box('retreat_meta_settings',esc_html__( 'listing settings', 'proyoga-plugin' ),array($this,'listingretreatpro_metabox_render'),'retreat','normal','high',$listingpro_settings);
        add_meta_box('restaurant_meta_settings',esc_html__( 'Restaurant settings', 'proyoga-plugin' ),array($this,'listingrestaurantpro_metabox_render'),'restaurant','normal','high',$listingpro_settings);
    }

    public function listingteacherpro_metabox_render($post, $metabox) {
        //echo '<pre>'; print_r( $metabox ); echo '</pre>';
        global $post;
        $options = get_post_meta($post->ID, 'lp_'.strtolower(THEMENAME).'_options', true);
        ?>
        <input type="hidden" name="lp_meta_box_nonce" value="<?php echo wp_create_nonce(basename(__FILE__));?>" />
        <table class="form-table lp-metaboxes">
            <tbody>
            <?php
            if( !empty($metabox['args'])){
                foreach ($metabox['args'] as $settings) {
                    $settings['value'] = isset($options[$settings['id']]) ? $options[$settings['id']] : (isset($settings['std']) ? $settings['std'] : '');
                    call_user_func('settings_'.$settings['type'], $settings);
                }
            }
            ?>
            </tbody>
        </table>
        <?php
    }

    public function listingstudiopro_metabox_render($post, $metabox) {
        //echo '<pre>'; print_r( $metabox ); echo '</pre>';
        global $post;
        $options = get_post_meta($post->ID, 'lp_'.strtolower(THEMENAME).'_options', true);
        ?>
        <input type="hidden" name="lp_meta_box_nonce" value="<?php echo wp_create_nonce(basename(__FILE__));?>" />
        <table class="form-table lp-metaboxes">
            <tbody>
            <?php
            if( !empty($metabox['args'])){
                foreach ($metabox['args'] as $settings) {
                    $settings['value'] = isset($options[$settings['id']]) ? $options[$settings['id']] : (isset($settings['std']) ? $settings['std'] : '');
                    call_user_func('settings_'.$settings['type'], $settings);
                }
            }
            ?>
            </tbody>
        </table>
        <?php
    }
    public function listingretreatpro_metabox_render($post, $metabox) {
        //echo '<pre>'; print_r( $metabox ); echo '</pre>';
        global $post;
        $options = get_post_meta($post->ID, 'lp_'.strtolower(THEMENAME).'_options', true);
        ?>
        <input type="hidden" name="lp_meta_box_nonce" value="<?php echo wp_create_nonce(basename(__FILE__));?>" />
        <table class="form-table lp-metaboxes">
            <tbody>
            <?php
            if( !empty($metabox['args'])){
                foreach ($metabox['args'] as $settings) {
                    $settings['value'] = isset($options[$settings['id']]) ? $options[$settings['id']] : (isset($settings['std']) ? $settings['std'] : '');
                    call_user_func('settings_'.$settings['type'], $settings);
                }
            }
            ?>
            </tbody>
        </table>
        <?php
    }

    public function comments_post_edit_page( $post ) {
        echo "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js'></script>
            <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css'>
            <div id='postcomments' class='modal' style=''>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <h3>Comments</h3>
                        <span class='close_add_new_live'>&times;</span>
                    </div>
                    <input name='filter_action' id='postedit-myBtn-submit' class='button pull-right' value='Add New Comment'>
                    <table class='comments-listings' id ='comments-post-edit-listings'>
                        <thead>
                            <tr>
                                <th>Comments</th>
                                <th>Time-Stamp</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id='listingtbody'>

                        </tbody>
                    </table>
                </div>
            </div>
            <div id='editpostmyModal' class='modal'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <h2>Add Comments</h2>
                        <span class='close_add_new'>&times;</span>
                    </div>
                    <div class='modal-body'>
                        <textarea id='wreview' name='wreview' rows='10' cols='90'></textarea>
                        <input type='hidden' value='$posts->post_type' name ='posttypecomment' id='posttypecomment'>
                    </div>
                    <div class='modal-footer'>
                        <button id='editpostsave' class='button' type='button' name='save-details'>Save</button>
                    </div>
                </div>
            </div>
            <script>
            $(function() {
                $('#comments').after('<a post_id=$post->ID class=comment-post-edit-popup id=comment-popup title=Comments>Comment</a>');
                $('.comment-post-edit-popup').click(function(e) {
                    e.preventDefault();
                    comment_post_id = $(this).attr('post_id');
                    getPostEditContent(comment_post_id);
                });
                $(document).on('click', '.close_add_new_live', function() {
                    $('#postcomments').removeClass('show');
                    $('#postcomments').attr('style','display:none;');
                });
                $(document).on('click', '.close_add_new', function() {
                    $('#editpostmyModal').removeClass('show');
                    $('#editpostmyModal').attr('style','display:none;');
                });
                function getPostEditContent(comment_post_id) {
                    jQuery.ajax({
                        url: export_all_ajax_object.ajaxurl,
                        type: 'post',
                        data: 'action=getTheContent&query_var=' + comment_post_id,
                        beforeSend: function() {
                            $('#loading-image').show();
                        },
                        success: function(res) {
                            var results = JSON.parse(res);
                            console.log(results);
                            $('#postcomments').addClass('show');
                            $('#postcomments').attr('style','display:block;');
                            $('#comments-post-edit-listings tbody').empty();
                            for (var i = 0; i < results.length; i++) {
                                console.log(results);
                                $('#comments-post-edit-listings tbody').append('<tr id=listing_' + results[i].id + '><td>' + results[i].comment + '</td><td>' + results[i].timestamp + '</td><td><a post_id=' + results[i].id + ' class=add  title= data-toggle=tooltip data-original-title=Add><i class=material-icons>&#xE03B;</i></a><a post_id=' + results[i].id + ' class=edit title=Edit data-toggle=tooltip><i class=material-icons>&#xE254;</i></a><a post_id=' + results[i].id + ' class=delete title=Delete data-toggle=tooltip><i class=material-icons>&#xE872;</i></a><a id=delete_action_' + results[i].id + ' post_id=' + results[i].id + ' class=save-btn title=save data-toggle=tooltip><button class=save-yes post_id=' + results[i].id + ' value=yes>Yes</button><button  post_id=' + results[i].id + ' class=save-no value=no>No</button></a> </td></tr>');
                            }
                        }

                    });
                }
            });
            </script>
        ";
    }
    
    public function listingschoolpro_metabox_render($post, $metabox) {
        //echo '<pre>'; print_r( $metabox ); echo '</pre>';
        global $post;
        $options = get_post_meta($post->ID, 'lp_'.strtolower(THEMENAME).'_options', true);

        ?>
        <input type="hidden" name="lp_meta_box_nonce" value="<?php echo wp_create_nonce(basename(__FILE__));?>" />
        <table class="form-table lp-metaboxes">
            <tbody>
            <?php
            if( !empty($metabox['args'])){
                foreach ($metabox['args'] as $settings) {
                    $settings['value'] = isset($options[$settings['id']]) ? $options[$settings['id']] : (isset($settings['std']) ? $settings['std'] : '');
                    call_user_func('settings_'.$settings['type'], $settings);
                }
            }
            // echo '<pre>'; print_r( $settings['value'] ); echo '</pre>';exit;
            ?>
            </tbody>
        </table>
        <?php
    }
    public function listingrestaurantpro_metabox_render($post, $metabox) {
        //echo '<pre>'; print_r( $metabox ); echo '</pre>';
        global $post;
        $options = get_post_meta($post->ID, 'lp_'.strtolower(THEMENAME).'_options', true);
        ?>
        <input type="hidden" name="lp_meta_box_nonce" value="<?php echo wp_create_nonce(basename(__FILE__));?>" />
        <table class="form-table lp-metaboxes">
            <tbody>
            <?php
            if( !empty($metabox['args'])){
                foreach ($metabox['args'] as $settings) {
                    $settings['value'] = isset($options[$settings['id']]) ? $options[$settings['id']] : (isset($settings['std']) ? $settings['std'] : '');
                    call_user_func('settings_'.$settings['type'], $settings);
                }
            }
            ?>
            </tbody>
        </table>
        <?php
    }


    public function wpli_listingpro_settings_formfields() {
        global $listingpro_formFields;
        $showInFilter = Array(
            'name'          => '',
            'id'            => 'lp-showin-filter',
            'type'          => 'hidden',
            'desc' => ''
        );
        $showFieldsInFilter = lp_theme_option('enable_extrafields_filter');
        if(!empty($showFieldsInFilter)){
            $showInFilter = Array(
                'name'          => __( 'Show in Filter', 'proyoga-plugin' ),
                'id'            => 'lp-showin-filter',
                'type'          => 'select',
                'child_of'=> '',
                'options'=>array(
                    'notshowinfilter'=> __('No', 'proyoga-plugin'),
                    'displaytofilt'=> __('Yes', 'proyoga-plugin'),
                ),
                'desc' => ''
            );
        }
        $listingpro_formFields = Array(


            Array(
                'name'          => __( 'Field Type', 'proyoga-plugin' ),
                'id'            => 'field-type',
                'type'          => 'select',
                'child_of'=> '',
                'options'       => array(
                    'text'                              => __( 'Text', 'proyoga-plugin' ),
                    //'textarea'                          => __( 'Textarea', 'proyoga-plugin' ),
                    //'wysiwyg'                           => __( 'TinyMCE wysiwyg editor', 'proyoga-plugin' ),
                    /* 			'text_time'                         => __( 'Time picker', 'proyoga-plugin' ),
                                'select_timezone'                   => __( 'Timezone', 'proyoga-plugin' ),
                                'text_date_timestamp'               => __( 'Date', 'proyoga-plugin' ),
                                'text_datetime_timestamp'           => __( 'Date and time', 'proyoga-plugin' ),
                                'text_datetime_timestamp_timezone'  => __( 'Date, time and timezone', 'proyoga-plugin' ), */
                    //'color'                       => __( 'Colorpicker', 'proyoga-plugin' ),
                    'check'                          => __( 'Checkbox', 'proyoga-plugin' ),
                    'checkbox'                          => __( 'Checkbox (Switch On/Off)', 'proyoga-plugin' ),
                    'checkboxes'                        => __( 'Multicheck', 'proyoga-plugin' ),
                    'radio'                             => __( 'Radio', 'proyoga-plugin' ),
                    'select'                            => __( 'Drop-Down', 'proyoga-plugin' ),
                ),
                'desc' => ''
            ),
            Array(
                'name'          => __( 'Radio Options', 'proyoga-plugin' ),
                'id'            => 'radio-options',
                'type'          => 'textarea',
                'child_of'=> 'field-type',
                'match'=>'radio',
                'desc' => 'Comma separated options if type support choices'
            ),
            Array(
                'name'          => __( 'Select Options', 'proyoga-plugin' ),
                'id'            => 'select-options',
                'type'          => 'textarea',
                'child_of'=> 'field-type',
                'match'=>'select',
                'desc' => 'Comma separated options if type support choices'
            ),
            Array(
                'name'          => __( 'Multicheck Options', 'proyoga-plugin' ),
                'id'            => 'multicheck-options',
                'type'          => 'textarea',
                'child_of'=> 'field-type',
                'match'=>'checkboxes',
                'desc' => 'Comma separated options if type support choices'
            ),
            Array(
                'name'          => __( 'Exclusive from Categories', 'proyoga-plugin' ),
                'id'            => 'exclusive_field',
                'type'          => 'check',
                'child_of'=> '',
                'desc' => ''
            ),
            Array(
                'name'          => __( 'Select category for this field', 'proyoga-plugin' ),
                'id'            => 'field-cat',
                'type'          => 'checkboxes',
                'child_of'=> '',
                'options'=>listing_get_cat_array(),
                'desc' => ''
            ),
            $showInFilter
        );

        add_meta_box('teacher_meta_settings',esc_html__(  'Custom Form Fields:', 'proyoga-plugin' ),array($this,'listingteacherpro_custom_metabox_render'),'form-fields','normal','high',$listingpro_formFields);
    }

    public function listingteacherpro_custom_metabox_render($post, $metabox) {
        // echo '<pre>'; print_r( $metabox ); echo '</pre>';
        global $post;
        $options = get_post_meta($post->ID, 'lp_'.strtolower(THEMENAME).'_options', true);
        ?>
        <input type="hidden" name="lp_meta_box_nonce" value="<?php echo wp_create_nonce(basename(__FILE__));?>" />
        <table class="form-table lp-metaboxes">
            <tbody>
            <?php
            //echo "<pre>";print_r($options);exit;
            //$metabox['args'] = options();
            if( !empty($metabox['args'])){
                foreach ($metabox['args'] as $settings) {
                    $settings['value'] = isset($options[$settings['id']]) ? $options[$settings['id']] : (isset($settings['std']) ? $settings['std'] : '');
                    call_user_func('settings_'.$settings['type'], $settings);
                }
            }
            ?>
            </tbody>
        </table>
        <?php
    }

    public function listing_admin_menu() {
        //$icon = plugin_dir_path( __DIR__ ).'public/assets/images/importer.svg';
        add_menu_page(__('Post Importer','WPLI'),__('Post Importer','WPLI'),'manage_options','file_upload',array($this,'app_builder_create_page'),'',"",2);
        add_submenu_page( 'file_upload', __('Website Checker','WPLI'),__('Website Checker','WPLI'),'manage_options', 'wpli_website_checker',array($this,'app_builder_create_website_checker'));
        add_submenu_page( 'file_upload', __('Restricted Usernames','WPLI'),__('Restricted Usernames','WPLI'),'manage_options', 'wpli_restricted_usernames',array($this,'app_builder_create_restricted_usernames'));
    }

    public function delete_comments_listing_records(){
        global $wpdb;
        if(isset($_REQUEST['saveDelete'])){
            $deleteId = $_REQUEST['saveDelete'];
            $table = $wpdb->prefix.'listing_comments';
            //echo "select * from $table where id='".$deleteId."'";exit;
            $myposts = $wpdb->query( $wpdb->prepare("DELETE FROM $table where id='".$deleteId."'"));
            if(!empty($myposts)){
                $response[ 'success' ]  = "1";
                $response[ 'msg' ]  = 'Row Deleted Successfully!';
            }else{
                $response[ 'fail' ]  = "0";
                $response[ 'msg' ]  = 'Row Deleted failed !';
            }
            echo json_encode( $response );exit;
        }
    }

    public function update_comments_listing_records(){
        global $wpdb;
       if(isset($_REQUEST['addnewcomments']) && isset($_REQUEST['addTable'])){
           //echo "<pre>";print_r($_REQUEST);exit;
           $addId = $_REQUEST['addTable'];
           $updateComments = preg_replace('@[/\\\]@','',$_REQUEST['addnewcomments']);
           
           $table = $wpdb->prefix.'listing_comments';
           //$sql = $wpdb->query($wpdb->prepare("UPDATE `$table` SET comment ='$updateComments' WHERE id ='$addId'"));           
           $sql = $wpdb->update($table, array( 'comment' => $updateComments), array('id' => $addId));
           if(!empty($sql)){
            $response[ 'success' ]  = "1";
            $response[ 'msg' ]  = 'Data Updated Successfully!';
            $response[ 'comment' ]  = $updateComments;
            $response[ 'timnestamp' ]  = date( 'Y-m-d H:i:s',time());
            
        }else{
            $response[ 'fail' ]  = "0";
            $response[ 'msg' ]  = 'Data Updated failed !';
        }
        echo json_encode( $response );exit;
       }
    }

    public function getTheContent_handler(){
        global $wpdb;
        $listingarray =array();
        if(isset($_REQUEST['query_var'])){
            $listing_id   = $_REQUEST['query_var'];
            //echo 'SELECT id,comment,timestamp FROM '.$wpdb->prefix.'listing_comments WHERE listing_id = "'.$listing_id.'"';exit;
            $sql = $wpdb->get_results('SELECT id,comment,timestamp FROM '.$wpdb->prefix.'listing_comments WHERE listing_id = "'.$listing_id.'"');
            //print_r($sql);exit;
            if(!empty($sql)){
                foreach($sql as $row){
                    $listingarray[]=$row;
                }
            }
        }
        echo json_encode($listingarray);exit;
    }

    public function comments_action_row($actions,$post){
        global $wpdb;
        //print_r('hjehklehwlr');exit;
        $post_type_array = array();
        $post_type_array[] = 'listing';
        $post_type_array[] = 'teacher';
        $post_type_array[] = 'studio';
        $post_type_array[] = 'school';
        $post_type_array[] = 'retreat';
        $post_type_array[] = 'restaurant';

        //echo "<pre>";print_r($posts); echo '</pre>';exit;
        // echo $post->post_type; exit;
        if (in_array($post->post_type,$post_type_array)){
            $post_ID = $post->ID;
            //echo"<pre>";print_r($actions);exit;
            //  count Ttoal comment
            //$color = "#2271b1";
            $results = $wpdb->get_results( $wpdb->prepare("SELECT count(listing_id) as total FROM {$wpdb->prefix}listing_comments where listing_id=".$post->ID));
            //echo "<pre>";print_r($results); echo '</pre>';
            if(  $results[0]->total  > 0 ){
                $color = '#228B22';
                $bold = '600';
                $required = '*';
            }else{
                $color ='#2271b1';
                $bold = '';
                $required = '';
            }

            $live_edit_color = '#228B22';

            $comment[ 'comment'] = sprintf('<a style=color:'.$color.';font-weight:'.$bold.' post_id="'.$post->ID.'" class="comment-popup thickbox" id="comment-popup" title="Comments" href="#TB_inline?width=1300&height=350&inlineId=modal-window-id" aria-label="%s">%s</a>',
                esc_attr__( $required.'Comment' ),
                __( $required.'Comment' )
                );
            // $post->ID

            $live_edit_section['live_edit'] = sprintf('<a style=color:'.$live_edit_color.';font-weight:bold post_id="'.$post_ID.'"  class="comment-popup-live-edit" id="comment-popup-live-edit" title="Update Listing Fields" href="#TB_inline?width=1300&height=250&inlineId=modal-33-live" aria-label="%s">%s</a>',
                esc_attr__( 'Live Edit' ),
                __( 'Live Edit' )
                );

            $offset = 3;
            $actions = array_slice($actions, 0, $offset, true) + $comment+ array_slice($actions, $offset, NULL, true);
            $edit_offset = 2;
            $actions = array_slice($actions, 0, $edit_offset, true) + $live_edit_section+ array_slice($actions, $edit_offset, NULL, true);
            $posts = get_post( $post );
            //echo "<pre>";print_r($posts);exit;
            add_thickbox();?>
            <div id="modal-window-id" style="display:none;">
                <input type="submit" name="filter_action" id="myBtn-submit" class="button" value="Add New Comment">
                <div id='loader'>
                    <img id="loading-image" src='<?php echo plugin_dir_url( __DIR__ ).'public\assets\images\reload.gif';?>' width='32px' height='32px'>
                </div>
                <table class="comments-listings" id ="comments-listings">
                    <thead>
                        <tr>
                            <th>Comments</th>
                            <th>Time-Stamp</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="listingtbody">

                    </tbody>
                </table>
            </div>
            <div id="myModal" class="modal">
                <div class="modal-content">
                    <div class="modal-header">
                        <span class="close_add_new">&times;</span>
                        <h2>Add Comments</h2>
                    </div>
                    <div class="modal-body">
                        <textarea id="wreview" name="wreview" rows="10" cols="90"></textarea>
                        <input type="hidden" value="<?php echo $posts->post_type;?>" name ="posttypecomment" id="posttypecomment">
                    </div>
                    <div class="modal-footer">
                        <button id="save" class="button" type="button" name="save-details">Save</button>
                    </div>
                </div>
            </div>

            <div id="myModallive" class="modal">
                <input type="hidden" id="live_post_id" value="" />
                <div class="modal-content">
                    <div class="modal-header">
                        <span class="close_add_new_live">&times;</span>
                        <h3><?php esc_html_e('Update Listing Fields:','listingpro'); ?></h3>
                    </div>
                    <div class="modal-body">
                        <div class="input-group">
                            <input type="text" class="form-control full-width" name="website7" id="update_website7" placeholder="<?php esc_html_e('Website:','listingpro'); ?>" />
                        </div>
                        <div class="input-group ">
                            <input type="text" class="form-control full-width" name="twitter7" id="update_twitter7" placeholder="<?php esc_html_e('Twitter:','listingpro'); ?>" />
                        </div>
                        <div class="input-group ">
                            <input type="text" class="form-control full-width" name="facebook7" id="update_facebook7" placeholder="<?php esc_html_e('Facebook:','listingpro'); ?>" />
                        </div>
                        <div class="input-group ">
                            <input type="text" class="form-control full-width" name="linkedin7" id="update_linkedin7" placeholder="<?php esc_html_e('Linkedin:','listingpro'); ?>" />
                        </div>
                        <div class="input-group ">
                            <input type="text" class="form-control full-width" name="instagram7" id="update_instagram7" placeholder="<?php esc_html_e('Instagram Profile Link:','listingpro'); ?>" />
                        </div>
                        <div class="input-group ">
                            <input type="text" class="form-control full-width" name="youtube7" id="update_youtube7" placeholder="<?php esc_html_e('Youtube Channel Link:','listingpro'); ?>" />
                        </div>
                        <div class="input-group ">
                            <input type="text" class="form-control full-width" name="pinterest7" id="update_pinterest7" placeholder="<?php esc_html_e('Pinterest Profile Link:','listingpro'); ?>" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="save_live" class="button" type="button" name="save-details"><?php esc_html_e('Save','listingpro'); ?></button>
                    </div>
                </div>
            </div>

            <?php

        }
        return $actions;
    }
   public function save_comments_listing_records(){
            //str_replace("//","/",$uri)
        //echo "<pre>";print_r(preg_replace('/\\\\/','', $_REQUEST['wreview']));exit;
        global $wpdb;
       if(isset($_REQUEST['wreview']) && isset($_REQUEST['posttypecomment']) && isset($_REQUEST['listingID'])){
           $listing_id   = $_REQUEST['listingID'];
           $commenttype  = preg_replace('@[/\\\]@','',$_REQUEST['wreview']);
           $posttype     = $_REQUEST['posttypecomment'];
           $table = $wpdb->prefix.'listing_comments';
           $data = array('post_type' => $posttype, 'comment' => $commenttype,'listing_id'=>$listing_id);
           //echo "<pre>";print_r($data);exit;
           //$format = array('%s','%s','%d');
           $wpdb->insert($table,$data);
           $response[ 'success' ] =  1;
           $response[ 'msg' ] =  'Data Insert Successfully!';
           echo json_encode( $response );exit;

       }
   }

    public function cstm_css_and_js() {
        global $pagenow, $typenow;
        if ($pagenow=='edit.php' && $typenow=='edited') {
            wp_enqueue_style ( 'importer_boostrap_modal_style', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css');
            wp_enqueue_script( 'import_boostrap_4_kvell_js','https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js');
        }
        wp_enqueue_style( 'wooccm-backend-css', plugins_url( 'public/assets/backend_css.css', APP_RELPATH ) );
        wp_enqueue_style( 'custom-backend-css', plugins_url( 'public/assets/css/custom_css.css', APP_RELPATH ) );
        wp_enqueue_script( 'script_wccs', plugins_url( 'public/assets/js/script_wccs.js', APP_RELPATH ), array( 'jquery' ), '1.2' );
        wp_enqueue_script( 'billing_script_wccs', plugins_url( 'public/assets/js/billing_script_wccs.js', APP_RELPATH ), array( 'jquery' ), '1.2' );
        wp_enqueue_script( 'shipping_script_wccs', plugins_url( 'public/assets/js/shipping_script_wccs.js', APP_RELPATH ), array( 'jquery' ), '1.2' );
        wp_enqueue_script( 'importer_custom_js', plugins_url( 'public/assets/js/custom-js.js', APP_RELPATH ), array( 'jquery' ), '1.3' );
        wp_enqueue_script( 'importer_sweet_alert_js','https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.0.19/sweetalert2.min.js');
        wp_enqueue_style ( 'importer_sweet_alert_style', 'https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.0.19/sweetalert2.min.css');
        wp_enqueue_style( 'icon-font-Material',"https://fonts.googleapis.com/icon?family=Material+Icons");
        wp_enqueue_style ( 'checking_website_select2_style', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css');
        wp_enqueue_script( 'checking_website_select2_js','https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js');
        wp_enqueue_script( 'proyoga_smoothness_js','https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js');
        wp_localize_script( 'importer_custom_js', 'importer_ajax_object', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
        wp_localize_script( 'importer_custom_js', 'remaining_ajax_object', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
        wp_localize_script( 'importer_custom_js', 'json_ajax_object', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
        wp_localize_script( 'importer_custom_js', 'export_all_ajax_object', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
    }

    public function exportall_handler(){
        global $wpdb;
        set_time_limit(0);
        $exportargs = array(
            'posts_per_page' => -1,
            'post_type' =>array('teacher','school','retreat','restaurant'),
            'post_status' => 'all'
        );
        //echo "<pre>"; print_r($exportargs);exit;
        //$allpostsexport = new WP_Query($exportargs);
        $allpostsexport = get_posts($exportargs);
        //echo "<pre>";print_r($allpostsexport);exit;
        if(!empty($allpostsexport)){
            $json_array = array();
            foreach( $allpostsexport as $allpostsexports ){
                //echo "<pre>";print_r($allpostsexports);exit;
                $post_id = $allpostsexports->ID;
                $listing_exportalljson  = get_post_meta( $post_id, 'listing_json', true );
                $json_exportobject[] = json_decode($listing_exportalljson);
                //echo "<pre>";print_r($json_exprotobject);exit;
            }
            $json_exportstring  =  json_encode( $json_exportobject,true );
            //echo "<pre>";print_r($json_exportstring);exit;
            $export_file_name="exportAll.json";
            $myexportfile = fopen(APP_PATH."logs/$export_file_name", "a") or die("Unable to open file!");
            fwrite($myexportfile, $json_exportstring);
            fclose($myexportfile);
            $json_file_export_url = APP_URL."logs/$export_file_name";
            $response[ 'success' ] =  1;
            $response[ 'file_data' ] =  $json_exportstring;
            $response[ 'file_url' ] =  $json_file_export_url;
            echo json_encode( $response ); exit;
        }
    }
    public function app_builder_create_website_checker(){
        require_once(APP_PATH.'/admin/json-checker-form.php');
    }
    public function app_builder_create_restricted_usernames(){
        require_once(APP_PATH.'/admin/restricted-username.php');
    }
    public function app_builder_create_page(){
        wp_enqueue_style( 'wooccm-backend-css', plugins_url( 'public/assets/backend_css.css', APP_RELPATH ) );
        wp_enqueue_script( 'script_wccs', plugins_url( 'public/assets/js/script_wccs.js', APP_RELPATH ), array( 'jquery' ), '1.2' );
        wp_enqueue_script( 'billing_script_wccs', plugins_url( 'public/assets/js/billing_script_wccs.js', APP_RELPATH ), array( 'jquery' ), '1.2' );
        wp_enqueue_script( 'shipping_script_wccs', plugins_url( 'public/assets/js/shipping_script_wccs.js', APP_RELPATH ), array( 'jquery' ), '1.2' );
        wp_enqueue_script( 'importer_custom_js', plugins_url( 'public/assets/js/custom-js.js', APP_RELPATH ), array( 'jquery' ), '1.3' );
        wp_enqueue_script( 'importer_custom_js', plugins_url( 'public/assets/js/my-js.js', APP_RELPATH ), array( 'jquery' ), '1.3' );
        wp_enqueue_script( 'importer_sweet_alert_js','https://cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.js');
        wp_enqueue_style ( 'importer_sweet_alert_style', 'https://cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.css');
        wp_localize_script( 'importer_custom_js', 'importer_ajax_object', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
        wp_localize_script( 'importer_custom_js', 'remaining_ajax_object', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
        wp_enqueue_style( 'wp-color-picker' );
        wp_enqueue_script( 'my-script-handle', plugins_url('public/assets/js/iris_init.js', __FILE__ ), array( 'wp-color-picker' ), false, true );
        require_once(APP_PATH.'/admin/json-importer-form.php');
    }

    public function smart_import_ajax_handler(){
        global $wpdb;
        ini_set("max_execution_time",3000000000);
        ini_set("max_allowed_memory",'4024M');
        ini_set("max_posts_vars",'4024M');
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);

        if($_FILES['file']['name'] !=''){
            // echo '<pre>'; print_r( $_FILES ); exit;

            if( $_FILES['file']['type'] /* == 'application/json'  */) {
                if (file_exists (ABSPATH.'/wp-admin/includes/taxonomy.php')) {
                    require_once (ABSPATH.'/wp-load.php');
                    require_once (ABSPATH.'/wp-admin/includes/taxonomy.php');
                }
                $str      = file_get_contents($_FILES['file']['tmp_name']);
                $strdata  = json_decode($str, true);
                $total_listings = count( $strdata );//12687
                //  Upload fiel here.
                $folder_path = APP_PATH.'uploads/';
                $filename = basename($_FILES['file']['name']);
                $newname = $folder_path . $filename;
                if (!move_uploaded_file($_FILES['file']['tmp_name'], $newname)) {
                    $response[ 'status' ]  = "fail";
                    echo json_encode( $response );exit;
                }
                // Create Login file
                $log_file_name = explode(".",$_FILES['file']['name']."");
                $log_file_txt_name = $log_file_name[0].".txt";
                // Save log name to WP Option
                update_option( 'current_log_file',$log_file_txt_name );
                $response[ 'total_records' ]  = $total_listings;
                $response[ 'data' ]  = $str;
                $response['yoya_file_name'] = $log_file_txt_name;
                update_option( 'temp_json_feed',$filename );
                $response[ 'status' ]  = 'completed';
                echo json_encode( $response );exit;
            }else{
                $error = "Please Insert Json format";
            }
        }
    }

    public function smart_remaining_ajax_handler(){
        global $wpdb;
        // Get File here
        $file_name = get_option( 'temp_json_feed' );
        $folder_path = APP_PATH.'uploads/';
        $file_parth = $folder_path.$file_name;
        $json_feed_string     = file_get_contents($file_parth );
        $json_array = json_decode( $json_feed_string, true , JSON_UNESCAPED_SLASHES);
        // echo"<pre>";print_r( $json_array ); exit;
        $current_index =  $_REQUEST[ 'index' ];
        $yoya_file_name =  get_option( 'current_log_file' );
        $total_deleted_items  = 0;
        for( $i=$current_index; $i<$current_index+30; $i++ ){
            //if( !isset($json_array[$i]) ) continue;
            //echo '<pre>'; print_r( $json_array[$i]);exit;
            if( isset( $json_array[$i]  )){
                $import_status=$this->save_json_listing($json_array[$i],$yoya_file_name);
                $total_deleted_items++;
            }
           // echo "<pre>";print_r($import_status);exit;
        }

        $current_index+= 30;

        if($import_status){
            $response[ 'status' ]  = "Feed Imported";
            $response[ 'current_index' ]  = $current_index;
            $response[ 'total_deleted_items' ]  = $total_deleted_items;
        }else{
            $response[ 'status' ]  = "Feed not Imported";
            $response[ 'total_deleted_items' ]  = $total_deleted_items;
        }

        echo json_encode( $response );exit;
    }
    public function save_json_listing($strshow,$yoya_file_name){
        global $wpdb;
        $table_name = $wpdb->prefix."rejection_listing";
        // echo '<pre>'; print_r( $strshow );exit;
        if(!empty($strshow) && isset($_REQUEST['selectcategory'])) {
            if(!empty($strshow['title'])) {
                $strshow['type'] = str_replace('yoga_instructors','yoga_instructor',$strshow['type']);
                $listing_address = $strshow['address'];
                $table_name = $wpdb->prefix."listing_location";
                $mylistingaddress = $wpdb->get_results("SELECT * FROM $table_name WHERE Formatted_Address ='".$listing_address."'");

                if(empty($mylistingaddress[0])){
                    $coordinates = $this->helper->get_geocode($strshow['address']);
                    $location_detail_json = $this->helper->get_city_name( $coordinates['lat'],$coordinates['lng'],$strshow['address'] );
                    $location_detail =  json_decode( $location_detail_json );
                    $listing_lat = $coordinates['lat'];
                    $listing_lng = $coordinates['lng'];
                    $listing_city_name = $location_detail->city;
                    $listing_country_name   = $location_detail->country;
                    $data = array('Latitude' => $listing_lat, 'Longitude' => $listing_lng , 'Formatted_Address'=>$listing_address,'City'=>$listing_city_name,'Country'=>$listing_country_name);
                    $table_name = $wpdb->prefix."listing_location";
                    $wpdb->insert($table_name,$data);
                }else{
                    $listing_lat = $mylistingaddress[0]->Latitude;
                    $listing_lng = $mylistingaddress[0]->Longitude;
                    $listing_address   = $mylistingaddress[0]->Formatted_Address;
                    $listing_city_name = $mylistingaddress[0]->City;
                    $listing_country_name = $mylistingaddress[0]->Country;
                    $coordinates[ 'formatted_address' ] = $mylistingaddress[0]->Formatted_Address;
                    $coordinates['lat']  = $mylistingaddress[0]->Latitude;
                    $coordinates['lng'] = $mylistingaddress[0]->Longitude;
                }

                $city_name = $listing_city_name;
                $country   = $listing_country_name;
                $tag_line_address = $city_name.', '.$country;

                if($strshow['address']){
                    $paddress = $strshow['address'];
                    if($strshow['city']){
                        $paddress .= ' '.@$strshow['city'];
                    }
                    if($strshow['country']){
                        $paddress .= ' '.str_replace('US','USA',@$strshow['country']);
                    }
                }else{
                    $paddress = $strshow['city'];
                    if(@$strshow['country']){
                        $paddress .= ', '.str_replace('US','USA',@$strshow['country']);
                    }
                }

                //  Set Listing Description
                $strshow['description'] = $strshow['title'].' is a '.ucwords(str_replace('_',' ',$strshow['type'])).' in '.$tag_line_address;
                $my_post = array();
                $my_post['post_type'] = $_REQUEST['selectcategory'];
                $title_name = explode(',',$strshow['address']);

                if( !empty($city_name)){
                    $my_post['post_title']   = $strshow['title'].' - '.$city_name;
                }else{
                    $my_post['post_title']   = $strshow['title'];
                }

                if( $this->helper->the_slug_exists($my_post['post_title'],$_REQUEST['selectcategory'] ) != 1) {
                    /*
                    $response['already_exist'] = 'Already exist' ;
                    $response['status'] = 1 ;
                    echo json_encode( $response );
                    exit;
                    */
                    //}else{
                    $myposts = $wpdb->get_results( $wpdb->prepare("select * from $wpdb->posts where post_title ='".$my_post['post_title']."' AND post_type='".$_REQUEST['selectcategory']."'"));
                    $listing_json = json_encode( $strshow,true );
                    $log_file_path         = fopen(APP_PATH."logs/$yoya_file_name", 'w') or die("Unable to open file!");
                    $current_log_file      = get_option( 'current_log_file' );
                    //print_r($listing_json);exit;
                    $is_website_exist = 0;

                    if(isset($strshow['website']) && filter_var($strshow['website'], FILTER_VALIDATE_URL) ){
                        $is_website_exist = 1;
                    }

                    //Listing Publish when vaid link website or social media link 30-july-2021 Start
                    if(empty($myposts) && ( $is_website_exist == 1 || (!empty($strshow['pinterest'])) || (!empty($strshow['instagram'])) || (!empty($strshow['youtube'])) || (!empty($strshow['facebook'])) || (!empty($strshow['twitter'])) || (!empty($strshow['linkedin'])))){
                        $my_post['post_status']  = 'publish';
                    }else{
                        $my_post['post_status']  = 'pending';
                    }

                    $listing_slug =  strtolower($this->helper->clean( sanitize_text_field( $my_post['post_title'] ) ) );
                    $is_listing_exists = get_page_by_path( $listing_slug ,OBJECT, $_REQUEST['selectcategory']);

                    if( !empty($is_listing_exists) ){   $error = "Record Already Exists!"; return false; }
                    $my_post['post_content'] =  $strshow['title'].' is a '.ucwords(str_replace('_',' ',$strshow['type'])).' in '.$tag_line_address;

                    $post_id   = wp_insert_post( $my_post );

                    if( $is_website_exist == 0 ){
                        $strshow['website'] = get_post_permalink( $post_id );
                    }

                    $cat_list  = $strshow['category'];
                    $cat       = explode(',',$cat_list);
                    $resultcat = array();
                    $parent = 0;
                    $cat_post_array = array();
                    $cat_post_array['teacher']      = 'teachers';
                    $cat_post_array['studio']       = 'studio-category';
                    $cat_post_array['school']       = 'schools';
                    $cat_post_array['retreat']      = 'retreats';
                    $cat_post_array['restaurant']   = 'restaurant-category';

                    $cat_list_array['teacher']      = 'Yoga Teacher';
                    $cat_list_array['studio']       = 'Yoga Studio';
                    $cat_list_array['school']       = 'Yoga School';
                    $cat_list_array['retreat']      = 'Yoga Retreat';
                    $cat_list_array['restaurant']   = 'Restaurant';

                    //  Check parent ID
                    $parentCategory = get_term_by('name',$cat_list_array[$_REQUEST['selectcategory']],$cat_post_array[$_REQUEST['selectcategory']]);

                    if( $parentCategory ){
                        $parent_cat_id  = $parentCategory->term_id;
                    }else{
                        //  Insert parent ID
                        $parent_cat_id  = wp_insert_category(array("cat_name"=>$cat_list_array[$_REQUEST['selectcategory']],"taxonomy"=>$cat_post_array[$_REQUEST['selectcategory']]),$wp_error);
                    }

                    $tags_list = $strshow['tags'];
                    $tagsitem  = array($strshow['type']);
                    $tagsList  = array();
                    $tagsList_array = array();
                    $tagsList_array['teacher'] = 'teacher-tags';
                    $tagsList_array['studio'] = 'studio-tags';
                    $tagsList_array['school'] = 'school-tags';
                    $tagsList_array['retreat'] = 'retreat-tags';
                    $tagsList_array['restaurant'] ='restaurant-tags';

                    if(!empty($tags_list) && isset($tagsList_array[$_REQUEST['selectcategory']])){
                        //$parent   = 0;
                        $tagsitem = explode(',',$tags_list);
                        foreach($tagsitem as $tags){
                            $catInfo = get_term_by('name',$tags,$tagsList_array[$_REQUEST['selectcategory']]);
                            if(!$catInfo){
                                $showcat  = wp_insert_category(array("cat_name"=>trim($tags),"taxonomy"=>$tagsList_array[$_REQUEST['selectcategory']],"category_parent"=>$parent),$wp_error);
                                $term_id  = $showcat->term_id;
                            }else{
                                $term_id  = $catInfo->term_id;
                            }
                            $tagsitem[] = $term_id;
                            $tagsList[] = trim($tags);
                        }
                        wp_set_object_terms($post_id,$tagsitem,$tagsList_array[$_REQUEST['selectcategory']]);
                    }

                    /*** Adding the location field here *****/
                    $locationList_array = array();
                    $locationList_array['teacher']      = 'teacher-location';
                    $locationList_array['studio']       = 'studio-location';
                    $locationList_array['school']       = 'school-location';
                    $locationList_array['retreat']      = 'retreat-location';
                    $locationList_array['restaurant']   = 'restaurant-location';
                    $locationInfo = get_term_by('name',trim($city_name),$locationList_array[$_REQUEST['selectcategory']]);

                    if(!$locationInfo){
                        $showcat      = wp_insert_category(array("cat_name"=>trim($city_name),"taxonomy"=>$locationList_array[$_REQUEST['selectcategory']]),$wp_error);
                        $locationInfo = get_term_by('name',trim($city_name),$locationList_array[$_REQUEST['selectcategory']]);
                        wp_set_object_terms($post_id,array($locationInfo->term_id),$locationList_array[$_REQUEST['selectcategory']]);
                    }else{
                        wp_set_object_terms($post_id,array($locationInfo->term_id),$locationList_array[$_REQUEST['selectcategory']]);
                    }

                    $listing_rate = array($strshow['price']);

                    if(!empty($listing_rate)){
                        //add_post_meta($post_id,'listing_rate',$listing_rate);
                    }
                    $listing_reviewed = array();
                    add_post_meta($post_id,'listing_reviewed',$listing_reviewed);
                    $plan_id = array(14);
                    if(!empty($plan_id)){
                        add_post_meta($post_id,'plan_id',$plan_id);
                    }
                    $claimed = array(0);
                    if(!empty($claimed)){
                        add_post_meta($post_id,'claimed',$claimed);
                    }

                    // 'price'=>$strshow['price']
                    $listing_plan_data = array('menu'=>'true','announcment'=>'true','deals'=>'true','competitor_campaigns'=>'true','events'=>'true','bookings'=>'true');
                    if(!empty($claimed)){
                        add_post_meta($post_id,'claimed',$claimed);
                    }

                    /** FAQ TABS **/
                    $faq = array('title'=>array(),'description'=>array());
                    $i=1;
                    if( $strshow['specifications']['tab_1']['tab_name'] ) {
                        $faq['title'][1]      = $strshow['specifications']['tab_1']['tab_name'];
                        $content = array();
                        foreach($strshow['specifications']['tab_1']['tab_content'] as $key=>$value){
                            $contentText = (is_array($value))? implode("\r\n",$value):$value;
                            $content[]   = $key.': '.$contentText;
                        }
                        $faq['description'][1] = implode("\r\n",$content);
                    }
                    if( $strshow['specifications']['tab_2']['tab_name'] ) {
                        $faq['title'][2]      = $strshow['specifications']['tab_2']['tab_name'];
                        $content = array();
                        foreach($strshow['specifications']['tab_2']['tab_content'] as $key=>$value){
                            $contentText = (is_array($value))? implode("\r\n",$value):$value;
                            $content[]   = $key.': '.$contentText;
                        }
                        $faq['description'][2] = implode("\r\n",$content);
                    }
                    if( $strshow['specifications']['tab_3']['tab_name'] ) {
                        $faq['title'][3]      = $strshow['specifications']['tab_3']['tab_name'];
                        $content = array();
                        foreach($strshow['specifications']['tab_3']['tab_content'] as $key=>$value){
                            $contentText = (is_array($value))? implode("\r\n",$value):$value;
                            $content[]   = $key.': '.$contentText;
                        }
                        $faq['description'][3] = implode("\r\n",$content);
                    }
                    if( $strshow['specifications']['tab_4']['tab_name'] ) {
                        $faq['title'][4]      = $strshow['specifications']['tab_4']['tab_name'];
                        $content = array();
                        foreach($strshow['specifications']['tab_4']['tab_content'] as $key=>$value){
                            $contentText = (is_array($value))? implode("\r\n",$value):$value;
                            $content[]   = $key.': '.$contentText;
                        }
                        $faq['description'][4] = implode("\r\n",$content);
                    }

                    if( $strshow['payment_methods'] ) {
                        add_post_meta($post_id,'payment_methods',$strshow['payment_methods']);
                    }
                    //echo "<pre>";print_r($strshow['phone']);exit;
                    $numbers = explode(',', $strshow['phone']);
                    if( count($numbers) >  1 ){
                        $update_phone_numbers = array();
                        if(!empty($numbers)){
                            foreach($numbers as $number)
                            {
                                $number_format = preg_replace('/[^0-9 _.,\-!()+= ]/', $number);
                                $update_phone_numbers[] =  $number_format;
                            }
                        }
                        $update_phone = implode( ',', $update_phone_numbers );
                    }else{
                        $update_phone = $strshow['phone'];
                    }
                   
                    $lp_listingpro_options = array(
                        //'tagline_text'=> $strshow['title'].' is a '.ucwords(str_replace('_',' ',$strshow['type'])).' in '.$paddress,
                        'tagline_text'=> $strshow['title'].' is a '.ucwords(str_replace('_',' ',$strshow['type'])).' in '.$tag_line_address,
                        //'gAddress'=>$paddress,
                        'gAddress'=>$coordinates[ 'formatted_address' ],
                        'latitude'=>$coordinates['lat'],
                        'longitude'=>$coordinates['lng'],
                        'mappin'=>'true',
                        'phone'=>$update_phone,
                        'linkedin'=>$strshow['linkedin'],
                        'twitter'=>$strshow['twitter'],
                        'facebook'=>$strshow['facebook'],
                        'youtube'=>$strshow['youtube'],
                        'instagram'=>$strshow['instagram'],
                        'pinterest'=>$strshow['pinterest'],
                        'whatsapp'=>'',
                        'email'=>$strshow['mail'],
                        'website'=>urldecode($strshow['website']),
                        'video'=>'',
                        'gallery'=>'',
                        //'price_status'=>'notsay',
                        //'list_price'=>@$priceRange[0],
                        //'list_price_to'=>@$priceRange[1],
                        'Plan_id'=>0,
                        'lp_purchase_days'=>'',
                        'reviews_ids'=>'',
                        'claimed_section'=>'',
                        'listings_ads_purchase_date'=>'',
                        'listings_ads_purchase_packages'=>'',
                        'faqs'=>array('faq'=>$faq['title'],'faqans'=>$faq['description']),
                        'business_hours'=>$strshow['work_hours'],
                        'campaign_id'=>'',
                        'changed_planid'=>'',
                        'listing_reported_by'=>'',
                        'listing_reported'=>'',
                        'business_logo'=>'',
                        'yoga_content_tab'=>$strshow['yoga_content_tab']
                        
                    );
                    //echo "<pre>";print_r($lp_listingpro_options);exit;
                    $lp_listingpro_options[ 'listing_title' ]  = $strshow['title'];
                    if( isset($strshow['linkedin']) && (!empty($strshow['linkedin']))){
                        //echo "1";
                        $lp_listingpro_options[ 'linkedin' ] = $strshow['linkedin'];
                    }else{
                        //echo "0";
                        $lp_listingpro_options[ 'linkedin' ] =   'https://www.linkedin.com';
                    }

                    if( isset($strshow['twitter']) && (!empty($strshow['twitter']))){
                        //echo "1";
                        $lp_listingpro_options[ 'twitter' ] = $strshow['twitter'];
                    }else{
                        //echo "0";
                        $lp_listingpro_options[ 'twitter' ] = 'https://www.twitter.com';
                    }

                    if( isset($strshow['facebook']) && (!empty($strshow['facebook']))){
                        //echo "1";
                        $lp_listingpro_options[ 'facebook' ] = $strshow['facebook'];
                    }else{
                        //echo "0";
                        $lp_listingpro_options[ 'facebook' ] = 'https://www.facebook.com/';
                    }

                    if( isset($strshow['youtube']) && (!empty($strshow['youtube']))){
                        //echo "1";
                        $lp_listingpro_options[ 'youtube' ] = $strshow['youtube'];
                    }else{
                        //echo "0";
                        $lp_listingpro_options[ 'youtube' ] = 'https://www.youtube.com/';
                    }

                    if( isset($strshow['instagram']) && (!empty($strshow['instagram']))){
                        //echo "1";
                        $lp_listingpro_options[ 'instagram' ] = $strshow['instagram'];
                    }else{
                        //echo "0";
                        $lp_listingpro_options[ 'instagram' ] = 'https://www.instagram.com/';
                    }
                    if( isset($strshow['pinterest']) && (!empty($strshow['pinterest']))){
                        //echo "1";
                        $lp_listingpro_options[ 'pinterest' ] = $strshow['pinterest'];
                    }else{
                        //echo "0";
                        $lp_listingpro_options[ 'pinterest' ] = 'https://www.pinterest.com/';
                    }

                    if(!empty($lp_listingpro_options)){
                        add_post_meta($post_id,'lp_listingpro_options',$lp_listingpro_options);
                    }

                    if( $strshow['other_images'] ) { }

                    //$gallery_image_ids = '1799,1798,1797,1796,1795';
                    $gallery_image_ids = '110688';
                    if(!empty($gallery_image_ids)){
                        add_post_meta($post_id,'gallery_image_ids',$gallery_image_ids);
                    }
                    $lp_listingpro_options_fields = array(array('lp_feature'=>array(28,32)));
                    if(!empty($lp_listingpro_options_fields)){
                        add_post_meta($post_id,'lp_listingpro_options_fields',$lp_listingpro_options_fields);
                    }

                    $campaign_status = array('active');

                    if(!empty($campaign_status)){
                        add_post_meta($post_id,'campaign_status',$campaign_status);
                    }

                    $lp_random_ads = array('active');
                    if(!empty($lp_random_ads)){
                        add_post_meta($post_id,'lp_random_ads',$lp_random_ads);
                    }

                    $lp_detail_page_ads = array('active');

                    if(!empty($lp_detail_page_ads)){
                        add_post_meta($post_id,'lp_detail_page_ads',$lp_detail_page_ads);
                    }

                    $lp_top_in_search_page_ads = array('active');
                    if(!empty($lp_top_in_search_page_ads)){
                        add_post_meta($post_id,'lp_top_in_search_page_ads',$lp_top_in_search_page_ads);
                    }

                    $response = wp_set_post_terms( $post_id, array( $parent_cat_id ),$cat_post_array[$_REQUEST['selectcategory']]);
                    add_post_meta ( $post_id , 'listing_json', json_encode( $strshow ) );
                    $msg = "Data imported successfully";
                }
            }

            return true;
        }else{
            return false;
            $error = "File Contains invalid Json format";
        }
        return false;
    }

    public function delete_all_listing_records(){
        $args=array('post_type'=>array('teacher','studio','school','retreat'),'post_status' => 'all','numberposts'=> 2000 );
        $allposts = get_posts($args);
        //echo '<pre>';echo "post"; print_r( $allposts ); exit;

        if( count( $allposts ) > 0 ){
            foreach($allposts as $eachpost){
                wp_delete_post( $eachpost->ID, true );
            }

            echo 'Post Deleted'; exit;
        }else{
        	echo 'Posts does not exist'; exit;
        }

    }

    public function delete_blank_listing_records(){
        global $wpdb;
        $args=array('post_type'=>array('teacher','studio','school','retreat','restaurant'),'numberposts'=>-1,'fields'=> 'ID');
        $allListing = get_posts($args);
        $table_name = $wpdb->prefix."rejection_listing";

        if(!empty($allListing)){
            foreach($allListing as $post){
                $post_id = $post->ID;
                $posttitle = $post->post_title;
                $myposts = $wpdb->get_results( $wpdb->prepare("select * from $wpdb->posts where post_title ='".$posttitle."'AND ID!='".$post_id."'"));
                if( !empty( $myposts )){
                    foreach( $myposts as $delete_post  ){
                        wp_delete_post( $delete_post->ID, true );
                    }
                }
                $website = $listing_json['website'];

                if(!$website){
                    wp_delete_post( $post_id, true );
                }
            }

            $responses['posts'] = get_post_meta( $post->ID, 'lp_listingpro_options', true );
            if(!empty($responses)){
                foreach($responses as $response){
                    $json_string = json_encode($response);
                    $folder_path = APP_PATH.'uploads/'.'file.json';
                    $file_path = $folder_path;
                }
            }
        }
    }
    // public function delete_all_taxonomy_records(){
    //     // Get taxonomy
    //     $taxonomy  = ( isset($_REQUEST[ 'taxonomy' ]))?$_REQUEST[ 'taxonomy' ]:0;
    //     $terms = get_terms([
    //         'taxonomy' => $taxonomy,
    //         'fields' => 'all',
    //         'hide_empty' => false,
    //     ]);
    //     //echo "<pre>";print_r($terms);exit;
    //     if(!empty($terms)){
    //         foreach ( $terms as $term ) {
    //             wp_delete_term($term->term_id, $taxonomy);
    //         }
    //         echo "Taxonomy Deleted";exit;
    //     }else{
    //         echo "Taxonomy does not exits";exit;
    //     }
    // }
}

new WPLI_Admin( 'JSON Feed Importer','1.0.0' );