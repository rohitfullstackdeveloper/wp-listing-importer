var total_records,total_data, total_remaining_records, current_index = 0;
jQuery.noConflict();
(function( $ ) {
    $( document ).ready(function(e) {        
        $('#upload_file_button').on('click',function(e){
            e.preventDefault();
            import_property();          
        });        
    });
     // self executing function here
    function import_property(){   
        var file_data = $('#file').prop('files')[0];   
        var selectcategory = $('#selectcategory').val();           
        var form_data = new FormData();                                    
        form_data.append('file', file_data);    
        swal.queue([{
            title: 'Importing Listing',
            confirmButtonText: 'Begin Import',
            html: '<table style="width:100%" id="import_table" border=1></table>'+'<div id="show_import_data"></div>'+'<div id="show_total_product"></div></div>',       
            width:'30%',
            allowEscapeKey: false,
            allowOutsideClick: false,
            showLoaderOnConfirm: true,                   
            preConfirm: () => {  
                return fetch(importer_ajax_object.ajaxurl+'?action=get_smartimportteacher&selectcategory='+selectcategory, {                    
                    method: 'POST', // *GET, POST, PUT, DELETE, etc.
                    body: form_data
                })
                .then(res => res.json())
                .then(data => import_listing(data))
                .catch(() => {
                    Swal.insertQueueStep({
                    icon: 'error',
                    title: 'Unable to get your Listing'
                    })
                })
            }
        }]);
        function import_listing(data){
            if(data.status=="completed"){
                Swal.insertQueueStep({
                    icon: 'success',
                    title: 'Listing Import is Completed',
                    showConfirmButton: true,
                    timer: 3000
                });
            }
            else{
                Swal.insertQueueStep({
                    icon: 'error',
                    title: 'Unable to get your Listing'
                })
            }                
        }
    };
})(jQuery);