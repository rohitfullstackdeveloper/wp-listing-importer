var total_records, total_data, total_remaining_records, current_index = 0,
    comment_post_id = 0,
    comment_text = '';
jQuery.noConflict();
(function($) {
    $(document).ready(function(e) {
        console.log('Checking1');
        $("input[name='phone']").keypress(function (e) {
            var regex = new RegExp(/[^0-9- _.,\-!()+=]/g, '');
            var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
            $("#phone-validation").remove("");
            if (regex.test(key)) {
                // event.preventDefault();
                $("input[name='phone']").after("<span id='phone-validation' class='text-danger' style='margin-top: 15px;display: inline-block;'>Please fill only numbers and symbols like +( )-</span>");
                return false;
            }
            /*if (!this.value.match(/^(\d|-)+$/)) {
                console.log('error');
                // $("input[name='phone']").after("<span id='phone-validation' class='text-danger' style='margin-top: 15px;display: inline-block;'>Please fill only numbers and symbols like +( )-</span>");
                this.value = this.value.replace(/[^0-9- _.,\-!()+=]/g, '');
            }
            
            var keyCode = e.keyCode || e.which;
 
            $("#phone-validation").remove("");
 
            //Regex for Valid Characters i.e. Numbers.
            // var regex = /[^0-9- _.,\-!()+=]/g, '';
            var regex = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?";
 
            //Validate TextBox value against the Regex.
            var isValid = regex.test(String.fromCharCode(keyCode));
            console.log(isValid);
            return false;
            if (isValid) {
                $("input[name='phone']").after("<span id='phone-validation' class='text-danger' style='margin-top: 15px;display: inline-block;'>Please fill only numbers and symbols like +( )-</span>");
            }
 
            return isValid;*/
        }); 
        $('#upload_file_button').on('click', function(e) {
            e.preventDefault();
            var file_data = $('#file').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            var selectcategory = $('#selectcategory').val();
            Swal.fire({
                title: 'Importing Listing',
                confirmButtonText: 'Begin Import',
                html: '<table style="width:100%" id="import_table" border=1></table>' + '<div id="show_import_data"></div>' + '<div id="show_total_product"></div></div>',
                width: '30%',
                allowEscapeKey: false,
                allowOutsideClick: false,
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    var data = {
                        action: 'get_smartimportteacher',
                        selectcategory: selectcategory,
                        index: current_index,
                    };

                    return fetch(importer_ajax_object.ajaxurl + '?action=get_smartimportteacher&selectcategory=' + selectcategory, {
                            method: 'POST', // *GET, POST, PUT, DELETE, etc.
                            body: form_data
                        }).then(res => res.json())
                        .then((out) => {
                            jQuery('.swal2-confirm').hide();
                            total_records = out.total_records;

                            total_data = out.data;
                            if (total_records > 1) {
                                total_remaining_records = total_records - current_index;
                                var import_statistics = ' <thead><tr><th>Total Records</th><th>Pending Records</th></tr></thead>';
                                import_statistics += "<tbody><tr><td id='total_records'>" + total_records + "</td><td id='pending_records'>" + total_remaining_records + "</td></tr></tbody>";
                                jQuery("#import_table").append(import_statistics);
                                jQuery('.swal2-loader').show();
                                jQuery('.swal2-confirm').hide();
                                return import_products(selectcategory, total_data);
                            } else {
                                Swal.fire(
                                    'No Record Found',
                                    'No Record exists for this JSON dump. Please try again!',
                                    'warning'
                                );
                            }
                        }).catch(err => console.error(err));
                }
            });
        });

        function import_products(selectcategory, total_data) {
            jQuery(document).find('.swal2-actions').html('<div class="swal2-loader" style="display:block;"></div>');
            jQuery.ajax({
                url: importer_ajax_object.ajaxurl + '?action=get_smartremainingrecords&selectcategory=' + selectcategory + '&index=' + current_index,
                success: function(result) {
                    //console.log(result);
                    var exist_status = JSON.parse(result);
                    //console.log(exist_status.status);return false;
                    if(exist_status.status==1){
                        Swal.fire(
                        'Listing Imported Successfully',
                            '',
                        'success'
                        ).then(() => {
                            location.reload();
                        });
                    }else{
                        var response_obj = JSON.parse(result);
                        var total_deleted_records = response_obj.total_deleted_items;
                        jQuery('.swal2-loader').show();
                        jQuery('.swal2-confirm').hide();
                        jQuery('#show_total_records').text('Remaining Pages = ' + total_records);
                        if (total_remaining_records > 0) {
                            var current_feed = total_data[current_index];
                            //console.log(current_feed);return false;
                            current_feed += total_deleted_records;
                            total_remaining_records -= total_deleted_records;
                            current_index += total_deleted_records;
                            $('#pending_records').text(total_remaining_records);
                            import_products(selectcategory, total_data);
                        } else {
                            Swal.fire(
                                'Listing Imported Successfully',
                                '',
                                'success'
                            ).then(() => {
                                location.reload();
                            })
                        }

                    }
                    

                }
            });
            return false;
        }

        // $('#export_all_button').on('click', function(e) {
        //     e.preventDefault();
        //     jQuery.ajax({
        //         url: export_all_ajax_object.ajaxurl,
        //         type: 'post',
        //         data: "action=get_exportall",
        //         success: function(result) {
        //             console.log(result);
        //             var results = JSON.parse(result);
        //             url = results.file_url;
        //             var blob = new Blob([results.file_data]);
        //             var linkExport = document.createElement('a');
        //             linkExport.href = window.URL.createObjectURL(blob);
        //             linkExport.download = "exportAll.json";
        //             document.body.append(linkExport);
        //             linkExport.click();
        //         }
        //     });
        // });
        $('#export_all_button').on('click', function(e) {
            e.preventDefault();
            //var selectcategory = $('#selectcategory').val();
            Swal.fire({
                title: 'Exporting Listing',
                confirmButtonText: 'Begin Export',
                html: '<table style="width:100%" id="import_table" border=1></table>' + '<div id="show_import_data"></div>' + '<div id="show_total_product"></div></div>',
                width: '30%',
                allowEscapeKey: false,
                allowOutsideClick: false,
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    var data = {
                        action: 'get_exportall'
                    };
                    return fetch(export_all_ajax_object.ajaxurl + '?action=get_exportall', {
                            method: 'POST', // *GET, POST, PUT, DELETE, etc.
                            body: data
                        }).then(res => res.json())
                        .then((result) => {
                            //console.log(result.success);
                            //console.log(result);
                            //var results = JSON.parse(result);
                            if (result != '') {
                                url = result.file_url;
                                var blob = new Blob([result.file_data]);
                                var linkExport = document.createElement('a');
                                linkExport.href = window.URL.createObjectURL(blob);
                                linkExport.download = "exportAll.json";
                                document.body.append(linkExport);
                                linkExport.click();
                                jQuery('.swal2-confirm').hide();
                            } else {
                                Swal.fire(
                                    'No Product Found',
                                    'No Product exists for this string. Please search again!',
                                    'warning'
                                );
                            }
                        }).catch(err => console.error(err));
                }
            });

        });

        $('#csv_export').on('click',function(e){
            e.preventDefault();
            var post_type = $('#post_type').val();
            var csv = [];
            var header_row =  ['Type','Title','Category','Description','Address','Phone','Email','Website','Work-Hours'];
            csv.push(header_row.join(","));
            Swal.fire({
                title: 'Do You Want to Export Pending List?',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:'Yes,Export',
                allowEscapeKey: false,
                allowOutsideClick: false,
                confirmButtonText: 'Ok',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    var data = {
                        action: 'get_exportcsvpending',
                        post_type :post_type
                    };
                    return fetch(export_all_ajax_object.ajaxurl + '?action=get_exportcsvpending&post_type='+post_type, {
                            method: 'POST', // *GET, POST, PUT, DELETE, etc.
                            body: data
                        }).then(res => res.json())
                        .then((response) => {
                            //console.log(response.file_data);
                            //return false;
                            //console.log(result);
                            //var results = JSON.parse(result);
                            //var data = JSON.parse(response);
                        var fileData = response.file_data; 
                        if(fileData.length > 0) {                            
                                var row = [];
                                for(var i = 0; i<fileData.length;i++ ){
                                    var row = [];
                                    row.push( fileData[i].Type);
                                    row.push( '"'+fileData[i].Title+'"');                        
                                    row.push( fileData[i].Category);                        
                                    row.push( '"'+fileData[i].Description+'"');
                                    row.push( '"'+fileData[i].Address+'"');
                                    row.push( fileData[i].Phone);
                                    row.push( fileData[i].Email);
                                    row.push( '"'+fileData[i].Website+'"');
                                    row.push( fileData[i].WorkHours);                        
                                    csv.push(row.join(","));
                                }   
                                download_csv(csv.join("\n"), response.file_name);
                                if (response.success){
                                    Swal.fire(
                                        'Pending Listing Exported',
                                        '',
                                        'success'
                                    ).then(function(){ 
                                        location.reload();
                                    });
                                }else {
                                    Swal.fire(
                                        'No Product Found',
                                        'No Product exists for this string. Please search again!',
                                        'warning'
                                    );
                                }
                        }    
                        }).catch(err => console.error(err));
                }
            });

        });

        var modal = $("#myModal");
        $(document).on('click', '#myBtn-submit', function() {
            modal.show();
            $('#wreview').val('');
        });

        var editpostmyModal = $("#editpostmyModal");
        $(document).on('click', '#postedit-myBtn-submit', function() {
            editpostmyModal.show();
        });

        $(document).on('click', '.close_add_new', function() {
            $('#myModal').hide();
            $('#TB_overlay').css('background','transparent');
        });

        /* Live Modal*/
        var myModallive = $("#myModallive");
        $(document).on('click', '#comment-popup-live-edit', function() {
            live_post_id = $(this).attr('post_id');
            $('#live_post_id').val(live_post_id);
            // $('#myModallive').show();

            setTimeout(function(){
                myModallive.show();
            }, 1200);
            jQuery.ajax({
                url: export_all_ajax_object.ajaxurl,
                type: 'post',
                data: "action=get_stagListingfields&post_id="+live_post_id,
                success: function(result) {
                    var results = JSON.parse(result);
                    if(results.success == 1){
                        $('#update_website7').val(results.data.website);
                        $('#update_twitter7').val(results.data.twitter);
                        $('#update_facebook7').val(results.data.facebook);
                        $('#update_linkedin7').val(results.data.linkedin);
                        $('#update_instagram7').val(results.data.instagram);
                        $('#update_youtube7').val(results.data.youtube);
                        $('#update_pinterest7').val(results.data.pinterest);
                    }
                }
            });
        });
        $(document).on('click', '.close_add_new_live', function() {
            $('#myModallive').hide();
            $('#TB_overlay').css('background','transparent');
        });
        /* Live Modal*/
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }

        $('#editpostsave').click(function(e) {
            //alert(comment_post_id);
            //$('#TB_window').css('visibility', 'visible');
            e.preventDefault();
            var wreview = $('#wreview').val();
            var posttypecomment = $('#posttypecomment').val();
            //var listingID = $('#listingID').val();
            var listingID = comment_post_id;
            jQuery.ajax({
                url: export_all_ajax_object.ajaxurl,
                type: 'post',
                data: "action=save_comments_listing&wreview=" + wreview + '&posttypecomment=' + posttypecomment + '&listingID=' + listingID,
                success: function(result) {
                    var results = JSON.parse(result);
                    results.success;
                    $('#postcomments').show();
                    $('#editpostmyModal').hide();
                    getPostEditContent(listingID);

                }
            });
        });

        $('#save').click(function(e) {
            //alert(comment_post_id);
            //$('#TB_window').css('visibility', 'visible');
            e.preventDefault();
            var wreview = $('#wreview').val();
            var posttypecomment = $('#posttypecomment').val();
            //var listingID = $('#listingID').val();
            var listingID = comment_post_id;
            jQuery.ajax({
                url: export_all_ajax_object.ajaxurl,
                type: 'post',
                data: "action=save_comments_listing&wreview=" + wreview + '&posttypecomment=' + posttypecomment + '&listingID=' + listingID,
                success: function(result) {
                    var results = JSON.parse(result);
                    results.success;
                    $('#TB_window').show();
                    $('#myModal').hide();
                    getContent(listingID);

                }
            });
        });

        $('#save_live').click(function(e) {
            e.preventDefault();
            var post_id = $('#live_post_id').val();
            var website7 = $('#update_website7').val();
            var twitter7 = $('#update_twitter7').val();
            var facebook7 = $('#update_facebook7').val();
            var linkedin7 = $('#update_linkedin7').val();
            var instagram7 = $('#update_instagram7').val();
            var youtube7 = $('#update_youtube7').val();
            var pinterest7 = $('#update_pinterest7').val();
            jQuery.ajax({
                url: export_all_ajax_object.ajaxurl,
                type: 'post',
                data: "action=get_updatingListingfields&website7=" + website7 + '&twitter7=' + twitter7 + '&facebook7=' + facebook7+ '&linkedin7='+ linkedin7 + '&instagram7=' + instagram7 + '&youtube7='+ youtube7 + '&pinterest7=' +pinterest7+ '&post_id='+post_id,
                success: function(result) {
                    var results = JSON.parse(result);
                    if(results.success == 1){
                        $('#myModallive').hide();
                    }
                }
            });
        });

        $('#myModal .close').click(function() {
            $('#TB_window').show();
        });
        $(document).on('click', "#myBtn-submit", function() {
            $('#TB_window').hide();
        });

        $('.comment-popup').click(function(e) {
            e.preventDefault();
            comment_post_id = $(this).attr('post_id');
            getContent(comment_post_id);
        })

        $('.comment-post-edit-popup').click(function(e) {
            e.preventDefault();
            comment_post_id = $(this).attr('post_id');
            getPostEditContent(comment_post_id);
        })

        function getPostEditContent(comment_post_id) {
            jQuery.ajax({
                url: export_all_ajax_object.ajaxurl,
                type: 'post',
                data: "action=getTheContent&query_var=" + comment_post_id,
                beforeSend: function() {
                    $("#loading-image").show();
                },
                success: function(res) {
                    var results = JSON.parse(res);
                    console.log(results);
                    //var table = $('#comments-listings');
                    $('#comments-post-edit-listings tbody').empty();
                    for (var i = 0; i < results.length; i++) {
                        $('#comments-post-edit-listings tbody').append("<tr id=listing_" + results[i].id + "><td>" + results[i].comment + "</td><td>" + results[i].timestamp + "</td><td><a post_id=" + results[i].id + " class='add' title='' data-toggle='tooltip' data-original-title='Add'><i class='material-icons'>&#xE03B;</i></a><a post_id=" + results[i].id + " class='edit' title='Edit' data-toggle='tooltip'><i class='material-icons'>&#xE254;</i></a><a post_id=" + results[i].id + " class='delete' title='Delete' data-toggle='tooltip'><i class='material-icons'>&#xE872;</i></a><a id='delete_action_" + results[i].id + "' post_id=" + results[i].id + " class='save-btn' title='save' data-toggle='tooltip'><button class='save-yes' post_id=" + results[i].id + " value='yes'>Yes</button><button  post_id=" + results[i].id + " class='save-no' value='no'>No</button></a></td></td></tr>");
                    }
                    $("#loading-image").hide();

                }
            });
        }

        function getContent(comment_post_id) {
            jQuery.ajax({
                url: export_all_ajax_object.ajaxurl,
                type: 'post',
                data: "action=getTheContent&query_var=" + comment_post_id,
                beforeSend: function() {
                    $("#loading-image").show();
                },
                success: function(res) {
                    var results = JSON.parse(res);
                    console.log(results);
                    //var table = $('#comments-listings');
                    $('#comments-listings tbody').empty();
                    for (var i = 0; i < results.length; i++) {
                        $('#comments-listings tbody').append("<tr id=listing_" + results[i].id + "><td>" + results[i].comment + "</td><td>" + results[i].timestamp + "</td><td><a post_id=" + results[i].id + " class='add' title='' data-toggle='tooltip' data-original-title='Add'><i class='material-icons'>&#xE03B;</i></a><a post_id=" + results[i].id + " class='edit' title='Edit' data-toggle='tooltip'><i class='material-icons'>&#xE254;</i></a><a post_id=" + results[i].id + " class='delete' title='Delete' data-toggle='tooltip'><i class='material-icons'>&#xE872;</i></a><a id='delete_action_" + results[i].id + "' post_id=" + results[i].id + " class='save-btn' title='save' data-toggle='tooltip'><button class='save-yes' post_id=" + results[i].id + " value='yes'>Yes</button><button  post_id=" + results[i].id + " class='save-no' value='no'>No</button></a></td></td></tr>");
                    }
                    $("#loading-image").hide();

                }
            });
        }

        $(document).on("click", ".edit", function() {
            // $(this).parents("tr").find("td:not(:last-child)").each(function(){
            //     $(this).html('<input type="text" class="form-control" value="' + $(this).text() + '">');
            // });
            var add = $(this).attr('post_id');
            var listingText = $('tr#listing_' + add + ' td:first-child').html();
            comment_text = listingText;
            $('tr#listing_' + add + ' td:first-child').html('<textarea id ="new_comment_' + add + '" class="form-control comment-input" rows="3" cols="90">' + listingText + '</textarea>');
            $('tr#listing_' + add + '').find(".add, .edit").toggle();
        });
        $(document).on("click", ".add", function(e) {
            e.preventDefault();
            var add = $(this).attr('post_id');
            // $( 'tr#listing_'+add+'').find('td:first-child').toggle();
            jQuery.ajax({
                url: export_all_ajax_object.ajaxurl,
                type: 'post',
                data: "action=update_comments_listing&addTable=" + add + '&addnewcomments=' + comment_text,
                success: function(response) {
                    console.log(response);
                    var result = JSON.parse(response);
                    console.log(response);
                    if (result.success == 1) {
                        $('tr#listing_' + add + ' td:first-child').empty();
                        $('tr#listing_' + add + ' td:first-child').text(result.comment);
                        $('tr#listing_' + add + ' td:eq(1)').empty();
                        $('tr#listing_' + add + ' td:eq(1)').text(result.timnestamp);
                        $('tr#listing_' + add + '').find(".add, .edit").toggle();
                    }
                }
            });
        });
        $(document).on("click", ".save-yes", function(e) {
            e.preventDefault();
            var saveDelete = $(this).attr('post_id');
            //console.log(saveDelete);return false;
            $(this).parents('tr#listing_' + saveDelete).remove();
            jQuery.ajax({
                url: export_all_ajax_object.ajaxurl,
                type: 'post',
                data: "action=delete_comments_listing&saveDelete=" + saveDelete,
                success: function(result) {
                    console.log(result);
                    var results = JSON.parse(result);
                    console.log(results.success);
                }
            });
        });
        jQuery(document).on('click', '.delete', function() {
            var post_id = $(this).attr('post_id');
            $(this).next('.save-btn').toggle();
            // $('#delete_action_'+post_id).toggle();
            //console.log( '#delete_action_'+post_id );
        });
        jQuery(document).on('click', '.save-no', function() {
            $(this).parents('.save-btn').toggle();
        });
        jQuery(document).on('keyup', '.comment-input', function() {
            comment_text = $(this).val();
        })

        $(".row-actions .view a").attr("target","_blank");

        $('.js-example-basic-multiple').select2();

        $('#checker_file_button').on('click', function(e) {
            e.preventDefault();
            console.log("checkingwebsite");  
            var selectcategorychecker = $('#selectcategorychecker').val();                        
            var csv = [];
            var header_row =  ['Type','Title','Website-Url','Listing-Slug','Facebook-Url','Twitter-Url','Linkedin-Url','Instagram-Url','Pinterest-Url','Reason'];
            csv.push(header_row.join(","));    
            Swal.fire({
                title: 'Listing Checking',
                text: 'Begin Checking',
                html: '<table style="width:100%" id="import_table" border=1></table>' + '<div id="show_import_data"></div>' + '<div id="show_total_product"></div></div>',
                width: '30%',
                showCancelButton: true,
                showConfirmButton: true,
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    var data = {
                        action: 'website_checking_listing'
                    };
                    return fetch(export_all_ajax_object.ajaxurl + '?action=website_checking_listing&selectcategorychecker=' + selectcategorychecker, {
                            method: 'POST', // *GET, POST, PUT, DELETE, etc.
                            body: data
                        }).then(res => res.json())
                        .then((response) => {
                            //console.log(result);
                            //var results = JSON.parse(result);
                            var fileDatawebchecker = response.file_data;
                            //console.log(fileDatawebchecker);
                            // if(!fileDatawebchecker){
                                if(fileDatawebchecker == undefined || fileDatawebchecker.length == 0){
                                    Swal.fire(
                                        'No Listing Found',
                                        'Selected listing(s) do not have the data. Please select more listings.',
                                        'warning'
                                    );
                                    return false;
                                }
                            // }
                            
                            if(fileDatawebchecker.length > 0) {  

                                var row = [];
                                Reason = '';
                                for(var i = 0; i<fileDatawebchecker.length;i++ ){
                                    var row = [];
                                    //console.log(fileDatawebchecker[i]);
                                    fileDatawebchecker[i].Reason1 = [];
                                    fileDatawebchecker[i].Reason2 = [];
                                    fileDatawebchecker[i].Reason3 = [];
                                    fileDatawebchecker[i].Reason4 = [];
                                    fileDatawebchecker[i].Reason5 = [];
                                    fileDatawebchecker[i].Reason6 = [];
                                    if(!fileDatawebchecker[i].Website){
                                        fileDatawebchecker[i].Website = '';
                                        fileDatawebchecker[i].Reason1 = 'Website Link Not Available,';
                                    }
                                    if(!fileDatawebchecker[i].Facebook){
                                        fileDatawebchecker[i].Facebook = '';
                                        fileDatawebchecker[i].Reason2 = 'Facebook Not Available,';
                                    }
                                    if(!fileDatawebchecker[i].Instagram){
                                        fileDatawebchecker[i].Instagram = '';
                                        fileDatawebchecker[i].Reason3 = 'Instagram Not Available,';
                                    }
                                    if(!fileDatawebchecker[i].Twitter){
                                        fileDatawebchecker[i].Twitter = '';
                                        fileDatawebchecker[i].Reason4 = 'Twitter Not Available,';
                                    }
                                    if(!fileDatawebchecker[i].Linkedin){
                                        fileDatawebchecker[i].Linkedin = '';
                                        fileDatawebchecker[i].Reason5 = 'Linkedin Not Available,';
                                    }
                                    if(!fileDatawebchecker[i].Pinterest){
                                        fileDatawebchecker[i].Pinterest = '';
                                        fileDatawebchecker[i].Reason6 = 'Pinterest Not Available';
                                    }
                                    row.push( fileDatawebchecker[i].Type);
                                    row.push( '"'+fileDatawebchecker[i].Title+'"' );
                                    row.push( '"'+fileDatawebchecker[i].Website+'"');
                                    row.push( '"'+fileDatawebchecker[i].Slug+'"');
                                    row.push( '"'+fileDatawebchecker[i].Facebook+'"');
                                    row.push( '"'+fileDatawebchecker[i].Instagram+'"');
                                    row.push( '"'+fileDatawebchecker[i].Twitter+'"');
                                    row.push( '"'+fileDatawebchecker[i].Linkedin+'"');
                                    row.push( '"'+fileDatawebchecker[i].Pinterest+'"');
                                    row.push( '"'+fileDatawebchecker[i].Reason1+fileDatawebchecker[i].Reason2+fileDatawebchecker[i].Reason3+fileDatawebchecker[i].Reason4+fileDatawebchecker[i].Reason5+fileDatawebchecker[i].Reason6+'"');
                                    csv.push(row.join(","));
                                }
                                download_csv(csv.join("\n"), "Website Checked Report.csv");
                                if (response.success){
                                    Swal.fire({
                                        position: 'center-center',
                                        icon: 'success',
                                        title: 'Export Complete',
                                        showConfirmButton: false,
                                        timer: 1750,
                                    })
                                }else {
                                    Swal.fire(
                                        'No Product Found',
                                        'No Product exists for this string. Please search again!',
                                        'warning'
                                    );
                                }
                            } 
                        }).catch(err => console.error(err));
                }
            });
               
            
        });

    });
    var appendthis = ("<div class='modal-overlay js-modal-close'></div>");
	jQuery("a[data-modal-id]").click(function(e) {
		e.preventDefault();
        console.log('hellow');
		jQuery("body").append(appendthis);
		jQuery(".modal-overlay").fadeTo(500, 0.7);
		//$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		jQuery("#" + modalBox).fadeIn($(this).data());
	});

	jQuery(".js-modal-close, .modal-overlay").click(function() {
		jQuery(".modal-box, .modal-overlay").fadeOut(500, function() {
			jQuery(".modal-overlay").remove();
		});
	});

	jQuery(window).resize(function() {
		jQuery(".modal-box").css({
			top: (jQuery(window).height() - jQuery(".modal-box").outerHeight()) / 2,
			left: (jQuery(window).width() - jQuery(".modal-box").outerWidth()) / 2
		});
	});

	jQuery(window).resize();

})(jQuery);

function download_csv(csv, filename){
    var csvFile;
    var downloadLink;
    csvFile = new Blob([csv], { type: "text/csv" });
    downloadLink = document.createElement("a");
    downloadLink.download = filename;
    downloadLink.href = window.URL.createObjectURL(csvFile);
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);
    downloadLink.click();
}

jQuery(document).ready(function( ){    
    jQuery("#nickname,#display_name").parent().parent().remove();
});

jQuery(document).ready(function($){
    var map;
    $('input[name=\'gAddress\']').autocomplete({
        'source': function(request, response) {
            var search = jQuery('#gAddress').val().trim();
            $.ajax({
                url: export_all_ajax_object.ajaxurl,
                type: 'post',
                dataType: "json",
                data: "action=test_osm&search=" + search,
                success: function(json) {
                    if(json.data == null){
                        $('#gAddress').after('<p class="errorMsg addressError">'+ json.message +'</p>'); 
                        setTimeout(function(){
                            $('.addressError').remove();
                        }, 1500);
                    }else{
                        response($.map(json.data, function(item) {
                            return {
                                Latitude: item['Latitude'],
                                Longitude: item['Longitude'],
                                value: item['Address']
                            }
                        }));
                    }  
                    
                }
            });
        },
        'select': function(event, ui) {
            jQuery('#latitude').val(ui.item.Latitude);
            jQuery('#longitude').val(ui.item.Longitude);
            $('#gAddress').after('<div id="result"><input type="hidden" name="Latitude" value="' + ui.item.Latitude + '" /> <input type="hidden" name="Longitude" value="' + ui.item.Longitude + '" /></div>');
        }
    });

    $('.ui-autocomplete').on('click', '.ui-menu-item', function(){
        map_intilized();
    });

    function map_intilized(){
        if (map !== undefined && map !== '') { 
            map.off();
            map.remove();
            //console.log('test');
        }
        var Latitude = jQuery('#latitude').val();
        var Longitude = jQuery('#longitude').val();
        var title = jQuery('#gAddress').val();
        var lat = Latitude;
        var long = Longitude;
        var mapIcon = 'https://pro.yoga/wp-content/themes/proyoga/assets/images/pins/pin.png'; 
        var iconAddress = title;
        var options = {
            center: [lat, long],
            zoom: 13
        }
        map = L.map('map', options);
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', { attribution: 'OSM' })
            .addTo(map);            
        var greenIcon = L.icon({
            iconUrl: mapIcon
        });
        var myMarker = L.marker([lat, long], { icon: greenIcon,title:iconAddress, alt: iconAddress, draggable: false })
        .addTo(map)
        .on('dragend', function() {
            var coord = String(myMarker.getLatLng()).split(',');
            console.log(coord);
            var lat = coord[0].split('(');
            console.log(lat);
            var lng = coord[1].split(')');
            console.log(lng);
            myMarker.bindPopup("Moved to: " + lat[1] + ", " + lng[0] + ".");
        });
    }
        map_intilized();
});

jQuery(document).ready(function($){
    $('#restricted_file_button').on('click', function(e) {
        e.preventDefault();
        var restrictedusernamecheck = $('#restricted-username-check').val(); 
        var replaced = restrictedusernamecheck.replace(/\n/g,',\n'); 
        
        if(restrictedusernamecheck == ""){            
            // console.log('textarea: ', restrictedusernamecheck);
            swal.fire({
                title:  "Missing Field",
                text:   "Please Check Fields!",
                icon:   "warning",
                confirmButtonText: 'Ok',
    
            });
        }else{
            Swal.fire({
                title: 'Please Wait',
                text: 'Saving Data',
                html: '<table style="width:100%" id="import_table" border=1></table>' + '<div id="show_import_data"></div>' + '<div id="show_total_product"></div></div>',
                width: '30%',
                showCancelButton: true,
                showConfirmButton: true,
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    var data = {
                        action: 'restricted_names_listing'
                    };
                    return fetch(export_all_ajax_object.ajaxurl + '?action=restricted_names_listing&restrictedusernamecheck=' + replaced, {
                            method: 'POST', // *GET, POST, PUT, DELETE, etc.
                            body: data
                        }).then(res => res.json())
                        .then((response) => {
                            if (response.success){
                                Swal.fire(
                                    'Data Saved Successfully',
                                    '',
                                    'success'
                                ).then(function(){ 
                                    location.reload();
                                });
                            }else {
                                Swal.fire(
                                    'No Data Found',
                                    'No Data exists for this string. Please search again!',
                                    'warning'
                                );
                            }
                        }).catch(err => console.error(err));
                }
            });
        }
        
    });    
});



