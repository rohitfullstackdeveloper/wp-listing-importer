<?php
/**
 * The frontend-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the frontend-specific stylesheet and JavaScript.
 *
 * @package    WPLI
 * @subpackage WPLI/admin
 * @author     Rohit Sharma
 */
class WPLI_Frontend {
    private $plugin_name,$version;

    public function __construct( $WPLI, $version ) {
        $this->plugin_name = $WPLI;
        $this->version = $version;
    }
    public function wpli_custom_template($single) {
        global $post;

        if ( $post->post_type == 'teacher' ) {
            if ( file_exists( plugin_dir_path( __DIR__ ) . 'includes/CPT/templates/custom-listing.php' ) ) {
                return APP_PATH . 'includes/CPT/templates/custom-listing.php';
            }
        }

        if ( $post->post_type == 'studio' ) {
            if ( file_exists(APP_PATH . 'includes/CPT/templates/custom-studio.php' ) ) {
                return APP_PATH. '/includes/CPT/templates/custom-studio.php';
            }
        }
        if ( $post->post_type == 'school' ) {
            if ( file_exists( APP_PATH . '/includes/CPT/templates/custom-school.php' ) ) {
                return APP_PATH . '/includes/CPT/templates/custom-school.php';
            }
        }
        if ( $post->post_type == 'retreat' ) {
            if ( file_exists(APP_PATH . '/includes/CPT/templates/custom-retreat.php' ) ) {
                return APP_PATH . '/includes/CPT/templates/custom-retreat.php';
            }
        }
        if ( $post->post_type == 'restaurant' ) {
            if ( file_exists(APP_PATH . '/includes/CPT/templates/custom-restaurant.php' ) ) {
                return APP_PATH . '/includes/CPT/templates/custom-restaurant.php';
            }
        }
        return $single;
    }
}

new WPLI_Frontend( 'JSON Feed Importer','1.0.0' );