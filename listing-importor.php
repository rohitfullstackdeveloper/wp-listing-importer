<?php
/**
 * @link              #
 * @since             1.0.0
 * @package           WPLI
 *
 * @wordpress-plugin
 * Plugin Name:       POST IMPORTER
 * Plugin URI:        #
 * Description:       This plugin is used to POST IMPORTER for different custom post types along with their data
 * Version:           1.1.0
 * Author:            Rohit Sharma
 * Author URI:        http://rohitfullstackdeveloper.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       WPLI
 * Domain Path:       /languages
 */

if ( !defined( 'ABSPATH' ) ) exit;
define( 'APP_DIRNAME', basename( dirname( __FILE__ ) ) );
define( 'APP_RELPATH', basename( dirname( __FILE__ ) ) . '/' . basename( __FILE__ ) );
define( 'APP_PATH', plugin_dir_path( __FILE__ ) );
define( 'APP_URL', plugin_dir_url( __FILE__ ) );
//define( 'KVELL_TRESTLE_PUBLIC',APP_PATH.'public/');
define( 'APP_PREFIX', 'app' );

//echo APP_PATH;exit;

/**
 *	@descripiton 	This function wil import the listing table on activation
 *	@param 			NONE
 *	@return 		NONE
 */

function activate_WPLI() {
	global $wpdb;
	$table_name = $wpdb->prefix . "listing_comments";
	$table_name_listing = $wpdb->prefix . "listing_location";
	$charset_collate = $wpdb->get_charset_collate();

	if ( $wpdb->get_var("SHOW TABLES LIKE '{$table_name}'") != $table_name ) {
    	$sql = "CREATE TABLE $table_name (
        			`id` int(10) NOT NULL AUTO_INCREMENT,
			        `post_type` varchar(255) NOT NULL,
			        `listing_id` int(10) NOT NULL,
			        `comment` varchar(255) NOT NULL,
					`timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			        PRIMARY KEY  (id)
			) $charset_collate;";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($sql);
	}
	if ( $wpdb->get_var("SHOW TABLES LIKE '{$table_name_listing}'") != $table_name_listing ) {
    	$sqlListing = "CREATE TABLE $table_name_listing (
        			`id` int(10) NOT NULL AUTO_INCREMENT,
			        `Latitude` TEXT NOT NULL,
			        `Longitude` TEXT NOT NULL,
			        `Formatted_Address` varchar(255) NOT NULL,
					`City` varchar(255) NOT NULL,
					`Country` varchar(255) NOT NULL,
					`timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			        PRIMARY KEY  (id)
			) $charset_collate;";
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($sqlListing);
	}
}

//	Register Active Hook
register_activation_hook(__file__, 'activate_WPLI');

//	Include Bridge File
require plugin_dir_path( __FILE__ ) . 'includes/class-listing-importer.php';

/**
 *	@description  This function will load the bridge file for the plugin
 *	@param 			NONE
 *	@return 		NONE
 */

function run_wp_listing_importer() {
	$plugin = new listing_importer();
}

// Call Importer Function
run_wp_listing_importer();