<?php
/**
 * @since      1.0.0
 * @package    WPLI
 * @subpackage WPLI/includes
 * @author     Rohit Sharma
 */

class listing_importer_helper {
	protected $loader,$plugin_name,$version;

	public function __construct() {
		$this->plugin_name 	= 'WPLI';
		$this->version 		= '1.0.0';
	}

	public function clean($string) {
		$string = str_replace(' ', '-' ,$string); // Replaces all spaces with hyphens.
		//print_r($string);exit;
		return htmlentities(preg_replace('/[^A-Za-z0-9\-]/', ' ' ,$string)); // Removes special chars.
	}

	public function the_slug_exists($post_title,$post_type) {
		global $wpdb;
		$record = $wpdb->get_row("SELECT ID FROM ".$wpdb->prefix."posts WHERE post_type='".$post_type."' AND post_title = '" . $post_title . "'");
		//echo"<pre>";print_r($record);exit;
		if(isset($record->ID)) {
			// echo 'true'; exit;
			return true;
		}else{
			// echo 'false'; exit;
			return false;
		}
		// if(!empty($record)) {
		// 	return true;
		// } else {
		// 	return false;
		// }
	}

	public function get_geocode($address){
		global $listingpro_options;
		$mapAPI = $listingpro_options['google_map_api'];
		// $url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&key=AIzaSyBMQKkIzg5Xty9iK4Uq0Z75785o2PXAdX4';
		$url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&key='.$mapAPI;
		//print_r($url);exit;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		$output = curl_exec($ch);
		curl_close($ch);
		$output = json_decode($output);

		if($output->status=='OK'){
			$location = $output->results[0]->geometry->location;
			$lat = $location->lat != '' ? $location->lat : '25.7612';
			$lng = $location->lng != '' ? $location->lng : '-80.1911';
			$formatted_address = $output->results[0]->formatted_address != '' ? $output->results[0]->formatted_address : '2037 Chestnut Philadelphia';
			return ['lat'=>$lat,'lng'=>$lng,'formatted_address' => $formatted_address ];
		}else{
			$formatted_address = '2037 Chestnut Philadelphia';
			return ['lat'=>'25.7612','lng'=>'-80.1911','post_zip'=>55101,'city'=>'stpaul','state'=>'MN','country'=>'US','formatted_address' => $address];
		}
	}

	public function set_featured_image($post_id,$image){
		if(!empty($image)){
			$imagePath = explode("?",$image);
			$image_url = @$imagePath[0];

			$reg 	= '/(?<=jpg|png|gif|jpeg).*/';
			$rep	= '';
			$result = preg_replace($reg, $rep, $image_url);

			$image_name = basename($image_url);
			$upload_dir = wp_upload_dir();

			$unique_file_name = wp_unique_filename($upload_dir['path'], $image_name);
			$filename = basename($unique_file_name);

			//$image_url = trim($image_url);
			if( wp_mkdir_p( $upload_dir['path'] ) ) {
				$file = $upload_dir['path'] . '/' . $filename;
			} else {
				$file = $upload_dir['basedir'] . '/' . $filename;
			}

			@copy($image_url,$file);

			if( wp_mkdir_p( $upload_dir['path'] ) ) {
				$file = $upload_dir['path'] . '/' . $filename;
			} else {
				$file = $upload_dir['basedir'] . '/' . $filename;
			}

			$wp_filetype = wp_check_filetype( $filename, null );
			$attachment = array(
				'post_mime_type' => $wp_filetype['type'],
				'post_title'     => sanitize_file_name( $filename ),
				'post_content'   => '',
				'post_status'    => 'inherit'
			);

			$attach_id = wp_insert_attachment( $attachment, $file, $post_id );
			require_once(ABSPATH . 'wp-admin/includes/image.php');
			$attach_data = wp_generate_attachment_metadata( $attach_id, $file );

			//echo"<pre>"; print_r($attach_data); die();
			wp_update_attachment_metadata( $attach_id, $attach_data );
			set_post_thumbnail( $post_id, $attach_id );
			return end(explode("uploads",$file));
		}
	}

	/**
	 *	@description  This function will get the city name from latitude and longitude
	 *	@param     latitude   This will be the latitude of the address
	 *			   longitude  This will be the longitude of the address
	 *
	 *	@return    string 	  City name of the address
	 *  @author    Rohit Sharma
	 */

	public function get_city_name( $lat,$lng,$address ){
		global $listingpro_options;
		$mapAPI = $listingpro_options['google_map_api'];
		$url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$lng.'&key='.$mapAPI;
		//print_r($url);
		$json = @file_get_contents($url);
		$data = json_decode($json);
		// echo '<pre>';  print_r( $data ); exit;

		$status = $data->status;
		$city  = '';
		$country = '';

		if( $status=="OK" ) {
			//Get address from json data
			$locality_city = '';
			$sublocality_city = '';
			$adminstrative_city = '';

			for ($j=0;$j<count($data->results[0]->address_components);$j++) {
				$cn=array($data->results[0]->address_components[$j]->types );

				if(in_array("locality", $cn[0])) {
					$locality_city = $data->results[0]->address_components[$j]->long_name;
				}else if(in_array("sublocality", $cn[0])) {
					$sublocality_city = $data->results[0]->address_components[$j]->long_name;
				}else if(in_array("administrative_area_level_1", $cn[0])) {
					$adminstrative_city = $data->results[0]->address_components[$j]->long_name;
				}else if(in_array("country", $cn[0])) {
					$country = $data->results[0]->address_components[$j]->long_name;
				}
			}

			if( !empty( $locality_city )){
				$city = $locality_city;
			}else if( !empty( $sublocality_city )){
				$city = $sublocality_city;
			}else{
				$city = $adminstrative_city;
			}
		}else{
			$addressArray = explode( ',',$address);
			$city = (isset($addressArray[1]))?$addressArray[1]:'';
		}

		$response =  array(
			'city' => $city,
			'country' => $country
		);

		return json_encode( $response );
	}
}