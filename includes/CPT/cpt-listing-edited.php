<?php

add_action('init', 'post_type_edited');

function post_type_edited() {  
   
    $labels = array(
        'name' => _x('Listing Editor', 'post type general name', 'listingpro-plugin'),
        'singular_name' => _x('Listing Editor', 'post type singular name', 'listingpro-plugin'),
        'add_new' => _x('Add New', 'book', 'listingpro-plugin'),
        'add_new_item' => __('Add New List', 'listingpro-plugin'),
        'edit_item' => __('Edit List', 'listingpro-plugin'),
        'new_item' => __('New Listing', 'listingpro-plugin'),
        'view_item' => __('View List', 'listingpro-plugin'),
        'search_items' => __('Search Listing', 'listingpro-plugin'),
        'not_found' =>  __('No List found', 'listingpro-plugin'),
        'not_found_in_trash' => __('No List found in Trash', 'listingpro-plugin'), 
        'parent_item_colon' => ''
    );      
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'query_var' => true,
        'rewrite'   => array( 'slug' => 'edited' ),
        'capability_type' => 'post',
        'has_archive' => false,
        'menu_position' => 21,
        'hierarchical' => false,
        'show_in_rest'       => true,
        'supports' => array( 'title' ),
        'menu_icon' => plugin_dir_url( __DIR__ ). '../public/assets/images/editing-listing.png'
    );      

    register_post_type( 'edited', $args );                   
} 

function save_edited_meta(){
    
}

add_action('save_post', 'save_edited_meta');


add_filter( 'manage_edited_posts_columns', 'add_edited_custom_columns') ;
function add_edited_custom_columns( $columns ) {    
    
	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => esc_html__( 'Title' ),
		'author11' => esc_html__( 'Submitted by' ),
		'view' => esc_html__( 'View' ),
		'status' => esc_html__( 'Status' ),
        'type' => esc_html__( 'Type' ),
		'date' => esc_html__( 'Date' )
	);

	return $columns;
}


    global $wpdb,$post;
    define('THEMENAME', 'listingpro');
    $meta = 'lp_'.strtolower(THEMENAME).'_options';
    //echo"SELECT * FROM $wpdb->postmeta WHERE (meta_key = '".$meta."') ORDER BY post_id DESC LIMIT 1";exit;
    $query  = "SELECT * FROM $wpdb->postmeta WHERE (meta_key = '".$meta."') ORDER BY post_id DESC LIMIT 1";
    $get_result = $wpdb->get_results($query);
    $post_id = $get_result[0]->post_id;
    $listing_options =  get_post_meta($post_id, $meta, true );
    //echo "<pre>";print_r($listing_options);exit;
function fill_edited_posts_custom_column($column_name,$post_id ) {  
    //print_r($column_name);exit;
    global $post;
    //echo "<pre>";print_r($post);exit;
    $user = wp_get_current_user(); 
    //echo "<pre>";print_r($user);exit; 
    $useredited = $user->user_login;
    //$author_id = $post->post_author;    
    switch($column_name) {
        case 'title' :			
            echo $listing_options['listing_title'];
            break;

        case 'author11':
			echo '<a href="'.get_edit_profile_url( $user->ID ).'">' .esc_attr($useredited). '</a>';
			break;
        case 'view': 
            // $view_link = get_permalink( $category_link  );   
            // $metabox = get_post_meta($post_id, 'lp_' . strtolower(THEMENAME) . '_options_contactownerform', true);
            // $view_link =$metabox['guid'] ;
            // $preview_target = " target='wp-preview-{$post->ID}'";
            // $display_link = urldecode( $view_link );
            $post_id = $post->ID;
            $listing_options_edited = get_post_meta($post_id, 'lp_' . strtolower(THEMENAME) . '_options_contactownerform', true); 
            //echo"<pre>";print_r($listing_options_edited);exit;
           
            
           ?>
                <?php if($listing_options_edited['report_id'] == 1){
                ?>
                    <a class="edited-contactOwner" data-toggle="modal" data-target="#exampleModal_report_<?php echo $post_id ?>">
                        <?php esc_html_e('Edit profile request','listingpro');?>
                    </a>
                <?php 
                } else {
                ?>
                    <a class="edited-contactOwner" data-toggle="modal" data-target="#exampleModal_<?php echo $post_id ?>">
                        <?php esc_html_e('Edited Your Listing','listingpro');?>
                    </a>
                <?php 
                } 
                ?>
                <div class="modal fade" id="exampleModal_<?php echo $post_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel"><?php esc_html_e('Update Listing Field','Listingpro')?></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="contact-form quickform">
                                    <form class="form-horizontal hidding-form-feilds margin-top-20" method="post" id="contactOwnerFormEdited_<?php echo $post_id ?>"> 
                                        <input type="hidden" id="lpNonce" name="lpNonce" value="" /> 
                                        <input type="hidden" id="post_title<?php echo $post_id ?>" name="post_title" value="<?php echo $listing_options_edited['post_title'];?>" />
                                        <input type="hidden" id="post_link<?php echo $post_id ?>" name="post_link" value="<?php echo $listing_options_edited['guid']?>" />
                                        <input type="hidden" id="post_status" name="post_status" value="2" />
                                        <?php
                                        
                                        ?>
                                        <input type="hidden" id="POST_ID" name="POST_ID" value="<?php echo $listing_options_edited['post_id']; ?>"/>
                                        <input type="hidden" id="editProfileRequestType<?php echo $post_id ?>" name="editProfileRequestType" value="<?php echo $listing_options_edited['profile_edit_reported']?>"/> 

                                        <div class="form-group d-flex">
                                            <input type="text" class="form-control" name="website7" id="website7<?php echo $post_id ?>" value="<?php echo $listing_options_edited['website']?>" placeholder="<?php esc_html_e('Website:','listingpro'); ?>">
                                            <span id="website7"></span>
                                        </div>
                                        <div class="form-group form-group-icon d-flex">
                                            <i class="fa fa-twitter" aria-hidden="true"></i>
                                            <input type="text" class="form-control" name="twitter7" id="twitter7<?php echo $post_id ?>" value="<?php echo $listing_options_edited['twitter']?>" placeholder="<?php esc_html_e('Twitter:','listingpro'); ?>">                                                        
                                        </div>
                                        <div class="form-group form-group-icon d-flex">
                                            <i class="fa fa-facebook" aria-hidden="true"></i>
                                            <input type="text" class="form-control" name="facebook7" id="facebook7<?php echo $post_id ?>" value="<?php echo $listing_options_edited['facebook']?>" placeholder="<?php esc_html_e('Facebook:','listingpro'); ?>">                                                        
                                        </div>
                                        <div class="form-group form-group-icon d-flex">
                                            <i class="fa fa-linkedin" aria-hidden="true"></i>
                                            <input type="text" class="form-control" name="linkedin7" id="linkedin7<?php echo $post_id ?>" value="<?php echo $listing_options_edited['linkedin']?>" placeholder="<?php esc_html_e('Linkedin:','listingpro'); ?>">                                                        
                                        </div>
                                        <div class="form-group form-group-icon d-flex">
                                            <i class="fa fa-instagram" aria-hidden="true"></i>
                                            <input type="text" class="form-control" name="instagram7" id="instagram7<?php echo $post_id ?>" value="<?php echo $listing_options_edited['instagram']?>" placeholder="<?php esc_html_e('Instagram Profile Link:','listingpro'); ?>">                                                        
                                        </div>
                                        <div class="form-group form-group-icon d-flex">
                                            <i class="fa fa-youtube" aria-hidden="true"></i>
                                            <input type="text" class="form-control" name="youtube7" id="youtube7<?php echo $post_id ?>" value="<?php echo $listing_options_edited['youtube']?>" placeholder="<?php esc_html_e('Youtube Channel Link:','listingpro'); ?>">                                                        
                                        </div>
                                        <div class="form-group form-group-icon d-flex">
                                            <i class="fa fa-pinterest" aria-hidden="true"></i>
                                            <input type="text" class="form-control" name="pinterest7" id="pinterest7<?php echo $post_id ?>" value="<?php echo $listing_options_edited['pinterest']?>" placeholder="<?php esc_html_e('Pinterest Profile Link:','listingpro'); ?>">                                                        
                                        </div>                                                    
                                        <div class="form-group">
                                            <textarea class="form-control" rows="5" name="descriptions7" id="descriptions7<?php echo $post_id ?>" placeholder="<?php esc_html_e('Descriptions:','listingpro'); ?>"><?php echo $listing_options_edited['descriptions']?></textarea>
                                        </div>
                                        <div class="form-group form-group-icon">                                                        
                                            <input type="button" class="button form-control-file" name="gallery" id="gallery_images_upload<?php echo $post_id ?>" value="<?php esc_html_e('Edit Gallery:','listingpro'); ?>">  
                                            <input type="hidden" name="gallery_image_ids" id="gallery_image_ids<?php echo $post_id ?>" value="<?php echo $listing_options_edited['gallery_image_ids'];?>"> 
                                            <?php 
                                            //echo $post_id;
                                            global $wpdb;
                                            $table_name = $wpdb->prefix.'postmeta';
                                            $table_name_posts = $wpdb->prefix.'posts';
                                            $metaKey = 'lp_'.strtolower(THEMENAME).'_options_contactownerform'; 
                                            $views = get_post_meta( $post_id, $metaKey, true );
                                            if(empty($views['filename'])){
                                                $filename = explode(',',$views['gallery']);
                                            }else{
                                                $filename = explode(',',$views['filename']);
                                            }
                                            if(!empty($filename[0]) && isset($filename)){
                                                echo '<ul class="gallery-thumbs filename">';
                                                foreach($filename as $filenames){
                                                    echo '<li class="gallery_approval_thumb'.$post_id.'">';
                                                        echo '<img width="32" height="32" src="'.APP_URL.'uploads/gallery/'.$filenames.'" alt="" filenames="'.$filenames.'">';
                                                    echo '</li>';
                                                }
                                                echo '</ul>';   
                                            }else { ?>
                                                <div class="col-lg-9 pr-0 pl-0">
                                                    <?php
                                                    //  echo "<pre>";print_r(explode(',',$listingOptions['filename']));exit;                     
                                                        if(!empty($gallery_image_ids)){
                                                            if (strpos($gallery_image_ids, ',') !== false) {
                                                                echo '<ul class="gallery-thumbs firstDiv">';
                                                                $gallArray = explode(',', $gallery_image_ids);
                                                                foreach($gallArray as $imgID){
                                                                    $imgGalFull = wp_get_attachment_image_src( $imgID,  'thumbnail');
                                                                    echo '<li>';
                                                                        echo '<img width="32" height="32" src="'.$imgGalFull[0].'" alt="">';
                                                                    echo '</li>';
                                                                }
                                                                echo '</ul>';
                                                            }
                                                            else{
                                                                echo '<ul class="gallery-thumbs">';
                                                                    $imgGalFull = wp_get_attachment_image_src( $gallery_image_ids,  'thumbnail');
                                                                        echo '<li>';
                                                                            echo '<img width="32" height="32" src="'.$imgGalFull[0].'" alt="">';
                                                                        echo '</li>';
                                                                    echo '</ul>';
                                                            }
                                                        }  
                                                    ?>
                                                </div>
                                            <?php } ?>                                                 
                                        </div>                                        
                                    </form>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="button" class="btn btn-secondary" data-dismiss="modal" id="btn-decline" value="<?php esc_html_e('Decline')?>"/>
                                <input type="button" ref="<?php echo $post_id; ?>" class="btn btn-primary" id="btn-approved_<?php echo $post_id ?>" value="<?php esc_html_e('Approved')?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="exampleModal_report_<?php echo $post_id ?>"tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelReport" aria-hidden="true">
                <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabelReport"><?php esc_html_e('Listing Report Description','Listingpro')?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">                                
                                <input type="hidden" id="post_status" name="post_status" value="2" />
                                <input type="hidden" id="reporterType" name="reporterType" value="<?php echo $listing_options_edited['reporterType'];?>" /> 
                                <input type="hidden" id="post_IDListing" name="post_IDListing" value="<?php echo $post_id?>"/>
                                <input type="hidden" id="POST_IDReport" name="POST_IDReport" value="<?php echo $listing_options_edited['post_id']?>"/>
                                <input type="hidden" id="post_title" name="post_title" value="<?php echo $listing_options_edited['post_title'];?>" />
                                
                                <div class="form-group col-lg-12 ml-0 mr-0 pl-0 pr-0">
                                    <label for="validationTextareaReport">Comment Box</label>
                                    <?php echo $listing_options_edited['postTypeComment']?>												
                                    <input type="hidden" id="validationTextareaReport" value="<?php echo $listing_options_edited['postTypeComment']?>">												
                                </div>
                                <div class="form-group col-lg-12 ml-0 mr-0 pl-0 pr-0">
                                    <label for="validationTooltip02Report">Email address</label>
                                    <?php echo $listing_options_edited['email']?>
                                    <input type="hidden" id="validationTooltip02Report" value="<?php echo $listing_options_edited['email']?>">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="button" class="btn btn-secondary" data-dismiss="modal" id="btn-reject-report" value="<?php esc_html_e('Reject Report')?>"/>
                                <input type="button" class="btn btn-primary" id="btn-deactive_listing_<?php echo $post_id ?>" value="<?php esc_html_e('Deactive Listing')?>">
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                jQuery(document).ready(function(){
                    jQuery("#btn-approved_<?php echo $post_id ?>").on('click', function(e){
                        e.preventDefault();
                        var website7 = jQuery('#website7<?php echo $post_id ?>').val();
                        var twitter7 = jQuery('#twitter7<?php echo $post_id ?>').val();
                        var facebook7 = jQuery('#facebook7<?php echo $post_id ?>').val();
                        var linkedin7 = jQuery('#linkedin7<?php echo $post_id ?>').val();
                        var instagram7 = jQuery('#instagram7<?php echo $post_id ?>').val();
                        var youtube7 = jQuery('#youtube7<?php echo $post_id ?>').val();
                        var pinterest7 = jQuery('#pinterest7<?php echo $post_id ?>').val();
                        var descriptions7 = jQuery('#descriptions7<?php echo $post_id ?>').val();
                        var gallery_image_ids = jQuery('#gallery_image_ids<?php echo $post_id ?>').val();
                        var post_title = jQuery('#post_title<?php echo $post_id ?>').val();
                        var post_link = jQuery('#post_link<?php echo $post_id ?>').val();
                        var post_status = jQuery('#post_status').val();
                        var POSTID = jQuery('#POST_ID').val();
                        var post_ID = jQuery(this).attr('ref');
                        var filename = '';
                        
                        var allAttributes = jQuery('.gallery_approval_thumb<?php echo $post_id ?> img[filenames]').map(function() {
                            return jQuery(this).attr('filenames');
                        }).get();
                        var editProfileType = jQuery('#editProfileRequestType<?php echo $post_id ?>').val();
                        //return false;
                        jQuery.ajax({
                            url: importer_ajax_object.ajaxurl,
                            type: 'post',
                            data: "action=proyoga_contactownerediteds&website7="+website7+'&twitter7='+twitter7+'&facebook7='+facebook7+'&linkedin7='+linkedin7+'&instagram7='+instagram7+'&youtube7='+youtube7+'&pinterest7='+pinterest7+'&descriptions7='+descriptions7+'&gallery_image_ids='+gallery_image_ids+'&post_title='+post_title+'&post_link='+post_link+'&post_status='+post_status+'&post_ID='+post_ID+'&editProfileType='+editProfileType+'&POSTID='+POSTID+'&filename='+allAttributes,
                            success: function(response){
                                var result = JSON.parse(response);
                                if(result.success == 1){
                                    Swal.fire(
                                        'Listing Approved Successfully',
                                        '',
                                        'success'
                                     ).then(() => {
                                        location.reload();
                                     })
                                }
                            }                            
                        });
                    });
                    jQuery("#btn-deactive_listing_<?php echo $post_id ?>").on('click', function(e){
                        e.preventDefault(); 
                        var validationComment = jQuery('#validationTextareaReport').val();
                        var ReportEmail       = jQuery('#validationTooltip02Report').val();
                        var post_status       = jQuery('#post_status').val();
                        var post_ID           = jQuery('#post_IDListing').val();
                        var postID            = jQuery('#POST_IDReport').val();
                        //console.log(postID);
                        var post_title        = jQuery('#post_title').val();
                        var type              = jQuery('#reporterType').val();
                        jQuery.ajax({
                            url: importer_ajax_object.ajaxurl,
                            type: 'post',
                            data:"action=proyoga_contactowerreport&validationComment="+validationComment+'&ReportEmail='+ReportEmail+'&post_status='+post_status+'&post_ID='+post_ID+'&postID='+postID+'&post_title='+post_title+'&type='+type,
                            success: function(response){
                                var result = JSON.parse(response);
                                if(result.success == 1){
                                    Swal.fire(
                                        'Listing Approved Successfully',
                                        '',
                                        'success'
                                     ).then(() => {
                                        location.reload();
                                     })
                                }
                            }    
                        });

                    });
                });
                </script>
            <?php 
          
            break;
            
        case 'status':
            $metabox = get_post_meta($post_id, 'lp_' . strtolower(THEMENAME) . '_options_contactownerform', true); 
            //echo "<pre>";print_r($metabox);exit;
            if($metabox['post_status'] == 0 ){
                $metabox_status = 'Pending';
            }else if($metabox['post_status'] == 1 ){
                $metabox_status = 'Decline';
            }else{
                $metabox_status = 'Approved';
            }
            echo $metabox_status;
            break;  
        case 'type':
            $metabox = get_post_meta($post_id, 'lp_' . strtolower(THEMENAME) . '_options_contactownerform', true); 
            // echo "<pre>";print_r($metabox['editProfileRequestType']);exit;//Edit profile request
            if(isset($metabox['reporterType'])){
                echo $metabox['reporterType'];
            }elseif(isset($metabox['profile_edit_reported']) && $metabox['post_status'] != 2){
                echo $metabox['profile_edit_reported'];
            }else{
                echo $metabox['editProfileRequestType'];
            }
        break;           
    }    
}
add_action( 'manage_edited_posts_custom_column','fill_edited_posts_custom_column',10,2);