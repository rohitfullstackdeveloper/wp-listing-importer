<?php
function post_type_restaurant() {  
    global $listingpro_options;   
    $labels = array(
        'name' => _x('Restaurant', 'post type general name', 'listingpro-plugin'),
        'singular_name' => _x('Restaurant', 'post type singular name', 'listingpro-plugin'),
        'add_new' => _x('Add New', 'book', 'listingpro-plugin'),
        'add_new_item' => __('Add New List', 'listingpro-plugin'),
        'edit_item' => __('Edit List', 'listingpro-plugin'),
        'new_item' => __('New Listing', 'listingpro-plugin'),
        'view_item' => __('View List', 'listingpro-plugin'),
        'search_items' => __('Search Listing', 'listingpro-plugin'),
        'not_found' =>  __('No List found', 'listingpro-plugin'),
        'not_found_in_trash' => __('No List found in Trash', 'listingpro-plugin'), 
        'parent_item_colon' => ''
    );      
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'query_var' => 'restaurant',
        'rewrite'   => array( 'slug' => 'restaurant' ),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => false,
        'menu_position' => null,
        'show_in_rest'       => true,
        'supports' => array( 'title', 'editor', 'author', 'thumbnail','comments' ),
        'menu_icon' => plugin_dir_url( __DIR__ ). '../public/assets/images/restaurant.png'
    );      

    register_post_type( 'restaurant', $args );                
} 
                                    
add_action('init', 'post_type_restaurant',0);

function post_type_restaurant_form_fields() {
    $labels = array(
        'name' => _x('Form Fields', 'post type general name', 'listingpro-plugin'),
        'singular_name' => _x('Field', 'post type singular name', 'listingpro-plugin'),
        'add_new' => _x('Add New Field', 'book', 'listingpro-plugin'),
        'add_new_item' => __('Add New Field', 'listingpro-plugin'),
        'edit_item' => __('Edit Field', 'listingpro-plugin'),
        'new_item' => __('New Field', 'listingpro-plugin'),
        'view_item' => __('View Field', 'listingpro-plugin'),
        'search_items' => __('Search Fields', 'listingpro-plugin'),
        'not_found' =>  __('No Field found', 'listingpro-plugin'),
        'not_found_in_trash' => __('No Field found in Trash', 'listingpro-plugin'), 
        'parent_item_colon' => ''
    );      
    $args = array(
        'labels' => $labels,
        'public' => false,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'query_var' => 'form-fields',
        'rewrite' => true,
        'capability_type' =>'post',
        'has_archive' => false,
        'hierarchical' => false,
        'menu_position' => null,
        'exclude_from_search' => true,
        'show_in_menu' => 'edit.php?post_type=restaurant',
        'supports' => array( 'title' ),
        'menu_icon' => plugins_url( 'post_importer/includes/templates/images/blog.png', dirname(__FILE__) )
    );      

    register_post_type( 'restaurant-fields', $args );                
} 
add_action('init', 'post_type_restaurant_form_fields',0);

function remove_post_custom__restaurant_fields() {
    remove_meta_box( 'commentstatusdiv', 'restaurant', 'normal' );
    remove_meta_box( 'commentsdiv', 'restaurant', 'normal' );
    remove_meta_box( 'revisionsdiv', 'restaurant', 'normal' );
    remove_meta_box( 'authordiv', 'restaurant', 'normal' ); 
    remove_meta_box( 'featuresdiv', 'restaurant', 'side' );
    //add_submenu_page('edit.php?post_type=restaurant', __('Rejection Listings', 'custom_table_example'), __('Rejection Listings', 'custom_table_example'), 'activate_plugins', 'school-rejection-listing', 'custom_table_restaurant_rejection_listing_page_handler');      

}
add_action( 'admin_menu' , 'remove_post_custom__restaurant_fields' );   

add_filter('views_edit-restaurant','my_filter_restaurant');

function my_filter_restaurant($views){
    $views['import'] = '<a href="javascript:void(0)" id="importJsonRestaurant" class="primary">Export</a>';
    return $views;
}

function custom_table_restaurant_rejection_listing_page_handler(){
    global $wpdb;
    $table = new Custom_Table_Example_List_Table( 'restaurant' );
    $table->prepare_items();
    $message = '';

    if ('delete' === $table->current_action()) {
        $message = '<div class="updated below-h2" id="message"><p>' . sprintf(__('Items deleted: %d', 'custom_table_example'), count($_REQUEST['id'])) . '</p></div>';
    }
?>
    <div class="wrap">
        <div class="icon32 icon32-posts-post" id="icon-edit"></div>
        <h2><?php _e('Rejection Listings', 'custom_table_example')?></h2>
        <?php echo $message; ?>
        <form id="rejection-table" method="GET">
            <?php $table->display() ?>
        </form>
    </div>
<?php
}

function restaurant_category() {            
    global $listingpro_options;
    $school_cat_slug = '';
    if(class_exists('ReduxFramework')){
        $restaurant_cat_slug = (isset($listingpro_options['restaurant_cat_slug']))?$listingpro_options['restaurant_cat_slug']:'';   
        $restaurant_cat_slug = trim($restaurant_cat_slug);
    }
    if(empty($restaurant_cat_slug)){
        $restaurant_cat_slug = 'restaurant-category';
    }
    register_taxonomy(
        'restaurant-category',
        'restaurant',
        array(
            'labels' => array(
                'name' => 'Category',
                'add_new_item' => 'Add New Category',
                'new_item_name' => "New Category"
            ),
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true,
            'rewrite'           => array( 'slug' => $restaurant_cat_slug,'hierarchical' => true ),
            'query_var'     => true,
            'public'            => true,
            'show_in_rest'       => true,
            'capabilities' => array(
                'assign_terms' => 'assign_restaurant-category',
            )
            )
    );
    
}
add_action( 'init', 'restaurant_category', 0 );

function restaurant_tags() {            
    register_taxonomy(
        'restaurant-tags',
        'restaurant',
            array(
                'hierarchical'  => false,
                'label'         => "Tags",
                'singular_name' => "Tag",
                'show_ui' => true,
                'rewrite'       => true,
                'query_var'     => true,
                'public'            => true,
                'show_in_rest'       => true,
                'capabilities' => array(
                    'assign_terms' => 'assign_restaurant-tags',
                )
            )
        );
}
add_action( 'init', 'restaurant_tags', 0 );

function restaurant_location() {
    global $listingpro_options;
        $restaurant_loc_slug = '';
        if(class_exists('ReduxFramework')){
            $restaurant_loc_slug = (isset($listingpro_options['restaurant_loc_slug']))?$listingpro_options['restaurant_loc_slug']:'';
            $restaurant_loc_slug = trim($restaurant_loc_slug);
        }
        if(empty($restaurant_loc_slug)){
            $restaurant_loc_slug = 'location';
        }
    $location_labels = array(
        'name' => __( 'Location', 'listingpro-plugin' ),
        'singular_name' => __( 'Location', 'listingpro-plugin' ),
        'search_items' =>  __( 'Search Locations', 'listingpro-plugin' ),
        'view_item' =>  __( 'View Location', 'listingpro-plugin' ),
        'popular_items' => __( 'Popular Locations', 'listingpro-plugin' ),
        'all_items' => __( 'All Locations', 'listingpro-plugin' ),
        'parent_item' => __( 'Parent Location', 'listingpro-plugin' ),
        'parent_item_colon' => __( 'Parent Location:', 'listingpro-plugin' ),
        'edit_item' => __( 'Edit Location', 'listingpro-plugin' ),
        'update_item' => __( 'Update Location', 'listingpro-plugin' ),
        'add_new_item' => __( 'Add New Location', 'listingpro-plugin' ),
        'new_item_name' => __( 'New Location Name', 'listingpro-plugin' ),
        'separate_items_with_commas' => __( 'Separate Locations with commas', 'listingpro-plugin' ),
        'add_or_remove_items' => __( 'Add or remove Location', 'listingpro-plugin' ),
        'choose_from_most_used' => __( 'Choose from the most used Locations', 'listingpro-plugin' ),
        'menu_name' => __( 'Locations', 'listingpro-plugin' ),
        'back_to_items' => __( 'Back to Locations', 'listingpro-plugin' )
    );
    register_taxonomy("restaurant-location",
        array("restaurant"),
        array("hierarchical" => true,
        'labels' => $location_labels,
        'show_ui' => true,
            'query_var' => true,
            'rewrite'   => array( 'slug' => $restaurant_loc_slug ),
            'show_in_rest'       => true,
            'capabilities' => array(
                'assign_terms' => 'assign_restaurant-location',
            )
        )
    );
    
}
add_action( 'init', 'restaurant_location', 0 );