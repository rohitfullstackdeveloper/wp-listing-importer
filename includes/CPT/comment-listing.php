<?php
/**
 * Defining Custom Table List
 * ============================================================================
 *
 * In this part you are going to define custom table list class,
 * that will display your database records in nice looking table
 *
 * http://codex.wordpress.org/Class_Reference/WP_List_Table
 * http://wordpress.org/extend/plugins/custom-list-table-example/
 */

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class Custom_Table_Comments_Example_List_Table extends WP_List_Table{
    function __construct( $type ){
        global $status, $page,$post_type;
        $this->screen->post_type;

        parent::__construct(array(
            'singular' => 'Comment Listing',
            'plural' => 'Comment Listings',
            'post_type' => $this->screen->post_type
        ));
    }

    function column_default($item, $column_name){
        return $item[$column_name];
    }

    function column_title($item){
        $listing_json = json_decode($item[ 'listing_options_json' ], true , JSON_UNESCAPED_SLASHES);
        return '<span>' . $listing_json['title'] . '</span>';
    }

    function column_reason($item){
        return '<span>' . $item['reason'] . '</span>';
    }
    function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="id[]" value="%s" />',
            $item['listing_id']
        );
    }
    function get_columns(){
        $columns = array(
            'cb' => '<input type="checkbox" />', //Render a checkbox instead of text
            'title' => __('Title', 'custom_table_example'),
            'reason' => __('Reason', 'custom_table_example')
        );
        return $columns;
    }
    function get_sortable_columns(){
        $sortable_columns = array(
            'title' => array('title', true),
        );
        return $sortable_columns;
    }
    function get_bulk_actions(){
        $actions = array(
            'delete' => 'Delete'
        );
        return $actions;
    }

    function process_bulk_action(){
        global $wpdb;
        $table_name = $wpdb->prefix . 'listing_comments'; // do not forget about tables prefix

        if ('delete' === $this->current_action()) {
            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
            if (is_array($ids)) $ids = implode(',', $ids);

            if (!empty($ids)) {
                $wpdb->query("DELETE FROM $table_name WHERE id IN($ids)");
            }
        }
    }

    function prepare_items(){
        global $wpdb;
        $table_name = $wpdb->prefix . 'listing_comments';
        $per_page = 5;
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        // here we configure table headers, defined in our methods
        $this->_column_headers = array($columns, $hidden, $sortable);
        // [OPTIONAL] process bulk action if any
        $this->process_bulk_action();
        // will be used in pagination settings
        $total_items = $wpdb->get_var("SELECT COUNT(id) FROM $table_name WHERE post_type='".$this->screen->post_type."'");
        // prepare query params, as usual current page, order by and order direction
        $paged = isset($_REQUEST['paged']) ? ($per_page * max(0, intval($_REQUEST['paged']) - 1)) : 0;
        $orderby = (isset($_REQUEST['orderby']) && in_array($_REQUEST['orderby'], array_keys($this->get_sortable_columns()))) ? $_REQUEST['orderby'] : 'id';
        $order = (isset($_REQUEST['order']) && in_array($_REQUEST['order'], array('asc', 'desc'))) ? $_REQUEST['order'] : 'asc';
        // [REQUIRED] define $items array
        // notice that last argument is ARRAY_A, so we will retrieve array

        $this->items = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE post_type='%s' ORDER BY $orderby $order LIMIT %d OFFSET %d", $this->screen->post_type, $per_page, $paged), ARRAY_A);
        // echo '<pre>'; print_r( $this->items ); exit;

        // [REQUIRED] configure pagination
        $this->set_pagination_args(array(
            'total_items' => $total_items, // total items defined above
            'per_page' => $per_page, // per page constant defined at top of method
            'total_pages' => ceil($total_items / $per_page) // calculate pages count
        ));
    }
}