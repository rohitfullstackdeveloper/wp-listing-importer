<?php
function post_type_retreat() {  
    global $listingpro_options;

    $labels = array(
        'name' => _x('Retreat', 'post type general name', 'listingpro-plugin'),
        'singular_name' => _x('Retreat', 'post type singular name', 'listingpro-plugin'),
        'add_new' => _x('Add New', 'book', 'listingpro-plugin'),
        'add_new_item' => __('Add New List', 'listingpro-plugin'),
        'edit_item' => __('Edit List', 'listingpro-plugin'),
        'new_item' => __('New Listing', 'listingpro-plugin'),
        'view_item' => __('View List', 'listingpro-plugin'),
        'search_items' => __('Search Listing', 'listingpro-plugin'),
        'not_found' =>  __('No List found', 'listingpro-plugin'),
        'not_found_in_trash' => __('No List found in Trash', 'listingpro-plugin'), 
        'parent_item_colon' => ''
    );      
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'query_var' => 'retreat',
        'rewrite'   => array( 'slug' => 'retreat' ),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => false,
        'menu_position' => null,
        'show_in_rest'       => true,
        'supports' => array( 'title', 'editor', 'author', 'thumbnail','comments' ),
        'menu_icon' => plugin_dir_url( __DIR__ ). '../public/assets/images/retreat.png'
    );      

    register_post_type( 'retreat', $args );                   
} 
                                    
add_action('init', 'post_type_retreat',0);

function post_type_retreat_form_fields() {
    $labels = array(
        'name' => _x('Form Fields', 'post type general name', 'listingpro-plugin'),
        'singular_name' => _x('Field', 'post type singular name', 'listingpro-plugin'),
        'add_new' => _x('Add New Field', 'book', 'listingpro-plugin'),
        'add_new_item' => __('Add New Field', 'listingpro-plugin'),
        'edit_item' => __('Edit Field', 'listingpro-plugin'),
        'new_item' => __('New Field', 'listingpro-plugin'),
        'view_item' => __('View Field', 'listingpro-plugin'),
        'search_items' => __('Search Fields', 'listingpro-plugin'),
        'not_found' =>  __('No Field found', 'listingpro-plugin'),
        'not_found_in_trash' => __('No Field found in Trash', 'listingpro-plugin'), 
        'parent_item_colon' => ''
    );      
    $args = array(
        'labels' => $labels,
        'public' => false,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'query_var' => 'form-fields',
        'rewrite' => true,
        'capability_type' =>'post',
        'has_archive' => false,
        'hierarchical' => false,
        'menu_position' => null,
        'exclude_from_search' => true,
        'show_in_menu' => 'edit.php?post_type=retreat',
        'supports' => array( 'title' ),
        'menu_icon' => plugin_dir_url( __DIR__ ). '../public/assets/images/blog.png'
    );      

    register_post_type( 'retreat-form-fields', $args );                   
}

add_action('init', 'post_type_retreat_form_fields',0);                        

function remove_post_custom__retreat_fields() {
    remove_meta_box( 'commentstatusdiv', 'retreat', 'normal' );
    remove_meta_box( 'commentsdiv', 'retreat', 'normal' );
    remove_meta_box( 'revisionsdiv', 'retreat', 'normal' );
    remove_meta_box( 'authordiv', 'retreat', 'normal' ); 
    remove_meta_box( 'featuresdiv', 'retreat', 'side' );
    //add_submenu_page('edit.php?post_type=retreat', __('Rejection Listings', 'custom_table_example'), __('Rejection Listings', 'custom_table_example'), 'activate_plugins', 'retreat-rejection-listing', 'custom_table_retreat_rejection_listing_page_handler');      

}
add_action( 'admin_menu' , 'remove_post_custom__retreat_fields' );

function custom_table_retreat_rejection_listing_page_handler(){
    global $wpdb;
    $table = new Custom_Table_Example_List_Table( 'retreat' );
    $table->prepare_items();
    $message = '';

    if ('delete' === $table->current_action()) {
        $message = '<div class="updated below-h2" id="message"><p>' . sprintf(__('Items deleted: %d', 'custom_table_example'), count($_REQUEST['id'])) . '</p></div>';
    }
?>
    <div class="wrap">
        <div class="icon32 icon32-posts-post" id="icon-edit"></div>
        <h2><?php _e('Rejection Listings', 'custom_table_example')?></h2>
        <?php echo $message; ?>
        <form id="rejection-table" method="GET">
            <?php $table->display() ?>
        </form>
    </div>
<?php
}

function retreat_category() {           
    global $listingpro_options;
    $retreat_cat_slug = '';
    if(class_exists('ReduxFramework')){
        $retreat_cat_slug = (isset($listingpro_options['retreat_cat_slug']))?$listingpro_options['retreat_cat_slug']:'';    
        $retreat_cat_slug = trim($retreat_cat_slug);
    }
    if(empty($retreat_cat_slug)){
        $retreat_cat_slug = 'retreats';
    }
    register_taxonomy(
        'retreats',
        'retreat',
        array(
            'labels' => array(
                'name' => 'Category',
                'add_new_item' => 'Add New Category',
                'new_item_name' => "New Category"
            ),
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true,
            'rewrite'           => array( 'slug' => $retreat_cat_slug,'hierarchical' => true ),
            'query_var'     => true,
            'public'            => true,
            'show_in_rest'       => true
            )
    );
    
}
add_action( 'init', 'retreat_category', 0 );

function retreat_tags() {           
    register_taxonomy(
        'retreat-tags',
        'retreat',
            array(
                'hierarchical'  => false,
                'label'         => "Tags",
                'singular_name' => "Tag",
                'show_ui' => true,
                'rewrite'       => true,
                'query_var'     => true,
                'public'            => true,
                'show_in_rest'       => true
            )
        );
}
add_action( 'init', 'retreat_tags', 0 );

function retreat_location() {
    global $listingpro_options;
    $retreat_loc_slug = '';
    if(class_exists('ReduxFramework')){
        $retreat_loc_slug = (isset($listingpro_options['retreat_loc_slug']))?$listingpro_options['retreat_loc_slug']:'';
        $retreat_loc_slug = trim($retreat_loc_slug);
    }

    if(empty($retreat_loc_slug)){
        $retreat_loc_slug = 'location';
    }
    
    $location_labels = array(
        'name' => __( 'Location', 'listingpro-plugin' ),
        'singular_name' => __( 'Location', 'listingpro-plugin' ),
        'search_items' =>  __( 'Search Locations', 'listingpro-plugin' ),
        'view_item' =>  __( 'View Location', 'listingpro-plugin' ),
        'popular_items' => __( 'Popular Locations', 'listingpro-plugin' ),
        'all_items' => __( 'All Locations', 'listingpro-plugin' ),
        'parent_item' => __( 'Parent Location', 'listingpro-plugin' ),
        'parent_item_colon' => __( 'Parent Location:', 'listingpro-plugin' ),
        'edit_item' => __( 'Edit Location', 'listingpro-plugin' ),
        'update_item' => __( 'Update Location', 'listingpro-plugin' ),
        'add_new_item' => __( 'Add New Location', 'listingpro-plugin' ),
        'new_item_name' => __( 'New Location Name', 'listingpro-plugin' ),
        'separate_items_with_commas' => __( 'Separate Locations with commas', 'listingpro-plugin' ),
        'add_or_remove_items' => __( 'Add or remove Location', 'listingpro-plugin' ),
        'choose_from_most_used' => __( 'Choose from the most used Locations', 'listingpro-plugin' ),
        'menu_name' => __( 'Locations', 'listingpro-plugin' ),
        'back_to_items' => __( 'Back to Locations', 'listingpro-plugin' )
    );
    register_taxonomy("retreat-location",
        array("retreat"),
        array("hierarchical" => true,
        'labels' => $location_labels,
        'show_ui' => true,
            'query_var' => true,
            'rewrite'   => array( 'slug' => $retreat_loc_slug ),
            'show_in_rest'       => true
        )
    );
    
}
add_action( 'init', 'retreat_location', 0 );