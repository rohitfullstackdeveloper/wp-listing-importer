<?php
/**
 * @since      1.0.0
 * @package    WPLI
 * @subpackage WPLI/includes
 * @author     Rohit Sharma
 */

class listing_importer {
	protected $loader,$plugin_name,$version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */

	public function __construct() {
		$this->plugin_name 	= 'WPLI';
		$this->version 		= '1.0.0';
		$this->load_dependencies();
	}

	/**
	 * @description  Load the required dependencies for this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @author   Rohit Sharma
	 */

	private function load_dependencies() {
		//	Include All Post Types
		require_once plugin_dir_path( __DIR__ ) . 'includes/CPT/cpt-retreat.php';
		require_once plugin_dir_path( __DIR__ ) . 'includes/CPT/cpt-school.php';
		//require_once plugin_dir_path( __DIR__ ) . 'includes/CPT/cpt-studio.php';
		require_once plugin_dir_path( __DIR__ ) . 'includes/CPT/cpt-teacher.php';
		require_once plugin_dir_path( __DIR__ ) . 'includes/CPT/cpt-restaurants.php';
		require_once plugin_dir_path( __DIR__ ) . 'includes/CPT/cpt-listing-edited.php';
		require_once plugin_dir_path( __DIR__ ) . 'includes/CPT/comment-listing.php';

		require_once plugin_dir_path( __DIR__ ) . 'includes/metaboxes/teacher-category-meta.php';
		require_once plugin_dir_path( __DIR__ ) . 'includes/metaboxes/school-category-meta.php';
		require_once plugin_dir_path( __DIR__ ) . 'includes/metaboxes/retreat-category-meta.php';
		require_once plugin_dir_path( __DIR__ ) . 'includes/metaboxes/restaurant-category-meta.php';
		
		// Load Helper File
		require_once plugin_dir_path( __DIR__ ).'includes/class-listing-importer-helper.php';

		// Load Admin File
		require_once plugin_dir_path( __DIR__ ).'admin/class-admin-wpli.php';

		// Load Frontend File
		require_once plugin_dir_path( __DIR__ ).'public/class-frontend-wpli.php';
	}

	/**
	 * @description The name of the plugin used to uniquely identify it within the context of
	 *	  			WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 * @author    Rohit Sharma
	 */

	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * @description Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 * @author    Rohit Sharma
	 */

	public function get_version() {
		return $this->version;
	}
}